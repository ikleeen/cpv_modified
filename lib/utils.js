﻿function $get(object) { return document.getElementById(object); }

function saveFieldsLandingPage(suffix)
{
    var fieldsLandingArray = new Array();
    var currentLanding = 0;
    while (currentLanding!=-1)
    {
        var idField = $get("hidLandingID" + suffix + currentLanding);
        var landingNameField = $get("txtPageName" + suffix + currentLanding);
        var landingIDField = $get("txtPageID" + suffix + currentLanding);
        var landingUrlField = $get("txtPageUrl" + suffix + currentLanding);
        var landingShareField = $get("txtShare" + suffix + currentLanding);
        var inactiveField = $get("chkInactive" + suffix + currentLanding);
        if (idField!=null && landingNameField!=null && landingIDField!=null && 
            landingUrlField!=null && landingShareField!=null && inactiveField!=null)
        {
            fieldsLandingArray.push({'id':idField.value, 'name':landingNameField.value, 'landingid':landingIDField.value,
                'url':landingUrlField.value, 'share':landingShareField.value, 'inactive':inactiveField.checked });
            currentLanding++;
        }
        else
            currentLanding = -1;
    }
    return fieldsLandingArray;
}

function saveFieldsOffer(suffix)
{
    var fieldsOfferArray = new Array();
    var currentOffer = 0;
    while(currentOffer != -1)
    {
        var idField = $get("hidOfferID" + suffix + currentOffer);
	    var offerNameField = $get("txtPageNameOffer" + suffix + currentOffer);
        var offerUrlField = $get("txtPageUrlOffer" + suffix + currentOffer);
        var offerPayoutField = $get("txtPayout" + suffix + currentOffer);
        var offerShareField = $get("txtShareOffer" + suffix + currentOffer);
        var networkField = $get("ddlNetwork" + suffix + currentOffer);
        var inactiveOffField = $get("chkInactiveOffer" + suffix + currentOffer);
        var offerPageIDField = $get("txtPageIDOffer" + suffix + currentOffer);
	    if (idField!=null && offerNameField!=null && offerUrlField!=null && offerPayoutField!=null && 
	        offerShareField!=null && networkField!=null && inactiveOffField!=null && offerPageIDField!=null)
        {
            networkValue = networkField.options.length > 0 ? networkField.options[networkField.selectedIndex].value : -1;
            fieldsOfferArray.push({'id':idField.value, 'offer':offerNameField.value, 'url':offerUrlField.value, 
                'payout': offerPayoutField.value, 'share':offerShareField.value, 'network':networkValue, 
                'inactive':inactiveOffField.checked, 'offerPageID':offerPageIDField.value});
            currentOffer++;
        }
        else
            currentOffer = -1;
    }
    return fieldsOfferArray;
}

function saveFieldsDirectLink()
{
	return { "landing":saveFieldsLandingPage('LP'), "offer":saveFieldsOffer('')};
}

function saveFieldsMultipleOptions()
{
	var fieldsArray = { "landing": saveFieldsLandingPage('LP'), "offer": [] };
	var currentPath=0;
	while (currentPath!=-1)
	{
	    var fieldsOfferArray = saveFieldsOffer(currentPath + '_');
	    if (fieldsOfferArray.length>0)
        {
        	fieldsArray["offer"].push(fieldsOfferArray);
		    currentPath++;
		}
		else
			currentPath = -1;
	}
	return fieldsArray;
}

function saveFieldsMultiplePaths()
{
	var fieldsArray = { "landing": [], "offer": [], "sharepath": [] };
	var currentPath=0;
	while (currentPath!=-1)
	{
	    var fieldsOfferArray = saveFieldsOffer(currentPath + '_');
        var fieldsLandingArray = saveFieldsLandingPage('LP' + currentPath + '_');
	    if (fieldsOfferArray.length>0 && fieldsLandingArray.length>0)
        {
            var sharePathField = $get("txtSharePath" + currentPath);
            fieldsArray["landing"].push(fieldsLandingArray);
            fieldsArray["offer"].push(fieldsOfferArray);
            fieldsArray["sharepath"].push(sharePathField.value);
		    currentPath++;
		}
		else
			currentPath = -1;
	}
	return fieldsArray;
}

function saveFieldsPageSequence()
{
	var fieldsArray = { "landing": [], "afteroptin": saveFieldsLandingPage("After"), "offer": [] };
	var currentPath=0;
	while (currentPath!=-1)
	{
	    var fieldsLandingArray = saveFieldsLandingPage('LP' + currentPath + '_');
	    if (fieldsLandingArray.length>0)
        {
        	fieldsArray["landing"].push(fieldsLandingArray);
		    currentPath++;
		}
		else
			currentPath = -1;
	}

	currentPath=0;
	while (currentPath!=-1)
	{
        var fieldsOfferArray = saveFieldsOffer(currentPath + '_');
    	if (fieldsOfferArray.length>0)
        {
        	fieldsArray["offer"].push(fieldsOfferArray);
		    currentPath++;
		}
		else
			currentPath = -1;
	}
	return fieldsArray;
}

function saveFieldsLeadCapture()
{
	return { "landing":saveFieldsLandingPage('LP'), "afteroptin":saveFieldsLandingPage("After"), "offer":saveFieldsOffer('')};
}

function saveFieldsEmailFollowUp()
{
	fieldsArray = { "landing": [], "offer": [], "sent": [] };
	var currentPath=0;
	while (currentPath!=-1)
	{
	    var fieldsLandingArray = saveFieldsLandingPage('LP' + currentPath + '_');
	    var fieldsOfferArray = saveFieldsOffer(currentPath + '_');
	    if (fieldsOfferArray.length>0 && fieldsLandingArray.length>0)
        {
	        var offerSentField = $get("txtSent" + currentPath);
            fieldsArray["landing"].push(fieldsLandingArray);
            fieldsArray["offer"].push(fieldsOfferArray);
            fieldsArray["sent"].push(offerSentField.value);
		    currentPath++;
		}
		else
			currentPath = -1;
	}
	return fieldsArray;
}

function addLandingPageSection(fieldsArray, destinationToShow, pathID, suffix, paddingtop)
{
   	var paddingtopValue = typeof paddingtop !== 'undefined' ? paddingtop : 0;
    var newHtml = '<div class="evenDiv"' + (paddingtopValue!=0 ? ' style="padding-top:' + paddingtopValue + 'px;"' : '') + '><a href="javascript:;" onclick="evenShares(\'LP' + suffix + '\')">\r\n';
    newHtml += '<img src="images/even_share_button.png" class="noborder" alt="Even Shares" title="Even Shares"/></a></div>\r\n';
    newHtml += '<div style="clear:both"></div>';

    for (i=0; i<destinationToShow; i++)
    {
        var idValue = fieldsArray["landing"][pathID]!=null && fieldsArray["landing"][pathID][i]!=null && fieldsArray["landing"][pathID][i]["id"]!=null ? fieldsArray["landing"][pathID][i]["id"] : 
            (fieldsArray["landing"][i]!=null && fieldsArray["landing"][i]["id"]!=null ? fieldsArray["landing"][i]["id"] : '0');
        var nameValue = htmlEntities(fieldsArray["landing"][pathID]!=null && fieldsArray["landing"][pathID][i]!=null && fieldsArray["landing"][pathID][i]["name"]!=null ? fieldsArray["landing"][pathID][i]["name"] : 
            (fieldsArray["landing"][i]!=null && fieldsArray["landing"][i]["name"]!=null ? fieldsArray["landing"][i]["name"] : ''));
        var landingIdValue = fieldsArray["landing"][pathID]!=null && fieldsArray["landing"][pathID][i]!=null && fieldsArray["landing"][pathID][i]["landingid"]!=null ? fieldsArray["landing"][pathID][i]["landingid"] : 
            (fieldsArray["landing"][i]!=null && fieldsArray["landing"][i]["landingid"]!=null ? fieldsArray["landing"][i]["landingid"] : '0');
        var urlValue = htmlEntities(fieldsArray["landing"][pathID] && fieldsArray["landing"][pathID][i] && fieldsArray["landing"][pathID][i]["url"]!=null ? fieldsArray["landing"][pathID][i]["url"] : 
            (fieldsArray["landing"][i]!=null && fieldsArray["landing"][i]["url"]!=null ? fieldsArray["landing"][i]["url"] : ''));
        var shareValue = fieldsArray["landing"][pathID]!=null && fieldsArray["landing"][pathID][i]!=null && fieldsArray["landing"][pathID][i]["share"]!=null ? fieldsArray["landing"][pathID][i]["share"] : 
            (fieldsArray["landing"][i]!=null && fieldsArray["landing"][i]["share"]!=null ? fieldsArray["landing"][i]["share"] : '0');
        var inactiveValue = fieldsArray["landing"][pathID]!=null && fieldsArray["landing"][pathID][i]!=null && fieldsArray["landing"][pathID][i]["inactive"]!=null ? fieldsArray["landing"][pathID][i]["inactive"] : 
            (fieldsArray["landing"][i]!=null && fieldsArray["landing"][i]["inactive"]!=null ? fieldsArray["landing"][i]["inactive"] : false);
        
        newHtml += '<div class="dsLabel">LP Name:<input type="hidden" name="hidLandingIDLP' + suffix + i + '" id="hidLandingIDLP' + suffix + i + '"value="' + idValue + '" /></div>\r\n';
        newHtml += '<div class="dsTextLong"><input type="text" class="dsInputLong" name="txtPageNameLP' +
	        suffix + i + '" id="txtPageNameLP' + suffix + i + '" value="' + nameValue + '"/></div>\r\n';
        newHtml += '<div class="dsLabel2">ID:</div>\r\n';
        newHtml += '<div style="float:left"><input type="text" class="dsInputShort" name="txtPageIDLP' + 
	        suffix + i + '" id="txtPageIDLP' + suffix + i + '" value="' + landingIdValue + '" onblur="validateInt(this,0)"/></div>\r\n';
        newHtml += '<div style="clear:both"></div><div class="dsLabel">Url:</div>\r\n';
        newHtml += '<div class="dsTextLong"><input type="text" class="dsInputLong" name="txtPageUrlLP' +
	        suffix + i + '" id="txtPageUrlLP' + suffix + i + '" value="' + urlValue + '"/></div>\r\n';
        newHtml += '<div class="dsLabel2">Share: %</div>\r\n';
        newHtml += '<div style="float:left"><input type="text" class="dsInputShort" name="txtShareLP' + 
	        suffix + i + '" id="txtShareLP' + suffix + i + '" value="' + shareValue + '" onblur="validateInt(this,0,100)"/></div>\r\n';
	    newHtml += '<div style="clear:both"></div><div class="dsLabel">&nbsp;</div><div class="dsTextLong">&nbsp;</div><div class="dsText">\r\n';
        newHtml += '<input type="checkbox" name="chkInactiveLP' + suffix + i + '" id="chkInactiveLP' + suffix + i + '" onclick="clickInactiveLP(this.checked,\'LP' + suffix + i + '\')"' + (inactiveValue ? ' checked="checked"' : '') + '/>\r\n';
        newHtml += ' <label for="chkInactiveLP' + suffix + i + '">Inactive</label></div>\r\n';
        newHtml += '<div style="clear:both">&nbsp;</div>\r\n';
	}
	return newHtml;
}

function addAfterOptinSection(fieldsArray, destinationToShow)
{
    var newHtml='<div style="float:left"><h3>After Opt-In</h3></div>';
    newHtml+='<div class="evenDiv" style="padding-top:12px;"><a href="javascript:;" onclick="evenShares(\'After\')">\r\n';
    newHtml+='<img src="images/even_share_button.png" class="noborder" alt="Even Shares" title="Even Shares"/></a></div>\r\n';
    newHtml+='<div style="clear:both"></div>';
    for (i=0; i<destinationToShow; i++)
    {
        var idValue = fieldsArray["afteroptin"][i]!=null && fieldsArray["afteroptin"][i]["id"]!=null ? fieldsArray["afteroptin"][i]["id"] : '0';
        var nameValue = htmlEntities(fieldsArray["afteroptin"][i]!=null && fieldsArray["afteroptin"][i]["name"]!=null ? fieldsArray["afteroptin"][i]["name"] : '');
        var landingIdValue = fieldsArray["afteroptin"][i]!=null && fieldsArray["afteroptin"][i]["landingid"]!=null ? fieldsArray["afteroptin"][i]["landingid"] : '0';
        var urlValue = htmlEntities(fieldsArray["afteroptin"][i]!=null && fieldsArray["afteroptin"][i]["url"]!=null ? fieldsArray["afteroptin"][i]["url"] : '');
        var shareValue = fieldsArray["afteroptin"][i]!=null && fieldsArray["afteroptin"][i]["share"]!=null ? fieldsArray["afteroptin"][i]["share"] : '0';
        var inactiveValue = fieldsArray["afteroptin"][i]!=null && fieldsArray["afteroptin"][i]["inactive"]!=null ? fieldsArray["afteroptin"][i]["inactive"] : false;

        newHtml += '<div class="dsLabel">LP Name:<input type="hidden" name="hidLandingIDAfter' + i + '" id="hidLandingIDAfter' + i + '"value="' + idValue + '" /></div>\r\n';
        newHtml += '<div class="dsTextLong"><input type="text" class="dsInputLong" name="txtPageNameAfter' +
	        i + '" id="txtPageNameAfter' + i + '" value="' + nameValue + '"/></div>\r\n';
        newHtml += '<div class="dsLabel2">ID:</div>\r\n';
        newHtml += '<div style="float:left"><input type="text" class="dsInputShort" name="txtPageIDAfter' + 
	        i + '" id="txtPageIDAfter' + i + '" value="' + landingIdValue + '" onblur="validateInt(this,0)"/></div>\r\n';
        newHtml += '<div style="clear:both"></div><div class="dsLabel">Url:</div>\r\n';
        newHtml += '<div class="dsTextLong"><input type="text" class="dsInputLong" name="txtPageUrlAfter' +
	        i + '" id="txtPageUrlAfter' + i + '" value="' + urlValue + '"/></div>\r\n';
        newHtml += '<div class="dsLabel2">Share: %</div>\r\n';
        newHtml += '<div style="float:left"><input type="text" class="dsInputShort" name="txtShareAfter' + 
	        i + '" id="txtShareAfter' + i + '" value="' + shareValue + '" onblur="validateInt(this,0,100)"/></div>\r\n';
	    newHtml += '<div style="clear:both"></div><div class="dsLabel">&nbsp;</div><div class="dsTextLong">&nbsp;</div><div class="dsText">\r\n';
        newHtml += '<input type="checkbox" name="chkInactiveAfter' + i + '" id="chkInactiveAfter' + i + '" onclick="clickInactiveLP(this.checked,\'After' + i + '\')"' + (inactiveValue ? ' checked="checked"' : '') + '/>\r\n';
	    newHtml += ' <label for="chkInactiveAfter' + i + '">Inactive</label></div>\r\n';
	    newHtml += '<div style="clear:both">&nbsp;</div>\r\n';
	}
	return newHtml;
}

function addOfferSection(fieldsArray, offersToShow, pathID, suffix, paddingtop)
{
    var paddingtopValue = typeof paddingtop !== 'undefined' ? paddingtop : 0;
    
    var networksMainControl = $get("hidNetworks");
    var networksArray = networksMainControl != null ? jQuery.parseJSON(networksMainControl.value) : new Array();

    var newHtml='<div class="evenDiv"' + (paddingtopValue!=0 ? ' style="padding-top:' + paddingtopValue + 'px;"' : '') + '><a href="javascript:;" onclick="evenShares(\'Offer' + suffix + '\')">\r\n';
    newHtml+='<img src="images/even_share_button.png" class="noborder" alt="Even Shares" title="Even Shares"/>\r\n';
    newHtml+='</a></div><div style="clear:both"></div>';
    for (i=0; i<offersToShow; i++)
    {
        var idValue = fieldsArray["offer"][pathID]!=null && fieldsArray["offer"][pathID][i]!=null && fieldsArray["offer"][pathID][i]["id"]!=null ? fieldsArray["offer"][pathID][i]["id"] : 
            (fieldsArray["offer"][i] && fieldsArray["offer"][i]["id"]!=null ? fieldsArray["offer"][i]["id"] : '0');
        var offerName = htmlEntities(fieldsArray["offer"][pathID]!=null && fieldsArray["offer"][pathID][i]!=null && fieldsArray["offer"][pathID][i]["offer"]!=null ? fieldsArray["offer"][pathID][i]["offer"] : 
            (fieldsArray["offer"][i]!=null && fieldsArray["offer"][i]["offer"]!=null ? fieldsArray["offer"][i]["offer"] : ''));
        var offerUrl = htmlEntities(fieldsArray["offer"][pathID]!=null && fieldsArray["offer"][pathID][i]!=null && fieldsArray["offer"][pathID][i]["url"]!=null ? fieldsArray["offer"][pathID][i]["url"] : 
            (fieldsArray["offer"][i]!=null && fieldsArray["offer"][i]["url"]!=null ? fieldsArray["offer"][i]["url"] : ''));
        var payout = fieldsArray["offer"][pathID]!=null && fieldsArray["offer"][pathID][i]!=null && fieldsArray["offer"][pathID][i]["payout"]!=null ? fieldsArray["offer"][pathID][i]["payout"] : 
            (fieldsArray["offer"][i]!=null && fieldsArray["offer"][i]["payout"]!=null ? fieldsArray["offer"][i]["payout"] : '0');
        var share = fieldsArray["offer"][pathID]!=null && fieldsArray["offer"][pathID][i]!=null && fieldsArray["offer"][pathID][i]["share"]!=null ? fieldsArray["offer"][pathID][i]["share"] : 
            (fieldsArray["offer"][i]!=null && fieldsArray["offer"][i]["share"]!=null ? fieldsArray["offer"][i]["share"] : '0');
        var networkValue = fieldsArray["offer"][pathID]!=null && fieldsArray["offer"][pathID][i]!=null && fieldsArray["offer"][pathID][i]["network"]!=null ? fieldsArray["offer"][pathID][i]["network"] : 
            (fieldsArray["offer"][i]!=null && fieldsArray["offer"][i]["network"]!=null ? fieldsArray["offer"][i]["network"] : '1');
        var inactiveValue = fieldsArray["offer"][pathID]!=null && fieldsArray["offer"][pathID][i]!=null && fieldsArray["offer"][pathID][i]["inactive"]!=null ? fieldsArray["offer"][pathID][i]["inactive"] : 
            (fieldsArray["offer"][i]!=null && fieldsArray["offer"][i]["inactive"]!=null ? fieldsArray["offer"][i]["inactive"] : false);
        var offerPageID = fieldsArray["offer"][pathID]!=null && fieldsArray["offer"][pathID][i]!=null && fieldsArray["offer"][pathID][i]["offerPageID"]!=null ? fieldsArray["offer"][pathID][i]["offerPageID"] : 
            (fieldsArray["offer"][i]!=null && fieldsArray["offer"][i]["offerPageID"]!=null ? fieldsArray["offer"][i]["offerPageID"] : '0');

        newHtml += '<div class="dsLabel">Offer:<input type="hidden" name="hidOfferID' + suffix + i + '" id="hidOfferID' + suffix + i + '" value="' + idValue + '"/></div>\r\n';
        newHtml += '<div class="dsTextLong"><input name="txtPageNameOffer' +
            suffix + i + '" id="txtPageNameOffer' + suffix + i + '" type="text" class="dsInputLong" value="' + offerName + '"  /></div>\r\n';
        newHtml += '<div class="dsLabel2">ID:</div>\r\n';
        newHtml += '<div style="float:left"><input name="txtPageIDOffer' +
            suffix + i + '" id="txtPageIDOffer' + suffix + i + '" type="text" class="dsInputShort" value="' + offerPageID + '" onblur="validateInt(this,0)" /></div>\r\n';
        newHtml += '<div style="clear:both"></div><div class="dsLabel">URL:</div>\r\n';
        newHtml += '<div class="dsTextLong"><input name="txtPageUrlOffer' +
            suffix + i + '" id="txtPageUrlOffer' + suffix + i + '" type="text" class="dsInputLong" value="' + offerUrl + '"  /></div>\r\n';
        newHtml += '<div class="dsLabel2">Share: %</div>\r\n';
        newHtml += '<div style="float:left"><input name="txtShareOffer' +
            suffix + i + '" id="txtShareOffer' + suffix + i + '" type="text" class="dsInputShort" value="' + share + '" onblur="validateInt(this,0,100)" /></div>\r\n';
        newHtml += '<div style="clear:both"></div><div class="dsLabel">Network:</div>\r\n';
        newHtml += '<div class="dsTextLong"><select name="ddlNetwork' + suffix + i + '" id="ddlNetwork' + suffix + i + '">\r\n';
        for(j=0; j<networksArray.length; j++)
        {
            var selectedText = '';
            if (networksArray[j]["AffiliateSourceID"] == networkValue)
                selectedText = 'selected="selected"';
            newHtml += '<option value="' + networksArray[j]["AffiliateSourceID"] + '" ' + selectedText + '>' +
                networksArray[j]["Affiliate"] + '</option>';
        }
        newHtml += '</select><div class="dsInside">\r\n';
        newHtml += '<input type="checkbox" name="chkInactiveOffer' + suffix + i + '" id="chkInactiveOffer' + suffix + i + '" onclick="clickInactiveOffer(this.checked,\'' + suffix + i + '\')"' + (inactiveValue ? ' checked="checked"' : '') + '/>\r\n';
        newHtml += ' <label for="chkInactiveOffer' + suffix + i + '">Inactive</label></div></div>\r\n';
        newHtml += '<div class="dsLabel2">Payout: $</div>\r\n';
        newHtml += '<div style="float:left"><input name="txtPayout' +
            suffix + i + '" id="txtPayout' + suffix + i + '" type="text" class="dsInputShort" value="' + payout + '" onblur="validateFloat(this,0)" /></div>\r\n';
        newHtml += '<div style="clear:both">&nbsp;</div>\r\n';
    }
    return newHtml;
}

function addDestinationDirectLink(incrDest, incrOffer)
{
	var fieldsArray = saveFieldsDirectLink();
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	destinationToShow=parseInt(destinationToShow)+incrDest;
	hidDestinationToShow.value = destinationToShow;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	offersToShow=parseInt(offersToShow)+incrOffer;
	hidOffersToShow.value = offersToShow;
	var hidDestinationIndex = $get("hidDestinationIndex");
	var destinationIndex = hidDestinationIndex.value;
	var containerControl = $get("divDestinations");

	var newHtml = '<div id="divLandingPages"' + (destinationIndex==2 ? 'style="display:none"' : '') + '>';
	newHtml += addLandingPageSection(fieldsArray, destinationToShow, 0, '');
	newHtml += '</div><div id="divOffers">';
	newHtml += addOfferSection(fieldsArray, offersToShow, 0, '');
	newHtml += '</div>';
	
	containerControl.innerHTML = newHtml;
	clickInactiveLPAll(destinationToShow,'LP');
    clickInactiveOfferAll(offersToShow,'');
}

function addDestinationMultipleOptions(incrDest, incrOffer, incrPath)
{
	var fieldsArray = saveFieldsMultipleOptions();
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	destinationToShow=parseInt(destinationToShow)+parseInt(incrDest);
	hidDestinationToShow.value = destinationToShow;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	offersToShow=parseInt(offersToShow)+parseInt(incrOffer);
	hidOffersToShow.value = offersToShow;
	var hidPathsToShow = $get("hidPathsToShow");
	var pathsToShow = hidPathsToShow.value;
	pathsToShow=parseInt(pathsToShow)+parseInt(incrPath);
	hidPathsToShow.value = pathsToShow;
	var containerControl = $get("divDestinations");

	var newHtml = addLandingPageSection(fieldsArray, destinationToShow, 0, '');
	
    for (k=0; k<pathsToShow; k++)
    {
        newHtml += '<div style="float:left"><h3>Option ' + (k+1) + '</h3></div>';
        newHtml += addOfferSection(fieldsArray, offersToShow, k, k + '_', 12);
	}
	containerControl.innerHTML = newHtml;
	clickInactiveLPAll(destinationToShow,'LP');
    for (j=0; j<pathsToShow; j++)
    	clickInactiveOfferAll(offersToShow,j + '_');
}

function addDestinationMultiplePaths(incrDest, incrOffer, incrPath)
{
	var fieldsArray = saveFieldsMultiplePaths();
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	destinationToShow=parseInt(destinationToShow)+parseInt(incrDest);
	hidDestinationToShow.value = destinationToShow;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	offersToShow=parseInt(offersToShow)+parseInt(incrOffer);
	hidOffersToShow.value = offersToShow;
	var hidPathsToShow = $get("hidPathsToShow");
	var pathsToShow = hidPathsToShow.value;
	pathsToShow=parseInt(pathsToShow)+parseInt(incrPath);
	hidPathsToShow.value = pathsToShow;
	var containerControl = $get("divDestinations");
	var newHtml = '';

    for (k=0; k<pathsToShow; k++)
    {
        var sharePathValue = fieldsArray["sharepath"][k]!=null ? fieldsArray["sharepath"][k] : '0';
        newHtml += '<div style="float:left"><h3>Path ' + (k+1) + '</h3></div>';
        newHtml += '<div class="dsLabel2" style="padding-left:100px; padding-top:15px">Share: %</div>';
        newHtml += '<div style="float: left; padding-top:10px"><input type="text" onblur="validateInt(this,0,100)" value="' + sharePathValue + '" id="txtSharePath' + k + '" name="txtSharePath' + k + '" class="dsInputShort"></div>';

        newHtml += addLandingPageSection(fieldsArray, destinationToShow, k, k + '_', 12);

		newHtml += addOfferSection(fieldsArray, offersToShow, k, k + '_');
	}
	containerControl.innerHTML = newHtml;
    for (j=0; j<pathsToShow; j++)
    {
	    clickInactiveLPAll(destinationToShow,'LP' + j + '_');
    	clickInactiveOfferAll(offersToShow,j + '_');
    }
}

function addDestinationPageSequence(incrDest, incrOffer, incrPath, incrOption)
{
	var fieldsArray = saveFieldsPageSequence();
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	destinationToShow=parseInt(destinationToShow)+parseInt(incrDest);
	hidDestinationToShow.value = destinationToShow;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	offersToShow=parseInt(offersToShow)+parseInt(incrOffer);
	hidOffersToShow.value = offersToShow;
	var hidPathsToShow = $get("hidPathsToShow");
	var pathsToShow = hidPathsToShow.value;
	pathsToShow=parseInt(pathsToShow)+parseInt(incrPath);
	hidPathsToShow.value = pathsToShow;
	var hidOptionsToShow = $get("hidOptionsToShow");
	var optionsToShow = hidOptionsToShow.value;
	optionsToShow=parseInt(optionsToShow)+parseInt(incrOption);
	hidOptionsToShow.value = optionsToShow;
	var hidDestinationIndex = $get("hidDestinationIndex");
	var destinationIndex = hidDestinationIndex.value;
	var hidTrackingIndex = $get("hidTrackingIndex");
	var trackingIndex = hidTrackingIndex.value;
	var containerControl = $get("divDestinations");
	var newHtml = '';

    for (k=0; k<pathsToShow; k++)
    {
        newHtml += '<div style="float:left"><h3>Level ' + (k+1) + '</h3></div>';
	    newHtml += addLandingPageSection(fieldsArray, destinationToShow, k, k + '_', 12);
	}
	
	newHtml+='<div id="divOptIn1" style="display:' + (destinationIndex==2 || trackingIndex!=1 ? "none" : "block") + '">';
	newHtml+=addAfterOptinSection(fieldsArray, destinationToShow);
	
	newHtml+='</div><div id="divOptIn2" style="display:' + (destinationIndex==4 && trackingIndex==3 ? "none" : "block") + '"><br/>';
	for (k=0; k<optionsToShow; k++)
    {
        newHtml += '<div style="float:left"><h3>Option ' + (k+1) + '</h3></div>';
        newHtml += addOfferSection(fieldsArray, offersToShow, k, k + '_', 12);
	}
	newHtml += '</div><div id="divOptIn3" style="display: ' + (destinationIndex==2 || trackingIndex!=3 ? "none" : "block") + '">';
	newHtml += '</div>';
	containerControl.innerHTML = newHtml;
    for (j=0; j<pathsToShow; j++)
	    clickInactiveLPAll(destinationToShow,'LP' + j + '_');
	clickInactiveLPAll(destinationToShow,'After');
    for (j=0; j<optionsToShow; j++)
        clickInactiveOfferAll(offersToShow,j + '_');
}

function addDestinationLeadCapture(incrDest, incrOffer)
{
	var fieldsArray = saveFieldsLeadCapture();
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	destinationToShow=parseInt(destinationToShow)+incrDest;
	hidDestinationToShow.value = destinationToShow;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	offersToShow=parseInt(offersToShow)+parseInt(incrOffer);
	hidOffersToShow.value = offersToShow;
	var hidTrackingIndex = $get("hidTrackingIndex");
	var trackingIndex = hidTrackingIndex.value;
	var containerControl = $get("divDestinations");

    var newHtml = addLandingPageSection(fieldsArray, destinationToShow, 0, '');
    
	newHtml += '<div id="divOptIn1" style="display:' + (trackingIndex!=1 ? "none" : "block") + '">';
	newHtml += addAfterOptinSection(fieldsArray, destinationToShow);

	newHtml += '</div><div id="divOptIn2" style="display:' + (trackingIndex==4 ? "none" : "block") + '"><br/>';
	newHtml += addOfferSection(fieldsArray, offersToShow, 0, '');
	newHtml += '</div>';
	containerControl.innerHTML = newHtml;
	clickInactiveLPAll(destinationToShow,'LP');
	clickInactiveLPAll(destinationToShow,'After');
    clickInactiveOfferAll(offersToShow,'');
}

function addDestinationEmailFollowUp(incrDest, incrOffer, incrPath)
{
	var fieldsArray = saveFieldsEmailFollowUp();
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	destinationToShow=parseInt(destinationToShow)+parseInt(incrDest);
	hidDestinationToShow.value = destinationToShow;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	offersToShow=parseInt(offersToShow)+parseInt(incrOffer);
	hidOffersToShow.value = offersToShow;
	var hidPathsToShow = $get("hidPathsToShow");
	var pathsToShow = hidPathsToShow.value;
	pathsToShow=parseInt(pathsToShow)+parseInt(incrPath);
	hidPathsToShow.value = pathsToShow;
	var hidDestinationIndex = $get("hidDestinationIndex");
	var destinationIndex = hidDestinationIndex.value;
	
	var containerControl = $get("divDestinations");
	var newHtml = '';

    for (k=0; k<pathsToShow; k++)
    {
        var currentPath = k+1;
        var currentSent = fieldsArray["sent"][k]!=null ? fieldsArray["sent"][k] : '0';
		newHtml += '<div class="csLabel">No. of Emails Sent:</div><div class="csText"><input type="text" name="txtSent' + k + '" id="txtSent' + k + '" value="' + currentSent + '" class="csInput center" onblur="validateInt(this,0)"/></div>';
    	newHtml += '<div class="csLabelShort center">Email #:</div><div style="float:left"><input type="text" value="' + currentPath + '" class="csInput center" readonly="readonly"/></div>';
    	newHtml += '<div style="clear:both">&nbsp;</div>';
        
        newHtml += '<div id="divLandingPages' + k + '" ' + (destinationIndex!=1 ? ' style="display:none"' : '') + '>'
	    newHtml += addLandingPageSection(fieldsArray, destinationToShow, k, k + '_');
		newHtml += '</div>';
		
		newHtml += '<div id="divOffers">';
	    newHtml += addOfferSection(fieldsArray, offersToShow, k, k + '_');
		newHtml += '</div>';
        newHtml += '<div style="clear:both">&nbsp;</div>';
	}
	containerControl.innerHTML = newHtml;
    for (j=0; j<pathsToShow; j++)
    {
	    clickInactiveLPAll(destinationToShow,'LP' + j + '_');
    	clickInactiveOfferAll(offersToShow,j + '_');
    }
}


function saveFieldsExtraTokens(startValue)
{
	fieldsArray = new Array();
	var current = typeof startValue !== 'undefined' ? startValue : 1;
	while (current!=-1)
	{
        var nameField = $get("txtExtraTokenName" + current);
	    var urlField = $get("txtExtraTokenUrl" + current);
	    var paramField = $get("txtExtraTokenParam" + current);
	    var lpField = $get("chkExtraTokenLP" + current);
	    var offerField = $get("chkExtraTokenOffer" + current);
	    var tokenIDField = $get("hidExtraTokenID" + current);
	    if (nameField != null && urlField != null && paramField != null)
	    {
	        fieldsArray.push({'name':nameField.value, 'url':urlField.value, 'param':paramField.value,
	            'lp':(lpField!=null ? lpField.checked : true), 'offer':(offerField!=null ? offerField.checked : ''),
	            'tokenID':(tokenIDField!=null ? tokenIDField.value : '') });
		    current++;
	    }
		else
			current = -1;
	}
}

function saveFieldsTrackings()
{
	fieldsArray = new Array();
	var current=0;
	while (current!=-1)
	{
        var codeField = $get("txtTrackingCode" + current);
	    var idField = $get("hidTrackingID" + current);
	    var offersField = $get("txtTrackingOffers" + current);
	    if (codeField != null && idField != null && offersField != null)
	    {
	        fieldsArray[current] = {'id':idField.value, 'code':codeField.value, 'offers':offersField.value };
		    current++;
	    }
		else
			current = -1;
	}
}

function addExtraToken()
{
	saveFieldsExtraTokens();
	var hidExtraTokensToShow = $get("hidExtraTokensToShow");
	var extraTokensToShow = hidExtraTokensToShow.value;
	extraTokensToShow=parseInt(extraTokensToShow)+1;
	if (extraTokensToShow > 10)
	    extraTokensToShow = 10;
	hidExtraTokensToShow.value = extraTokensToShow;
	var containerControl = $get("divExtraTokensContainer");
	var newHtml = '';
	var tokenOptionsControl = $get("ddlExtraTokenSelect1");
    var tokenOptions = tokenOptionsControl != null ? tokenOptionsControl.innerHTML : '<option value="0">Select...</option>';
    var styleCode = tokenOptionsControl != null ? 4 : 3;
    for (i=1; i<=extraTokensToShow; i++)
    {
        nameValue = '';
        urlValue = '';
        paramValue = '';
        lpValue = true;
        offerValue = false;
        if (i<=fieldsArray.length)
        {
            nameValue = fieldsArray[i-1]["name"];
            urlValue = fieldsArray[i-1]["url"];
            paramValue = fieldsArray[i-1]["param"];
            lpValue = fieldsArray[i-1]["lp"];
            offerValue = fieldsArray[i-1]["offer"];
        }
        newHtml += '<div class="csLabel">Extra Token ' + i + ':';
        if (i==extraTokensToShow && i!=10)
            newHtml += '&nbsp;&nbsp;<a href="javascript:;" onclick="addExtraToken()">(add)</a>'
        newHtml += '&nbsp;<img src="images/spinner.gif" alt="Working..." id="spine' + i + '" style="width:12px; display:none"/></div>';
        if (tokenOptionsControl!=null)
            newHtml += '<div class="etText4s"><select id="ddlExtraTokenSelect' + i + '" onchange="ajaxGetTokenDetails(this)">' + tokenOptions + '</select></div>';
        newHtml += '<div class="etText' + styleCode + '"><input name="txtExtraTokenName' + i + '" id="txtExtraTokenName' + i + '" type="text" value="' + htmlEntities(nameValue) + '"/></div>';
        newHtml += '<div class="etText' + styleCode + '"><input name="txtExtraTokenUrl' + i + '" id="txtExtraTokenUrl' + i + '" type="text" value="' + htmlEntities(urlValue) + '" onblur="updateDestinationUrl()"/></div>';
        newHtml += '<div class="etText' + styleCode + '"><input name="txtExtraTokenParam' + i + '" id="txtExtraTokenParam' + i + '" type="text" value="' + htmlEntities(paramValue) + '" onblur="checkExtraTokenParam(this)"/></div>';
        newHtml += '<div class="etText' + styleCode + '2"><input name="chkExtraTokenLP' + i + '" id="chkExtraTokenLP' + i + '" type="checkbox" ' + (lpValue ? 'checked="checked" ' : '') + '/></div>';
        newHtml += '<div class="etText' + styleCode + '2"><input name="chkExtraTokenOffer' + i + '" id="chkExtraTokenOffer' + i + '" type="checkbox" ' + (offerValue ? 'checked="checked" ' : '') + '/></div>';
        newHtml += '<div style="clear:both"></div>';
    }
	containerControl.innerHTML = newHtml;
}

function addExtraTokenSources()
{
	saveFieldsExtraTokens(0);
	var hidExtraTokensToShow = $get("hidExtraTokensToShow");
	var extraTokensToShow = hidExtraTokensToShow.value;
	extraTokensToShow=parseInt(extraTokensToShow)+1;
	hidExtraTokensToShow.value = extraTokensToShow;
	var containerControl = $get("divExtraTokensContainer");
	var newHtml = '';
    for (i=0; i<extraTokensToShow; i++)
    {
        nameValue = '';
        urlValue = '';
        paramValue = '';
        tokenIDValue = '';
        if (i<fieldsArray.length)
        {
            nameValue = fieldsArray[i]["name"];
            urlValue = fieldsArray[i]["url"];
            paramValue = fieldsArray[i]["param"];
            tokenIDValue = fieldsArray[i]["tokenID"];
        }
        displayNumber = i + 1;
        newHtml += '<div class="fdLabel">Token ' + displayNumber + ':';
        if (displayNumber==extraTokensToShow)
            newHtml += '&nbsp;&nbsp;<a href="javascript:;" onclick="addExtraTokenSources()">(add)</a>'
        newHtml += '</div>';
        newHtml += '<div class="etText3"><input name="txtExtraTokenName' + i + '" id="txtExtraTokenName' + i + '" type="text" value="' + htmlEntities(nameValue) + '"/></div>';
        newHtml += '<div class="etText3"><input name="txtExtraTokenUrl' + i + '" id="txtExtraTokenUrl' + i + '" type="text" value="' + htmlEntities(urlValue) + '"/></div>';
        newHtml += '<div class="etText3"><input name="txtExtraTokenParam' + i + '" id="txtExtraTokenParam' + i + '" type="text" value="' + htmlEntities(paramValue) + '" onblur="checkExtraTokenParam(this)"/></div>';
        newHtml += '<input type="hidden" name="hidExtraTokenID' + i + '" id="hidExtraTokenID' + i + '" value="' + tokenIDValue + '" />';
        newHtml += '<div style="clear:both"></div>';
    }
	containerControl.innerHTML = newHtml;
}

function addTracking()
{
	saveFieldsTrackings();
	var hidPixelsToShow = $get("hidPixelsToShow");
	var pixelsToShow = hidPixelsToShow.value;
	pixelsToShow=parseInt(pixelsToShow)+1;
	hidPixelsToShow.value = pixelsToShow;
	var containerControl = $get("divTrackingsContainer");
	var newHtml = '';
    for (i=0; i<pixelsToShow; i++)
    {
        codeValue = idValue = offersValue = '';
        if (i<fieldsArray.length)
        {
            codeValue = fieldsArray[i]["code"];
            idValue = fieldsArray[i]["id"];
            offersValue = fieldsArray[i]["offers"];
        }
        newHtml += '<div class="floatleft"><textarea name="txtTrackingCode' + i + '" id="txtTrackingCode' + i + '" class="csInputStepPixel" rows="2">';
        newHtml += codeValue;
        newHtml += '</textarea>';
        newHtml += '<input type="hidden" name="hidTrackingID' + i + '" id="hidTrackingID' + i + '" value="';
        newHtml += idValue;
        newHtml += '" /></div><div style="padding-left:10px" class="floatleft center">Offer IDs:<br/>';
        newHtml += '<input type="text" name="txtTrackingOffers' + i + '" id="txtTrackingOffers' + i + '" value="';
        newHtml += offersValue;
        newHtml += '" class="csInputStepOffer" /></div>';
        newHtml += '<div class="clear"></div>';
    }
	containerControl.innerHTML = newHtml;
}

function checkCampaignName()
{
    var nameControl = $get("txtName");
    if (nameControl!=null)
    {
        if (trim(nameControl.value)=='')
        {
            alert('Enter a Campaign Name before Saving the Campaign.');
            return false;
        }
    }
    return true;
}

function checkUrls(suffix, count, message)
{
    for(i=0;i<count;i++)
    {
        var pageControl = $get('txtPageName' + suffix + i);
        var urlControl = $get('txtPageUrl' + suffix + i);
        if (pageControl!=null && urlControl!=null && pageControl.value!='' && urlControl.value=='')
        {
            alert('Please enter an URL for ' + message + ' ' + (i + 1));
            return false;
        }
    }
    return true;
}

function sumShares(suffix, destinationToShow, message, checkTotalUrls)
{
   	var checkTotalUrlsValue = typeof checkTotalUrls !== 'undefined' ? checkTotalUrls : false;
    var totalShare = 0;
    var totalUrls = false;
    for (i=0; i<destinationToShow; i++)
    {
        var currentUrlControl = $get('txtPageUrl' + suffix + i);
	    var currentShareControl = $get('txtShare' + suffix + i);
        var currentInactiveControl = $get('chkInactive' + suffix + i);
        if (currentUrlControl!=null && trim(currentUrlControl.value)!='' && currentShareControl!=null && currentInactiveControl!=null && !currentInactiveControl.checked)
		    totalShare += (+currentShareControl.value);
        if (!totalUrls && currentUrlControl!=null && trim(currentUrlControl.value)!='' && currentInactiveControl!=null && !currentInactiveControl.checked)
            totalUrls = true;
    }
    if ((!checkTotalUrlsValue || totalUrls) && totalShare != 100)
    {
	    alert('Current total share for ' + message + ' is ' + totalShare + ' and should be 100. Please fix this!');
	    return false;
    }
    return true;
}

function checkSharesDirectLink()
{
    if (!checkCampaignName())
        return false;
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	var hidDestinationIndex = $get("hidDestinationIndex");
	var destinationIndex = hidDestinationIndex.value;
	
    if (destinationIndex!=2 && !checkUrls('LP',destinationToShow,'Landing Page'))
	    return false;
	if (!checkUrls('Offer',offersToShow,'Offer'))
	    return false;
	
    if (destinationIndex!=2 && !sumShares('LP', destinationToShow, 'Landing Pages'))
        return false;
    if (!sumShares('Offer', offersToShow, 'Offers'))
        return false;
	return true;
}

function checkSharesMultipleOptions()
{
    if (!checkCampaignName())
        return false;
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	var hidPathsToShow = $get("hidPathsToShow");
	var pathsToShow = hidPathsToShow.value;
	
	if (!checkUrls('LP',destinationToShow,'Landing Page'))
	    return false;
	for (j=0;j<pathsToShow;j++)
	    if (!checkUrls('Offer' + j + '_',offersToShow,'Option ' + (j+1) + ' Offer'))
	        return false;

    if (!sumShares('LP', destinationToShow, 'Landing Pages'))
	    return false;
	for (j=0; j<pathsToShow; j++)
	{
	    if (!sumShares('Offer' + j + '_', offersToShow, 'Option ' + (j+1) + ' Offers', true))
	        return false;
	}
	return true;
}

function checkSharesMultiplePaths()
{
    if (!checkCampaignName())
        return false;
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	var hidPathsToShow = $get("hidPathsToShow");
	var pathsToShow = hidPathsToShow.value;
	
	for (j=0;j<pathsToShow;j++)
	    if (!checkUrls('LP' + j + '_',destinationToShow,'Path ' + (j+1) + ' Landing Page'))
	        return false;
	for (j=0;j<pathsToShow;j++)
	    if (!checkUrls('Offer' + j + '_',offersToShow,'Path ' + (j+1) + ' Offer'))
	        return false;
	
	var totalShare = 0;
	for (j=0; j<pathsToShow; j++)
	{
	    var currentShareControl = $get('txtSharePath' + j);
        if (currentShareControl!=null)
		    totalShare += (+currentShareControl.value);
	}
	if (totalShare != 100)
	{
		alert('Current total share for Paths is ' + totalShare + ' and should be 100. Please fix this!');
		return false;
	}
	for (j=0; j<pathsToShow; j++)
	{
	    if (!sumShares('LP' + j + '_', destinationToShow, 'Path ' + (j+1) + ' Landing Pages', true))
		    return false;
	}
	for (j=0; j<pathsToShow; j++)
	{
	    if (!sumShares('Offer' + j + '_', offersToShow, 'Path ' + (j+1) + ' Offers', true))
	        return false;
	}
	return true;
}

function checkSharesPageSequence()
{
    if (!checkCampaignName())
        return false;
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	var hidPathsToShow = $get("hidPathsToShow");
	var pathsToShow = hidPathsToShow.value;
	var hidOptionsToShow = $get("hidOptionsToShow");
	var optionsToShow = hidOptionsToShow.value;
	var hidDestinationIndex = $get("hidDestinationIndex");
	var destinationIndex = hidDestinationIndex.value;
	var hidTrackingIndex = $get("hidTrackingIndex");
	var trackingIndex = hidTrackingIndex.value;
	
	for (j=0;j<pathsToShow;j++)
	    if (!checkUrls('LP' + j + '_',destinationToShow,'Level ' + (j+1) + ' Landing Page'))
	        return false;
	//TODO: something wrong below
    if (destinationIndex!=4 || trackingIndex!=3)
    {
	    for (j=0; j<optionsToShow; j++)
	    {
            if (!checkUrls('Offer' + j + '_',offersToShow,'Offer'))
                return false;
        }
    }

	for (j=0; j<pathsToShow; j++)
	{
        if (!sumShares('LP' + j + '_', destinationToShow, 'Level ' + (j+1), true))
		    return false;
	}
	if (destinationIndex==4 && trackingIndex==1)
	{
        if (!checkUrls('After',destinationToShow,'After Opt-In'))
            return false;
        if (!sumShares('After', destinationToShow, 'After Opt-In pages'))
	        return false;
    }
    if (destinationIndex!=4 || trackingIndex!=3)
    {
	    for (j=0; j<optionsToShow; j++)
	    {
            if (!sumShares('Offer' + j + '_', offersToShow, 'Option ' + (j+1) + ' Offers', true))
	            return false;
        }
    }
	return true;
}

function checkSharesLeadCapture()
{
    if (!checkCampaignName())
        return false;
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	var hidTrackingIndex = $get("hidTrackingIndex");
	var trackingIndex = hidTrackingIndex.value;
	
	if (!checkUrls('LP',destinationToShow,'Landing Page'))
	    return false;
	if (trackingIndex==1 && !checkUrls('After',destinationToShow,'After Opt-In'))
	    return false;
    if (trackingIndex!=4 && !checkUrls('Offer',offersToShow,'Offer'))
        return false;
	
	if (!sumShares('LP', destinationToShow, 'Landing Pages'))
		return false;
	if (trackingIndex==1 && !sumShares('After', destinationToShow, 'After Opt-In pages'))
	    return false;
    if (trackingIndex!=4 && !sumShares('Offer', offersToShow, 'Offers'))
	    return false;
	return true;
}

function checkSharesEmailFollowUp()
{
    if (!checkCampaignName())
        return false;
	var hidDestinationToShow = $get("hidDestinationToShow");
	var destinationToShow = hidDestinationToShow.value;
	var hidOffersToShow = $get("hidOffersToShow");
	var offersToShow = hidOffersToShow.value;
	var hidPathsToShow = $get("hidPathsToShow");
	var pathsToShow = hidPathsToShow.value;
	var hidDestinationIndex = $get("hidDestinationIndex");
	var destinationIndex = hidDestinationIndex.value;
	if (destinationIndex==1)
	{
	    for (j=0;j<pathsToShow;j++)
	        if (!checkUrls('LP' + j + '_',destinationToShow,'Email ' + (j+1) + ' Landing Page'))
	            return false;
	}
	for (j=0;j<pathsToShow;j++)
	    if (!checkUrls('Offer' + j + '_',offersToShow,'Email ' + (j+1) + ' Offer'))
	        return false;

	if (destinationIndex==1)
	{
	    for (j=0; j<pathsToShow; j++)
	    {
            if (!sumShares('LP' + j + '_', destinationToShow, 'Email ' + (j+1) + ' Landing Pages', true))
		        return false;
	    }
	}
	for (j=0; j<pathsToShow; j++)
	{
        if (!sumShares('Offer' + j + '_', offersToShow, 'Email ' + (j+1) + ' Offers', true))
	        return false;
	}
	return true;
}

function evenShares(suffix)
{
    var hidDestinationToShow = $get(suffix.indexOf('Offer') >= 0 ? 'hidOffersToShow' : 'hidDestinationToShow');
    var totalControls=hidDestinationToShow.value;
    var addShare=new Array();
    var removeShare=new Array();
    for (i=0; i<totalControls; i++)
    {
        var control = $get('txtPageUrl' + suffix + i);
        var controlShare = 'txtShare' + suffix + i;
        var controlInactive = $get('chkInactive' + suffix + i);
        if (control!=null)
        {
            if (control.value!='' && (controlInactive == null || !controlInactive.checked))
            {
                addShare[addShare.length] = controlShare;
            }
            else
            {
                removeShare[removeShare.length] = controlShare;
            }
        }
    }
    var mainShare= Math.round(100 / addShare.length);
    for(i=0; i<addShare.length; i++)
    {
        if (i==addShare.length-1)
            currentShare = 100 - (addShare.length-1) * mainShare;
        else
            currentShare = mainShare;
        $get(addShare[i]).value = currentShare;
    }
    for(i=0;i<removeShare.length; i++)
        $get(removeShare[i]).value=0;
}

function clickInactiveLP(senderChecked, suffix)
{
    var txtPageName = $get('txtPageName' + suffix);
    var txtPageID = $get('txtPageID' + suffix);
    var txtPageUrl = $get('txtPageUrl' + suffix);
    var txtShare = $get('txtShare' + suffix);
    txtShare.readOnly = senderChecked;
    txtPageName.style.color = txtPageID.style.color = txtPageUrl.style.color = txtShare.style.color = senderChecked ? "#777" : "";
    txtPageName.style.backgroundColor = txtPageID.style.backgroundColor = txtPageUrl.style.backgroundColor = txtShare.style.backgroundColor = senderChecked ? "#EBEBEB" : "";
    if (senderChecked)
        txtShare.value = 0;
}

function clickInactiveOffer(senderChecked, suffix)
{
    var txtPageNameOffer = $get('txtPageNameOffer' + suffix);
    var txtPayout = $get('txtPayout' + suffix);
    var txtPageUrlOffer = $get('txtPageUrlOffer' + suffix);
    var txtShareOffer = $get('txtShareOffer' + suffix);
    var ddlNetwork = $get('ddlNetwork' + suffix);
    var txtPageIDOffer = $get('txtPageIDOffer' + suffix);
    if (txtPageIDOffer == null)
        txtPageIDOffer = txtShareOffer;
    txtShareOffer.readOnly = senderChecked;
    txtPageNameOffer.style.color = txtPayout.style.color = txtPageUrlOffer.style.color = 
        txtShareOffer.style.color = ddlNetwork.style.color = txtPageIDOffer.style.color = senderChecked ? "#777" : "";
    txtPageNameOffer.style.backgroundColor = txtPayout.style.backgroundColor = txtPageUrlOffer.style.backgroundColor = 
        txtShareOffer.style.backgroundColor = ddlNetwork.style.backgroundColor = txtPageIDOffer.style.backgroundColor = senderChecked ? "#EBEBEB" : "";
    if (senderChecked)
        txtShareOffer.value = 0;
}

function clickInactiveLPAll(count, suffix)
{
    for (i=0; i<count; i++)
    {
        var chkInactive = $get('chkInactive' + suffix + i);
        if (chkInactive != null && chkInactive.checked)
            clickInactiveLP(chkInactive.checked, suffix + i);
    }
}

function clickInactiveOfferAll(count, suffix)
{
    for (i=0; i<count; i++)
    {
        var chkInactiveOffer = $get('chkInactiveOffer' + suffix + i);
        if (chkInactiveOffer != null && chkInactiveOffer.checked)
            clickInactiveOffer(chkInactiveOffer.checked, suffix + i);
    }
}

function validateInt(control, minV, maxV, defaultV)
{
   	var minValue = typeof minV !== 'undefined' ? minV : null;
   	var maxValue = typeof maxV !== 'undefined' ? maxV : null;
   	var defaultValue = typeof defaultV !== 'undefined' ? defaultV : null;
   	validateNumeric(control, minValue, maxValue, defaultValue, false)
}

function validateFloat(control, minV, maxV)
{
   	var minValue = typeof minV !== 'undefined' ? minV : null;
   	var maxValue = typeof maxV !== 'undefined' ? maxV : null;
   	var defaultValue = typeof defaultV !== 'undefined' ? defaultV : null;
   	validateNumeric(control, minValue, maxValue, defaultValue, true)
}

function validateNumeric(control, minValue, maxValue, defaultValue, floatCheck)
{
	var newValue = floatCheck ? parseFloat(control.value) : parseInt(control.value);
	if (minValue!=null && (isNaN(newValue) || newValue < minValue))
		newValue = defaultValue != null ? defaultValue : minValue;
	if (maxValue!=null && (isNaN(newValue) || newValue > maxValue))
		newValue = defaultValue != null ? defaultValue : maxValue;
	control.value = newValue;
}

function validateName(nameControl, message)
{
    var control = $get(nameControl);
	if (control.value == '')
	{
		alert('Please enter a name for the ' + message + '!');
		return false;
	}
	return true;
}

function writeCampaignName(control)
{
    var currentIndex = 1;
    var continueLoop = 1;
    while (continueLoop!=0)
    {
        var currentControl = $get('spCamp' + currentIndex);
        if (currentControl == null)
            continueLoop = 0;
        else
            currentControl.innerHTML = control.value;
        currentIndex++;
    }
}

function clickCaptureExtraTokens(control)
{
    control.checked ? $('#divExtraTokens').slideDown(700) : $('#divExtraTokens').slideUp(700);
}

function clickCostType(costTypeID)
{
    costTypeID == 2 ? $('#divAdToken').slideDown(700) : $('#divAdToken').slideUp(700);
}

function updateDestinationUrl()
{
    var txtAppendToken = $get('txtAppendToken');
    var txtCampaignUrl = $get('txtCampaignUrl');
    var radCostTypeCPC = $get('radCostTypeCPC');
    var chkExtraTokens = $get('chkExtraTokens');
    var hidBaseUrl = $get('hidBaseUrl');
    var urlValue = hidBaseUrl.value;
    if (txtAppendToken!=null)
        urlValue += txtAppendToken.value;
    if (radCostTypeCPC!=null && radCostTypeCPC.checked)
    {
        var txtAdTokenUrl = $get('txtAdTokenUrl');
        if (txtAdTokenUrl != null)
            urlValue += txtAdTokenUrl.value;
    }
    if (chkExtraTokens!=null && chkExtraTokens.checked)
    {
        for (i=0; i<=10; i++)
        {
            var txtExtraTokenUrl = $get('txtExtraTokenUrl' + i);
            if (txtExtraTokenUrl != null)
                urlValue += txtExtraTokenUrl.value;
        }
    }
    txtCampaignUrl.value = urlValue;
}

function checkExtraTokenParam(control)
{
    var notAllowed = [ 'c', 'key' ];
    var foundParam = '';
    i=0;
    while (foundParam == '' && i<notAllowed.length)
    {
        if (control.value == notAllowed[i])
            foundParam = control.value;
        else
            i++;
    }
    if (foundParam != '')
    {
        alert('Warning! \'' + foundParam + '\' cannot be used as a token!');
        control.value = '';
        setTimeout("$get('" + control.id + "').focus();",1);
    }
}

function trim(str) {
    return str.replace(/^\s*|\s*$|\n|\r/g,"");
}

function ajaxChangeCampaignDropdownCpv() {
    if ($('#spanCpvName').length==0 || $('#lblUpdateStatsCpv').length==0)
        return;
    
    $.ajax({url: "ajax/getCampaignDetailsCpv.php",
        data: {"campaignID": $('#ddlCampaign').val()},
        dataType: "json",
        type: "POST",
        success: function(response, textStatus, xhr)
        {            
            $('#spanCpvName').html(response['SourceName']);
            $('#lblUpdateStatsCpv').html('Update Stats ' + (response['CostTypeID'] == 2 ? 'CPC with Average CPC' : 'CPV with Average CPV') + ' from upload file');
        }
    });
}

function ajaxGetPredefinedSource() {
    var ddlvalue = $('#ddlPredefinedSource').val();
    if (ddlvalue==0)
        return;
    showSpinner("spin1");
    
    $.ajax({url: "ajax/getSource.php",
        data: {"cpvSourceID": ddlvalue},
        dataType: "json",
        type: "POST",
        success: function(response, textStatus, xhr)
        {
            var tokenOptions = '<option id="0">Select...</option>';
            $.each(response, function(key, value)
            {
                if (key == "Tokens")
                {
                    $.each(value, function(index, optionItem) {
                        tokenOptions += '<option value="' + optionItem["CpvSourceTokenID"] + '">' + optionItem["ExtraTokenName"] + '</option>';
                    });
                }
                else if (key == "CostTypeID")
                {
                    $('#radCostType' + (value == 2 ? "CPC" : "CPV")).prop("checked", true);
                    clickCostType(value);
                }
                else
                {
                    $('#txt' + key).val(value);
                }
            });
            $('.etText4s select').html(tokenOptions);
            updateDestinationUrl();
        },
        complete: function(xhr,status)
        {
            hideSpinner("spin1");
        }
    });
}

function ajaxOpenChart(ddlSource,reportTypeID,levelID) {
    var column = '';
    var columnS = '';
    if (ddlSource!=null)
    {
        if (ddlSource.id.indexOf('ddlChartColumnsS')>=0)
            columnS = $(ddlSource).val();
        else
            column = $(ddlSource).val();
    }
    
    $.ajax({url: "ajax/chartDisplay" + reportTypeID + ".php",
        data: {"camp": $('#ddlCampaigns').val(), "col": column, "colS": columnS, "level": levelID, 
            "whereClause": $('#hidWhereClause').length ? $('#hidWhereClause').val() : '', 
            "intw": $('#hidIntervalWhere').length ? $('#hidIntervalWhere').val() : ''},
        type: "POST",
        success: function(response, textStatus, xhr)
        {
            $('[name="divContainer' + reportTypeID + '-' + levelID + '"]').html(response);
        }
    });
}

function ajaxLoadOffers() {
    if ($('#ddlCampaignsRevenue').val() == null || $('#ddlCampaignsRevenue').val() == '')
        return;
    showSpinner("spin1");
    $('#ddlOffersRevenue').html('');
    
    $.ajax({url: "ajax/loadOffers.php",
        data: {"campaignID": $('#ddlCampaignsRevenue').val()},
        type: "POST",
        success: function(response, textStatus, xhr)
        {
            $('#ddlOffersRevenue').html(response);
        },
        complete: function(xhr,status)
        {
            hideSpinner("spin1");
        }
    });
}

function ajaxSetDefaultProfile(profileID,isAlert) {
    showSpinner("spin1");
    $.ajax({url: "ajax/setDefault" + (isAlert ? "Alert" : "") + "Profile.php",
        data: {"profileID": profileID},
        type: "POST",
        complete: function(xhr,status)
        {
            hideSpinner("spin1");
        }
    });
}

function ajaxGetTokenDetails(sender) {
    var ddlvalue = $(sender).val();
    if (ddlvalue==0)
        return;
    sender.selectedIndex = 0;
    var rowID = sender.id.replace("ddlExtraTokenSelect", "");
    showSpinner("spine" + rowID);
    
    $.ajax({url: "ajax/getSourceToken.php",
        data: {"cpvSourceTokenID": ddlvalue},
        dataType: "json",
        type: "POST",
        success: function(response, textStatus, xhr)
        {
            $.each(response, function(key, value)
            {
                $('#txt' + key + rowID).val(value);
            });
            updateDestinationUrl();
        },
        complete: function(xhr,status)
        {
            hideSpinner("spine" + rowID);
        }
    });
}

function select_all(control)
{
    control.focus();
    control.select();
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function addDatePickers()
{
	$(function() {
		$("#txtIntervalFrom").datepicker({
			showOn: "button",
			buttonImage: "images/calendar.gif",
			buttonImageOnly: true,
			showAnim: "slideDown",
			dateFormat: "mm/dd/yy",
			buttonText: ""
		});
		$("#txtIntervalTo").datepicker({
			showOn: "button",
			buttonImage: "images/calendar.gif",
			buttonImageOnly: true,
			showAnim: "slideDown",
			dateFormat: "mm/dd/yy",
			buttonText: ""
		});
	});
}

function addDatePicker(controlName)
{
	$(function() {
		$( "#" + controlName ).datepicker({
			showOn: "button",
			buttonImage: "images/calendar.gif",
			buttonImageOnly: true,
			showAnim: "slideDown",
			dateFormat: "mm/dd/yy",
			buttonText: ""
		});
	});
}

function openDeleteWindow(objectid)
{
    closeOpenWindows();
    $('#divModal' + objectid).fadeIn(500);
    $('#hidDeleteID').val(objectid);
}
        
function closeOpenWindows()
{
    $('.divModal').fadeOut(500);
}

function redirectToCampaigns()
{
    window.location.href='campaigns.php';
    return false;
}

function setToCustom()
{
	var intervalField = $get("ddlInterval");
	intervalField.selectedIndex=7;
}

function checkInputCount()
{
    var maxInputsControl = $("#hidInputCount");
    if (maxInputsControl.length > 0)
    {
        var maxInputs = maxInputsControl.val();
        if (maxInputs > 0)
        {
            var currentInputs = $('input').length;
            if (maxInputs - currentInputs < 50)
                $("#divMaxInputs").fadeIn(500);
        }
    }
}

function showSpinner(name)
{
    $('#' + name).fadeIn(10);
}

function hideSpinner(name)
{
    $('#' + name).fadeOut(10);
}

function hideSection(control, section)
{
	var sectionControl = $('#' + section);
	if (sectionControl.length>0)
	{
		var shouldHide = control.src.indexOf('minus.gif') != -1;
		sectionControl.slideToggle(700);
		control.src = 'images/' + (shouldHide ? "plus.gif" : "minus.gif");
	}
}
