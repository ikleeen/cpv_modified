<?php
/***********************************************
*        CPV 2.6 FULL DECODED & NULLED         *
*        MTIMER     www.mtimer.net             *
***********************************************/

if (!isset($GLOBALS['__GETDATA_INC__'])) {
	$GLOBALS['__GETDATA_INC__'] = 1;
	function computeDataTableStats(&$reportLines, $currentCpvCampaign, $withOpt, $crFromViews) {
		$countReportLines = count($reportLines);
		$i = 0;

		while ($i < $countReportLines) {
			$currentViews = (isset($reportLines[$i]['Views'])?$reportLines[$i]['Views']:0);
			$currentCpv = ($currentViews == 0?0:$currentCpvCampaign);
			$currentEngages = (isset($reportLines[$i]['Engages'])?$reportLines[$i]['Engages']:0);
			$currentRevenue = (isset($reportLines[$i]['Revenue'])?$reportLines[$i]['Revenue']:0);

			if ($currentViews < $currentEngages) {
				$currentEngages = $currentViews;
			}

			$currentCost = $currentViews * $currentCpv;
			$currentClicks = ((isset($reportLines[$i]['Clicks']) && $reportLines[$i]['Clicks'] != "")?$reportLines[$i]['Clicks']:0);
			$currentSubscribers = (isset($reportLines[$i]['Subscribers'])?$reportLines[$i]['Subscribers']:0);

			if ($currentViews) {
				$currentEngageRate = floatval($currentEngages) * 100 / $currentViews;
			}
			else {
				$currentEngageRate = 0;
			}


			if ($currentViews) {
				$currentCtr = floatval($currentClicks) * 100 / $currentViews;
			}
			else {
				$currentCtr = 0;
			}


			if ($currentViews) {
				$currentSr = floatval($currentSubscribers) * 100 / $currentViews;
			}
			else {
				$currentSr = 0;
			}


			if ($currentClicks) {
				$currentCpc = floatval($currentCost) / $currentClicks;
			}
			else {
				$currentCpc = 0;
			}


			if ($currentSubscribers) {
				$currentCps = floatval($currentCost) / $currentSubscribers;
			}
			else {
				$currentCps = 0;
			}

			$currentConversions = ((isset($reportLines[$i]['Conversion']) && $reportLines[$i]['Conversion'] != "")?$reportLines[$i]['Conversion']:0);

			if ($currentConversions) {
				$currentCpa = floatval($currentCost) / $currentConversions;
			}
			else {
				$currentCpa = 0;
			}


			if ($crFromViews) {
				if ($currentViews) {
					$currentCr = floatval($currentConversions) * 100 / $currentViews;
				}
				else {
					$currentCr = 0;
				}
			}
			else {
				if ($currentClicks) {
					$currentCr = floatval($currentConversions) * 100 / $currentClicks;
				}
				else {
					$currentCr = 0;
				}
			}


			if ($currentViews) {
				$currentEpv = floatval($currentRevenue) / $currentViews;
			}
			else {
				$currentEpv = 0;
			}

			$currentPpv = $currentEpv - $currentCpv;
			$currentPl = $currentRevenue - $currentCost;

			if ($currentViews) {
				$currentEcpm = floatval($currentPl) * 1000 / $currentViews;
			}
			else {
				$currentEcpm = 0;
			}


			if ($currentCost) {
				$currentROI = floatval($currentPl) * 100 / $currentCost;
			}
			else {
				$currentROI = 0;
			}

			$currentSent = (isset($reportLines[$i]['Sent'])?$reportLines[$i]['Sent']:0);

			if ($currentSent) {
				$currentEps = floatval($currentRevenue) / $currentSent;
			}
			else {
				$currentEps = 0;
			}

			$reportLines[$i]['EngageRate'] = $currentEngageRate;
			$reportLines[$i]['CPV'] = $currentCpv;
			$reportLines[$i]['Cost'] = $currentCost;
			$reportLines[$i]['CTR'] = $currentCtr;
			$reportLines[$i]['CPC'] = $currentCpc;
			$reportLines[$i]['SR'] = $currentSr;
			$reportLines[$i]['CPSUB'] = $currentCps;
			$reportLines[$i]['CPA'] = $currentCpa;
			$reportLines[$i]['CR'] = $currentCr;
			$reportLines[$i]['EPV'] = $currentEpv;
			$reportLines[$i]['PPV'] = $currentPpv;
			$reportLines[$i]['Profit'] = $currentPl;
			$reportLines[$i]['eCPM'] = $currentEcpm;
			$reportLines[$i]['ROI'] = $currentROI;
			$reportLines[$i]['EPS'] = $currentEps;

			if ($withOpt) {
				if ($currentViews) {
					$reportLines[$i]['EmbedPercent'] = floatval($reportLines[$i]['Embed']) * 100 / $currentViews;
					$reportLines[$i]['PopupPercent'] = floatval($reportLines[$i]['Popup']) * 100 / $currentViews;
					$reportLines[$i]['ExitPercent'] = floatval($reportLines[$i]['Exit']) * 100 / $currentViews;
				}
			}

			$i++;
		}

	}

	function computeDataTableReports(&$reportLines) {
		$countReportLines = count($reportLines);
		$i = 0;

		while ($i < $countReportLines) {
			$currentViews = $reportLines[$i]['Views'];
			$currentClicks = $reportLines[$i]['Clicks'];
			$currentConversions = $reportLines[$i]['Conversion'];
			$currentCost = $reportLines[$i]['Cost'];
			$currentRevenue = $reportLines[$i]['Revenue'];

			if ($currentViews == 0) {
				$currentCpv = 0;
			}
			else {
				$currentCpv = $currentCost / $currentViews;
			}


			if ($currentViews) {
				$currentCtr = floatval($currentClicks) * 100 / $currentViews;
			}
			else {
				$currentCtr = 0;
			}


			if ($currentClicks) {
				$currentCpc = floatval($currentCost) / $currentClicks;
			}
			else {
				$currentCpc = 0;
			}


			if ($currentViews) {
				$currentCr = floatval($currentConversions) * 100 / $currentViews;
			}
			else {
				$currentCr = 0;
			}


			if ($currentConversions) {
				$currentCpa = floatval($currentCost) / $currentConversions;
			}
			else {
				$currentCpa = 0;
			}


			if ($currentViews) {
				$currentEpv = floatval($currentRevenue) / $currentViews;
			}
			else {
				$currentEpv = 0;
			}

			$currentPpv = $currentEpv - $currentCpv;
			$currentPl = $currentRevenue - $currentCost;

			if ($currentViews) {
				$currentEcpm = floatval($currentPl) * 1000 / $currentViews;
			}
			else {
				$currentEcpm = 0;
			}


			if ($currentCost) {
				$currentROI = floatval($currentPl) * 100 / $currentCost;
			}
			else {
				$currentROI = 0;
			}


			if ($currentViews) {
				$currentEps = floatval($currentRevenue) / $currentViews;
			}
			else {
				$currentEps = 0;
			}

			$reportLines[$i]['CPV'] = $currentCpv;
			$reportLines[$i]['CTR'] = $currentCtr;
			$reportLines[$i]['CPC'] = $currentCpc;
			$reportLines[$i]['CPA'] = $currentCpa;
			$reportLines[$i]['CR'] = $currentCr;
			$reportLines[$i]['EPV'] = $currentEpv;
			$reportLines[$i]['PPV'] = $currentPpv;
			$reportLines[$i]['Profit'] = $currentPl;
			$reportLines[$i]['eCPM'] = $currentEcpm;
			$reportLines[$i]['ROI'] = $currentROI;
			$reportLines[$i]['EPS'] = $currentEps;
			$i++;
		}

	}

	function computeDataTableCampaignStats(&$reportLines) {
		$countReportLines = count($reportLines);
		$i = 0;

		while ($i < $countReportLines) {
			$currentViews = (isset($reportLines[$i]['Views'])?$reportLines[$i]['Views']:0);
			$currentCost = (isset($reportLines[$i]['Cost'])?$reportLines[$i]['Cost']:0);
			$currentRevenue = (isset($reportLines[$i]['Revenue'])?$reportLines[$i]['Revenue']:0);
			$currentClicks = (isset($reportLines[$i]['Clicks'])?$reportLines[$i]['Clicks']:0);
			$currentSubscribers = (isset($reportLines[$i]['Subscribers'])?$reportLines[$i]['Subscribers']:0);
			$currentPl = $reportLines[$i]['Profit'] = $currentRevenue - $currentCost;
			$currentSent = (isset($reportLines[$i]['Sent'])?$reportLines[$i]['Sent']:0);

			if ($currentViews) {
				$currentEpv = floatval($currentRevenue) / $currentViews;
				$currentPpv = floatval($currentPl) / $currentViews;
			}
			else {
				$currentEpv = $currentPpv = 0;
			}


			if ($currentCost) {
				$currentROI = floatval($currentPl) * 100 / $currentCost;
			}
			else {
				$currentROI = 0;
			}


			if ($currentViews) {
				$currentCtr = floatval($currentClicks) * 100 / $currentViews;
			}
			else {
				$currentCtr = 0;
			}


			if ($currentViews) {
				$currentSr = floatval($currentSubscribers) * 100 / $currentViews;
			}
			else {
				$currentSr = 0;
			}


			if ($currentClicks) {
				$currentCpc = floatval($currentCost) / $currentClicks;
			}
			else {
				$currentCpc = 0;
			}


			if ($currentSubscribers) {
				$currentCps = floatval($currentCost) / $currentSubscribers;
			}
			else {
				$currentCps = 0;
			}


			if ($currentSent) {
				$currentEps = floatval($currentRevenue) / $currentSent;
			}
			else {
				$currentEps = 0;
			}

			$currentConversions = (isset($reportLines[$i]['Conversion'])?$reportLines[$i]['Conversion']:0);

			if ($currentConversions) {
				$currentCpa = floatval($currentCost) / $currentConversions;
			}
			else {
				$currentCpa = 0;
			}


			if ($currentViews) {
				$currentCr = floatval($currentConversions) * 100 / $currentViews;
			}
			else {
				$currentCr = 0;
			}

			$reportLines[$i]['EPV'] = $currentEpv;
			$reportLines[$i]['PPV'] = $currentPpv;
			$reportLines[$i]['ROI'] = $currentROI;
			$reportLines[$i]['CTR'] = $currentCtr;
			$reportLines[$i]['CPC'] = $currentCpc;
			$reportLines[$i]['SR'] = $currentSr;
			$reportLines[$i]['CPSUB'] = $currentCps;
			$reportLines[$i]['Conversion'] = $currentConversions;
			$reportLines[$i]['CPA'] = $currentCpa;
			$reportLines[$i]['CR'] = $currentCr;
			$reportLines[$i]['Sent'] = $currentSent;
			$reportLines[$i]['EPS'] = $currentEps;
			$i++;
		}

	}

	function sortDataTable(&$reportLines, $sortColumn, $sortDirection) {
		$countReportLines = count($reportLines);

		if ($countReportLines < 2 || !isset($reportLines[0][$sortColumn])) {
			return null;
		}

		$changeMade = 1;

		while ($changeMade) {
			$changeMade = 0;
			$i = 0;

			while ($i < $countReportLines - 1) {
				if ((isset($reportLines[$i][$sortColumn]) && isset($reportLines[$i + 1][$sortColumn])) && changeNeeded($reportLines[$i][$sortColumn], $reportLines[$i + 1][$sortColumn], $sortDirection)) {
					$tempVar = $reportLines[$i];
					$reportLines[$i] = $reportLines[$i + 1];
					$reportLines[$i + 1] = $tempVar;
					$changeMade = 1;
				}

				$i++;
			}
		}

	}

	function changeNeeded($firstValue, $secondValue, $sortDirection) {
		return $sortDirection == "desc"?$firstValue < $secondValue:$secondValue < $firstValue;
	}

	function getReportsTarget($app, $selectedReportID, $campaignTypeID, $sortField, $dirField) {
		if ($campaignTypeID == 9) {
			$fields = array("CampaignID", "concat('Email #', CAST(SubIdID AS char)) AS Offer", "concat('Email #', CAST(SubIdID AS char)) AS Email", "\'' AS Url", "\'' AS Keyword", "Views", "EngageRate", "Cost", "CPV", "Clicks", "CTR", "CPC", "Conversion", "CR", "CPA", "Revenue", "EPV", "PPV", "eCPM", "Profit", "ROI", "Sent", "EPS");
			$reportLines = $app->shared_db->get("reportdetailsgroup rd inner join reports r on rd.ReportID=r.ReportID", $fields, $count, "rd.ReportID=" . $selectedReportID, trim($sortField . " " . $dirField));
		}
		else {
			$fields = array("CampaignID", "\'' AS Offer", "\'' AS Url", "Keyword", "Views", "EngageRate", "Cost", "CPV", "Clicks", "CTR", "CPC", "Conversion", "CR", "CPA", "Revenue", "EPV", "PPV", "eCPM", "Profit", "ROI");
			$reportLines = $app->shared_db->get("reportdetailsgroup rd inner join subids s on rd.SubIdID=s.SubIdID
						inner join reports r on rd.ReportID=r.ReportID", $fields, $count, "rd.ReportID=" . $selectedReportID, trim($sortField . " " . $dirField));
		}

		return $reportLines;
	}

	function getReportsCampaign($app, $selectedReportID, $campaignTypeID) {
		$fields = array("rd.CampaignID", "CampaignName AS Offer", "Views", "Cost", "Revenue", "EPV", "PPV", "Clicks", "CTR", "Conversion", "CR", "CPA", "Profit", "ROI", "EPS");
		$reportLines = $app->shared_db->get("reportdetailscampaign rd inner join campaigns c on rd.CampaignID=c.CampaignID", $fields, $count, "ReportID=" . $selectedReportID);
		return $reportLines;
	}

	function getStatsTarget($app, $selectedCampaignID, $campaignTypeID, $currentCpv, $sortInDatabase, $sortField, $dirField, $groupClauseArray, $whereClause, $recordsDisplayed, $timezoneOffset = 0) {
		$reportLines = getStatsTargetSwitch($app, $selectedCampaignID, $campaignTypeID, $sortInDatabase, $sortField, $groupClauseArray, $whereClause, "ConversionDate", true, $recordsDisplayed, $timezoneOffset);
		computeDataTableStats($reportLines, $currentCpv, false, true);

		if ($sortInDatabase == "" && $sortField != "") {
			sortDataTable($reportLines, $sortField, $dirField);

			if (0 < $recordsDisplayed) {
				$countReportLines = count($reportLines);
				$i = $recordsDisplayed;

				while ($i < $countReportLines) {
					unset($reportLines[$i]);
					$i++;
				}

				$reportLines = array_values($reportLines);
			}
		}

		return $reportLines;
	}

	function getStatsTargetDefault($app, $selectedCampaignID, $campaignTypeID, $currentCpv, $whereClause, $recordsDisplayed) {
		$reportLines = getStatsTargetSwitch($app, $selectedCampaignID, $campaignTypeID, "", "", array("s.SubIdID"), $whereClause, "ConversionDate", true, $recordsDisplayed);
		computeDataTableStats($reportLines, $currentCpv, false, true);
		return $reportLines;
	}

	function getStatsTargetForReports($app, $selectedCampaignID, $campaignTypeID, $currentCpv, $sortInDatabase, $sortField, $dirField, $whereClause) {
		$reportLines = getStatsTargetSwitch($app, $selectedCampaignID, $campaignTypeID, $sortInDatabase, $sortField, array("s.SubIdID"), $whereClause, "ConversionDateReport", false, 0);
		computeDataTableStats($reportLines, $currentCpv, false, true);
		return $reportLines;
	}

	function getStatsTargetSwitch($app, $selectedCampaignID, $campaignTypeID, $sortInDatabase, $sortField, $groupClauseArray, $whereClause, $conversionDateField, $withAllMailings, $recordsDisplayed, $timezoneOffset = 0) {
		if ($sortInDatabase == "" && $sortField != "") {
			$recordsDisplayed = 0;
		}

		$groupClause = implode(",", $groupClauseArray);
		$joinExtra = $joinReferrers = $joinReferrerDomains = $joinSiteCategory = false;
		switch (false) {
			default:
				$fields = array("rd.CampaignID");

				if (in_array("s.SubIdID", $groupClauseArray)) {
					array_push($fields, "s.SubIdID", "Keyword");
				}


				if (in_array("ViewDay", $groupClauseArray)) {
					array_push($fields, "date(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND)) AS ViewDay");
				}


				if (in_array("ViewHour", $groupClauseArray)) {
					array_push($fields, "hour(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND)) AS ViewHour");
				}


				if (in_array("d.DestinationID", $groupClauseArray)) {
					array_push($fields, "d.DestinationID", "d.Offer", "d.Url");
				}


				if (in_array("d2.DestinationID", $groupClauseArray)) {
					array_push($fields, "d2.DestinationID AS DestinationID2", "COALESCE(d2.Offer,'') AS Offer2", "COALESCE(d2.Url,'') AS Url2");
				}


				if (in_array("r.ReferrerID", $groupClauseArray)) {
					array_push($fields, "Referrer");
					$joinReferrers = true;
				}


				if (in_array("rr.ReferrerDomainID", $groupClauseArray)) {
					array_push($fields, "ReferrerDomain");
					$joinReferrers = true;
					$joinReferrerDomains = true;
				}


				if (in_array("sc.SiteCategoryID", $groupClauseArray)) {
					array_push($fields, "COALESCE(SiteCategory,'" . $GLOBALS['CampaignTrafficCategoryText'] . "') AS SiteCategory");
					$joinSiteCategory = true;
				}

				$extraID = 1;

				while ($extraID <= 10) {
					if (in_array("Extra" . $extraID, $groupClauseArray)) {
						array_push($fields, "COALESCE(ce.Extra" . $extraID . ",'') AS Extra" . $extraID);
						$joinExtra = true;
					}

					$extraID++;
				}


				if (in_array("AdValue", $groupClauseArray)) {
					array_push($fields, "AdValue");
					$joinExtra = true;
				}

				array_push($fields, "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(" . $conversionDateField . " is null,0,1)) AS Revenue", "sum(if(ViewDate is null OR IsDup=1,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(rd.ClickDate is null" . ($campaignTypeID == 6?" and clp.ClickDate is null":"") . ",0,1)) AS Clicks", "sum(if(" . $conversionDateField . " is null,0,1)) AS Conversion", "sum(if(SubscribeDate is null,0,1)) AS Subscribers");
				$reportLines = $app->shared_db->get("clicks rd inner join subids s on rd.SubIdID=s.SubIdID
							inner join destinations d on rd.DestinationID=d.DestinationID
							left outer join destinations d2 on rd.OfferID=d2.DestinationID " . ($campaignTypeID == 6?"left outer join clickslp clp on rd.ClickID=clp.ClickID and clp.Level=2 ":"") . ($joinExtra?"left outer join clicksextra ce on rd.ClickID=ce.ClickID ":"") . ($joinReferrers?"inner join referrers r on rd.ReferrerID=r.ReferrerID ":"") . ($joinReferrerDomains?"inner join referrerdomains rr on r.ReferrerDomainID=rr.ReferrerDomainID ":"") . ($joinSiteCategory?"left outer join sitecategories sc on rd.SiteCategoryID=sc.SiteCategoryID ":""), $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause . " " . ($groupClause?"GROUP BY " . $groupClause:""), $sortInDatabase, $recordsDisplayed, 1);
				break;

			case 9:
				$fields = array("rd.CampaignID", "concat('Email #', CAST(d.PathID AS char)) AS Offer", "concat('Email #', CAST(d.PathID AS char)) AS Email", "d.PathID AS SubIdID", "\'' AS Keyword");

				if (in_array("d2.DestinationID", $groupClauseArray)) {
					array_push($fields, "COALESCE(d2.DestinationID,'') AS DestinationID2", "COALESCE(d2.Offer,'') AS Offer2", "COALESCE(d2.Url,'') AS Url2");
				}


				if (in_array("r.ReferrerID", $groupClauseArray)) {
					array_push($fields, "Referrer");
					$joinReferrers = true;
				}


				if (in_array("rr.ReferrerDomainID", $groupClauseArray)) {
					array_push($fields, "ReferrerDomain");
					$joinReferrers = true;
					$joinReferrerDomains = true;
				}


				if (in_array("sc.SiteCategoryID", $groupClauseArray)) {
					array_push($fields, "COALESCE(SiteCategory,'" . $GLOBALS['CampaignTrafficCategoryText'] . "') AS SiteCategory");
					$joinSiteCategory = true;
				}

				array_push($fields, "d.Sent", "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(" . $conversionDateField . " is null,0,1)) AS Revenue", "d.Sent AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(ViewDate is null,0,1)) AS Clicks", "sum(if(" . $conversionDateField . " is null,0,1)) AS Conversion");
				$reportLines = $app->shared_db->get("clicks rd inner join subids s on rd.SubIdID=s.SubIdID
							inner join destinations d on rd.DestinationID=d.DestinationID
							left outer join destinations d2 on rd.OfferID=d2.DestinationID " . ($joinReferrers?"inner join referrers r on rd.ReferrerID=r.ReferrerID ":"") . ($joinReferrerDomains?"inner join referrerdomains rr on r.ReferrerDomainID=rr.ReferrerDomainID ":"") . ($joinSiteCategory?"left outer join sitecategories sc on rd.SiteCategoryID=sc.SiteCategoryID ":""), $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause . " " . "GROUP BY d.PathID, d.Sent" . ($groupClause?"," . $groupClause:""), $sortInDatabase, $recordsDisplayed, 1);
				break;
				return $reportLines;
		}
	}

	function getStatsLanding($app, $selectedCampaignID, $campaignTypeID, $currentCpv, $sortInDatabase, $sortField, $dirField, $groupClauseArray, $maxLevel, $whereClause) {
		$reportLines = getStatsLandingSwitch($app, $selectedCampaignID, $campaignTypeID, $sortInDatabase, $groupClauseArray, $maxLevel, $whereClause, "ConversionDate");
		computeDataTableStats($reportLines, $currentCpv, false, true);

		if ($sortInDatabase == "" && $sortField != "") {
			sortDataTable($reportLines, $sortField, $dirField);
		}

		return $reportLines;
	}

	function getStatsLandingDefault($app, $selectedCampaignID, $campaignTypeID, $currentCpv, $maxLevel, $whereClause) {
		$reportLines = getStatsLandingSwitch($app, $selectedCampaignID, $campaignTypeID, "d.DestinationID", array("d.DestinationID"), $maxLevel, $whereClause, "ConversionDate");
		computeDataTableStats($reportLines, $currentCpv, false, true);
		return $reportLines;
	}

	function getStatsLandingSwitch($app, $selectedCampaignID, $campaignTypeID, $sortInDatabase, $groupClauseArray, $maxLevel, $whereClause, $conversionDateField) {
		$groupClause = implode(",", $groupClauseArray);
		switch ($campaignTypeID) {
			case 6:
				if (2 < $maxLevel) {
					$fields = array("rd.CampaignID");

					if (in_array("d.DestinationID", $groupClauseArray)) {
						array_push($fields, "d.DestinationID", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID");
					}


					if (in_array("d2.DestinationID", $groupClauseArray)) {
						array_push($fields, "d2.DestinationID AS DestinationID2", "COALESCE(d2.Offer,'') AS Offer2", "COALESCE(d2.Url,'') AS Url2");
					}

					array_push($fields, "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(" . $conversionDateField . " is null,0,1)) AS Revenue", "sum(if(ViewDate is null OR IsDup=1,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(lp.ClickDate is null,0,1)) AS Clicks", "sum(if(" . $conversionDateField . " is null,0,1)) AS Conversion");
					$dbsource = "clicks rd
							inner join destinations d on rd.DestinationID=d.DestinationID
							left outer join destinations d2 on rd.OfferID=d2.DestinationID
							left outer join clickslp lp on rd.ClickID=lp.ClickID AND lp.Level=2";
				}
				else {
					$fields = array("rd.CampaignID");

					if (in_array("d.DestinationID", $groupClauseArray)) {
						array_push($fields, "d.DestinationID", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID");
					}


					if (in_array("d2.DestinationID", $groupClauseArray)) {
						array_push($fields, "d2.DestinationID AS DestinationID2", "COALESCE(d2.Offer,'') AS Offer2", "COALESCE(d2.Url,'') AS Url2");
					}

					array_push($fields, "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(" . $conversionDateField . " is null,0,1)) AS Revenue", "sum(if(ViewDate is null OR IsDup=1,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(ClickDate is null,0,1)) AS Clicks", "sum(if(" . $conversionDateField . " is null,0,1)) AS Conversion");
					$dbsource = "clicks rd
							inner join destinations d on rd.DestinationID=d.DestinationID
							left outer join destinations d2 on rd.OfferID=d2.DestinationID";
				}

				break;

			default:
				$fields = array("rd.CampaignID");

				if (in_array("d.DestinationID", $groupClauseArray)) {
					array_push($fields, "d.DestinationID", "concat('Email #', CAST(d.PathID AS char)) AS Email", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID");
				}


				if (in_array("d2.DestinationID", $groupClauseArray)) {
					array_push($fields, "d2.DestinationID AS DestinationID2", "COALESCE(d2.Offer,'') AS Offer2", "COALESCE(d2.Url,'') AS Url2");
				}

				array_push($fields, "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(" . $conversionDateField . " is null,0,1)) AS Revenue", "sum(if(ViewDate is null OR IsDup=1,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(ClickDate is null,0,1)) AS Clicks", "sum(if(" . $conversionDateField . " is null,0,1)) AS Conversion", "sum(if(SubscribeDate is null,0,1)) AS Subscribers");
				$dbsource = "clicks rd
						inner join destinations d on rd.DestinationID=d.DestinationID
						left outer join destinations d2 on rd.OfferID=d2.DestinationID";
				break;
		}

		return $app->shared_db->get($dbsource, $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause . " " . ($campaignTypeID == 9?"AND rd.DestinationID<>rd.OfferID ":"") . ($groupClause?"GROUP BY " . $groupClause:""), $sortInDatabase);
	}

	function getStatsLandingLevel2CPV($app, $selectedCampaignID, $currentLevel, $whereClause, $campaignCost) {
		$fields = array("sum(if(lp.ClickDate is null,0,1)) AS Views");
		$dbsource = "clicks rd
				left outer join clickslp lp on rd.ClickID=lp.ClickID AND lp.Level=" . $currentLevel . "
				inner join destinations d on lp.DestinationID=d.DestinationID
				left outer join destinations d2 on rd.OfferID=d2.DestinationID";
		$views = $app->shared_db->get($dbsource, $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause);
		$views = (isset($views[0])?$views[0]:0);
		return $views?floatval($campaignCost) / $views:0;
	}

	function getCampaignCost($app, $selectedCampaignID, $whereClause, $currentCpv) {
		$fields = array("sum(if(ViewDate is null OR IsDup=1,0,1)) AS Views");
		$dbsource = "clicks rd inner join destinations d on rd.DestinationID=d.DestinationID";
		$views = $app->shared_db->get($dbsource, $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause);
		$views = (isset($views[0])?$views[0]:0);
		return $views * $currentCpv;
	}

	function getStatsLandingLevel2Default($app, $selectedCampaignID, $campaignTypeID, $campaignCost, $maxLevel, $currentLevel, $destinationType, $trackingType, $whereClause) {
		return getStatsLandingLevel2($app, $selectedCampaignID, $campaignTypeID, $campaignCost, "", "", "", array("d.DestinationID"), $maxLevel, $currentLevel, $destinationType, $trackingType, $whereClause);
	}

	function getStatsLandingLevel2($app, $selectedCampaignID, $campaignTypeID, $campaignCost, $sortInDatabase, $sortField, $dirField, $groupClauseArray, $maxLevel, $currentLevel, $destinationType, $trackingType, $whereClause) {
		$groupClause = implode(",", $groupClauseArray);

		if ($currentLevel == $maxLevel - 1) {
			$fields = array("rd.CampaignID");

			if (in_array("d.DestinationID", $groupClauseArray)) {
				array_push($fields, "d.DestinationID", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID");
			}


			if (in_array("d2.DestinationID", $groupClauseArray)) {
				array_push($fields, "d2.DestinationID AS DestinationID2", "COALESCE(d2.Offer,'') AS Offer2", "COALESCE(d2.Url,'') AS Url2");
			}

			array_push($fields, "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "sum(if(lp.ClickDate is null,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(rd.ClickDate is null and lp2.ClickDate is null,0,1)) AS Clicks", "sum(if(ConversionDate is null,0,1)) AS Conversion", "sum(if(SubscribeDate is null,0,1)) AS Subscribers");
			$dbsource = "clicks rd
					left outer join clickslp lp on rd.ClickID=lp.ClickID AND lp.Level=" . $currentLevel . "
					inner join destinations d on lp.DestinationID=d.DestinationID
					left outer join destinations d2 on rd.OfferID=d2.DestinationID
					left outer join clickslp lp2 on rd.ClickID=lp2.ClickID and lp2.Level=" . ($currentLevel + 1);
		}
		else {
			$fields = array("rd.CampaignID");

			if (in_array("d.DestinationID", $groupClauseArray)) {
				array_push($fields, "d.DestinationID", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID");
			}


			if (in_array("d2.DestinationID", $groupClauseArray)) {
				array_push($fields, "d2.DestinationID AS DestinationID2", "COALESCE(d2.Offer,'') AS Offer2", "COALESCE(d2.Url,'') AS Url2");
			}

			array_push($fields, "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "sum(if(lp.ClickDate is null,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(rd.ClickDate is null and lp2.ClickDate is null,0,1)) AS Clicks", "sum(if(ConversionDate is null,0,1)) AS Conversion", "sum(if(SubscribeDate is null,0,1)) AS Subscribers");
			$dbsource = "clicks rd
					left outer join clickslp lp on rd.ClickID=lp.ClickID AND lp.Level=" . $currentLevel . "
					inner join destinations d on lp.DestinationID=d.DestinationID
					left outer join destinations d2 on rd.OfferID=d2.DestinationID
					left outer join clickslp lp2 on rd.ClickID=lp2.ClickID and lp2.Level=" . ($currentLevel + 1);
		}

		$reportLines = $app->shared_db->get($dbsource, $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause . " " . ($groupClause?"GROUP BY " . $groupClause:""), $sortInDatabase);
		$currentCpv = getStatsLandingLevel2CPV($app, $selectedCampaignID, $currentLevel, $whereClause, $campaignCost);
		computeDataTableStats($reportLines, $currentCpv, false, true);

		if ($sortInDatabase == "" && $sortField != "") {
			sortDataTable($reportLines, $sortField, $dirField);
		}

		return $reportLines;
	}

	function getStatsLandingStepBDefault($app, $selectedCampaignID, $campaignTypeID, $campaignCost, $whereClause) {
		return getStatsLandingStepB($app, $selectedCampaignID, $campaignTypeID, $campaignCost, "", "", "", array("d.DestinationID"), $whereClause);
	}

	function getStatsLandingStepB($app, $selectedCampaignID, $campaignTypeID, $campaignCost, $sortInDatabase, $sortField, $dirField, $groupClauseArray, $whereClause) {
		$groupClause = implode(",", $groupClauseArray);
		$fields = array("rd.CampaignID");

		if (in_array("d.DestinationID", $groupClauseArray)) {
			array_push($fields, "d.DestinationID", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID");
		}


		if (in_array("d2.DestinationID", $groupClauseArray)) {
			array_push($fields, "d2.DestinationID AS DestinationID2", "COALESCE(d2.Offer,'') AS Offer2", "COALESCE(d2.Url,'') AS Url2");
		}

		array_push($fields, "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "sum(if(lp.ClickDate is null,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(rd.ClickDate is null,0,1)) AS Clicks", "sum(if(ConversionDate is null,0,1)) AS Conversion", "sum(if(SubscribeDate is null,0,1)) AS Subscribers");
		$dbsource = "clicks rd
				left outer join clickslp lp on rd.ClickID=lp.ClickID AND lp.Level=-1
				inner join destinations d on lp.DestinationID=d.DestinationID
				left outer join destinations d2 on rd.OfferID=d2.DestinationID";
		$reportLines = $app->shared_db->get($dbsource, $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause . " " . ($groupClause?"GROUP BY " . $groupClause:""), $sortInDatabase);
		$currentCpv = getStatsLandingLevel2CPV($app, $selectedCampaignID, 0 - 1, $whereClause, $campaignCost);
		computeDataTableStats($reportLines, $currentCpv, false, true);

		if ($sortInDatabase == "" && $sortField != "") {
			sortDataTable($reportLines, $sortField, $dirField);
		}

		return $reportLines;
	}

	function getStatsOptIn($app, $selectedCampaignID, $campaignTypeID, $currentCpv, $sortInDatabase, $sortField, $dirField, $maxLevel, $destinationType, $trackingType, $whereClause) {
		$currentLevel = $maxLevel - 1;

		if ($campaignTypeID == 6) {
			if ($destinationType == 4 && $trackingType == 1) {
				if (2 < $maxLevel) {
					$fields = array("rd.CampaignID", "d.DestinationID", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID", "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "sum(if(lp.ClickDate is null,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(rd.SubscribeDate is null,0,1)) AS Subscribers", "sum(if(ConversionDate is null,0,1)) AS Conversion", "sum(if(co.OptEmbedDate is null,0,1)) AS Embed", "sum(if(co.OptPopUpDate is null,0,1)) AS Popup", "sum(if(co.OptExitPopDate is null,0,1)) AS 'Exit'");
					$dbsource = "clicks rd
							left outer join clickslp lp on rd.ClickID=lp.ClickID AND lp.Level=" . $currentLevel . "
							left outer join clicksopt co on rd.ClickID=co.ClickID
							inner join destinations d on lp.DestinationID=d.DestinationID
							left outer join destinations d2 on rd.OfferID=d2.DestinationID";
				}
				else {
					$fields = array("rd.CampaignID", "d.DestinationID", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID", "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "sum(if(rd.ViewDate is null OR IsDup=1,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(rd.SubscribeDate is null,0,1)) AS Subscribers", "sum(if(ConversionDate is null,0,1)) AS Conversion", "sum(if(co.OptEmbedDate is null,0,1)) AS Embed", "sum(if(co.OptPopUpDate is null,0,1)) AS Popup", "sum(if(co.OptExitPopDate is null,0,1)) AS 'Exit'");
					$dbsource = "clicks rd
							left outer join clicksopt co on rd.ClickID=co.ClickID
							inner join destinations d on rd.DestinationID=d.DestinationID
							left outer join destinations d2 on rd.OfferID=d2.DestinationID";
				}
			}
			else {
				if (2 < $maxLevel) {
					$fields = array("rd.CampaignID", "d.DestinationID", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID", "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "sum(if(lp.ClickDate is null,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(rd.SubscribeDate is null,0,1)) AS Subscribers", "sum(if(ConversionDate is null,0,1)) AS Conversion", "sum(if(co.OptEmbedDate is null,0,1)) AS Embed", "sum(if(co.OptPopUpDate is null,0,1)) AS Popup", "sum(if(co.OptExitPopDate is null,0,1)) AS 'Exit'");
					$dbsource = "clicks rd
							left outer join clickslp lp on rd.ClickID=lp.ClickID AND lp.Level=" . $currentLevel . "
							left outer join clicksopt co on rd.ClickID=co.ClickID
							inner join destinations d on lp.DestinationID=d.DestinationID
							left outer join destinations d2 on rd.OfferID=d2.DestinationID";
				}
				else {
					$fields = array("rd.CampaignID", "d.DestinationID", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID", "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "sum(if(rd.ViewDate is null OR IsDup=1,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(rd.SubscribeDate is null,0,1)) AS Subscribers", "sum(if(ConversionDate is null,0,1)) AS Conversion", "sum(if(co.OptEmbedDate is null,0,1)) AS Embed", "sum(if(co.OptPopUpDate is null,0,1)) AS Popup", "sum(if(co.OptExitPopDate is null,0,1)) AS 'Exit'");
					$dbsource = "clicks rd
							left outer join clicksopt co on rd.ClickID=co.ClickID
							inner join destinations d on rd.DestinationID=d.DestinationID
							left outer join destinations d2 on rd.OfferID=d2.DestinationID";
				}
			}
		}
		else {
			$fields = array("rd.CampaignID", "d.DestinationID", "d.Offer", "d.Url", "d.Inactive", "d.LandingPageID", "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "sum(if(rd.ViewDate is null OR IsDup=1,0,1)) AS Views", "sum(if(EngageDate is null,0,1)) AS Engages", "sum(if(rd.SubscribeDate is null,0,1)) AS Subscribers", "sum(if(ConversionDate is null,0,1)) AS Conversion");
			$dbsource = "clicks rd
					inner join destinations d on rd.DestinationID=d.DestinationID
					left outer join destinations d2 on rd.OfferID=d2.DestinationID";
		}

		$reportLines = $app->shared_db->get($dbsource, $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause . " GROUP BY d.DestinationID", $sortInDatabase);
		computeDataTableStats($reportLines, $currentCpv, true, true);

		if ($sortInDatabase == "" && $sortField != "") {
			sortDataTable($reportLines, $sortField, $dirField);
		}

		return $reportLines;
	}

	function getStatsThankYou($app, $selectedCampaignID, $campaignTypeID, $campaignCost, $sortInDatabase, $sortField, $dirField, $maxLevel, $currentLevel, $destinationType, $trackingType, $whereClause) {
		if ($destinationType == 4 && $trackingType == 1) {
			$fields = array("rd.CampaignID", "\'Thank You Page' AS Offer", "\'' AS Url", "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "sum(if(rd.SubscribeDate is null,0,1)) AS Views", "sum(if(lp2.ClickDate is null,0,1)) AS Clicks", "sum(if(ConversionDate is null,0,1)) AS Conversion");
			$dbsource = "clicks rd
					inner join destinations d on rd.DestinationID=d.DestinationID
					left outer join destinations d2 on rd.OfferID=d2.DestinationID
					left outer join clickslp lp2 on rd.ClickID=lp2.ClickID and lp2.Level=-1";
		}
		else {
			$fields = array("rd.CampaignID", "\'Thank You Page' AS Offer", "\'' AS Url", "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "sum(if(rd.SubscribeDate is null,0,1)) AS Views", "sum(if(rd.ClickDate is null,0,1)) AS Clicks", "sum(if(ConversionDate is null,0,1)) AS Conversion");
			$dbsource = "clicks rd
					inner join destinations d on rd.DestinationID=d.DestinationID
					left outer join destinations d2 on rd.OfferID=d2.DestinationID";
		}

		$reportLines = $app->shared_db->get($dbsource, $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause, $sortInDatabase);
		$currentViews = (isset($reportLines[0]['Views'])?$reportLines[0]['Views']:0);
		$currentCpv = ($currentViews?floatval($campaignCost) / $currentViews:0);
		computeDataTableStats($reportLines, $currentCpv, false, true);

		if ($sortInDatabase == "" && $sortField != "") {
			sortDataTable($reportLines, $sortField, $dirField);
		}

		return $reportLines;
	}

	function getStatsOfferCPV($app, $selectedCampaignID, $whereClause, $campaignCost) {
		$fields = array("sum(if(ViewDate is null OR IsDup=1,0,1)) AS Views");
		$dbsource = "clicks rd inner join destinations d2 on rd.OfferID=d2.DestinationID";
		$views = $app->shared_db->get($dbsource, $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause);
		$views = (isset($views[0])?$views[0]:0);
		return $views?floatval($campaignCost) / $views:0;
	}

	function getStatsOffer($app, $selectedCampaignID, $campaignTypeID, $currentCpv, $campaignCost, $sortInDatabase, $sortField, $dirField, $groupClauseArray, $whereClause) {
		$reportLines = getStatsOfferSwitch($app, $selectedCampaignID, $sortInDatabase, $groupClauseArray, $whereClause, "ConversionDate");

		if ($campaignTypeID == 6 || $campaignTypeID == 7) {
			$currentCpv = getStatsOfferCPV($app, $selectedCampaignID, $whereClause, $campaignCost);
		}

		computeDataTableStats($reportLines, $currentCpv, false, false);

		if ($sortInDatabase == "" && $sortField != "") {
			sortDataTable($reportLines, $sortField, $dirField);
		}

		return $reportLines;
	}

	function getStatsOfferDefault($app, $selectedCampaignID, $campaignTypeID, $currentCpv, $campaignCost, $whereClause) {
		$reportLines = getStatsOfferSwitch($app, $selectedCampaignID, "", array("d2.DestinationID"), $whereClause, "ConversionDate");

		if ($campaignTypeID == 6 || $campaignTypeID == 7) {
			$currentCpv = getStatsOfferCPV($app, $selectedCampaignID, $whereClause, $campaignCost);
		}

		computeDataTableStats($reportLines, $currentCpv, false, false);
		return $reportLines;
	}

	function getStatsOfferSwitch($app, $selectedCampaignID, $sortInDatabase, $groupClauseArray, $whereClause, $conversionDateField) {
		$groupClause = implode(",", $groupClauseArray);
		$fields = array("rd.CampaignID", "concat('Email #', CAST(d2.PathID AS char)) AS Email", "d2.DestinationID", "Offer", "Url", "d2.Inactive", "d2.LandingPageID", "sum(COALESCE(rd.Revenue,d2.Payout,0) * if(" . $conversionDateField . " is null,0,1)) AS Revenue", "sum(if(ViewDate is null OR IsDup=1,0,1)) AS Views", "sum(if(ClickDate is null,0,1)) AS Clicks", "sum(if(" . $conversionDateField . " is null,0,1)) AS Conversion");
		$reportLines = $app->shared_db->get("clicks rd
					inner join destinations d2 on rd.OfferID=d2.DestinationID", $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause . " " . ($groupClause?"GROUP BY " . $groupClause:""), $sortInDatabase);
		return $reportLines;
	}

	function getStatsCampaign($app, $selectedCampaignID, $campaignTypeID, $campaignName, $currentCpv, $whereClause) {
		$reportLines = getStatsCampaignSwitch($app, $selectedCampaignID, $campaignTypeID, $campaignName, $currentCpv, "ConversionDate", $whereClause);
		computeDataTableStats($reportLines, $currentCpv, false, true);
		return $reportLines;
	}

	function getStatsCampaignSwitch($app, $selectedCampaignID, $campaignTypeID, $campaignName, $currentCpv, $conversionDateField, $whereClause) {
		$fields = array("sum(COALESCE(rd.Revenue,d2.Payout,0) * if(" . $conversionDateField . " is null,0,1)) AS Revenue", "sum(if(rd.ViewDate is null OR IsDup=1,0,1)) AS Views", "sum(if(rd.SubscribeDate is null,0,1)) AS Subscribers", "sum(if(" . $conversionDateField . " is null,0,1)) AS Conversion");
		$dbsource = "clicks rd
				left outer join destinations d2 on rd.OfferID=d2.DestinationID";
		$reportLines = $app->shared_db->get($dbsource, $fields, $count, "rd.CampaignID=" . $selectedCampaignID . " " . $whereClause);
		$reportLines[0]['CampaignID'] = $selectedCampaignID;
		$reportLines[0]['Offer'] = $campaignName;

		if ($campaignTypeID == 9) {
			$reportLines[0]['Clicks'] = (isset($reportLines[0]['Views'])?$reportLines[0]['Views']:0);
			$sumSent = $app->shared_db->get("(select distinct PathID, Sent from destinations where CampaignID=" . $selectedCampaignID . ") a", array("sum(Sent) AS Sent"), $count);
			$reportLines[0]['Sent'] = $reportLines[0]['Views'] = $sumSent[0];
		}

		return $reportLines;
	}

	function getTrendsTime($app, $selectedCampaignID, $campaignTypeID, $viewType, $whereClause, $realTimeCPV, $timezoneOffset) {
		$fields = array("HOUR(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND)) AS TheInterval", "DATE_FORMAT(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND),'%l%p') AS TheIntervalName", "SUM(IF(ViewDate IS NOT NULL AND IsDup=0,1,0)) AS Views", "SUM(IF(EngageDate,1,0)) AS Engages", "sum(COALESCE(c.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "SUM(IF(ClickDate,1,0)) AS Clicks", "SUM(IF(SubscribeDate,1,0)) AS Subscribers", "SUM(IF(ConversionDate,1,0)) AS Conversion");
		$reportLines = $app->shared_db->get("clicks c left outer join destinations d2 on c.OfferID=d2.DestinationID", $fields, $count, $whereClause . " GROUP BY HOUR(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND))", "TheInterval");
		$currentIndex = 0;
		$completeLines = array();
		$i = 0;

		while ($i <= 23) {
			if ((isset($reportLines[$currentIndex]['TheInterval']) && $reportLines[$currentIndex]['TheInterval'] != "") && $i == $reportLines[$currentIndex]['TheInterval']) {
				$completeLines[$i] = $reportLines[$currentIndex];
				$currentIndex++;
			}
			else {
				$currentValue = ($i == 0?12:(12 < $i?$i - 12:$i)) . ($i < 12?"AM":"PM");
				$completeLines[$i] = array("TheInterval" => $i, "TheIntervalName" => $currentValue, "Views" => 0, "Engages" => 0, "Revenue" => 0, "Clicks" => 0, "Subscribers" => 0, "Conversion" => 0);
			}

			$i++;
		}

		computeDataTableStats($completeLines, $realTimeCPV, false, true);
		return $completeLines;
	}

	function getTrendsWeekDays($app, $selectedCampaignID, $campaignTypeID, $viewType, $whereClause, $realTimeCPV, $timezoneOffset) {
		$fields = array("DAYOFWEEK(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND)) AS TheInterval", "DAYNAME(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND)) AS TheIntervalName", "SUM(IF(ViewDate IS NOT NULL AND IsDup=0,1,0)) AS Views", "SUM(IF(EngageDate,1,0)) AS Engages", "sum(COALESCE(c.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "SUM(IF(ClickDate,1,0)) AS Clicks", "SUM(IF(SubscribeDate,1,0)) AS Subscribers", "SUM(IF(ConversionDate,1,0)) AS Conversion");
		$reportLines = $app->shared_db->get("clicks c left outer join destinations d2 on c.OfferID=d2.DestinationID", $fields, $count, $whereClause . " GROUP BY DAYOFWEEK(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND)),
					DAYNAME(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND))", "TheInterval");
		$currentIndex = 0;
		$days = array(1 => "Sunday", 2 => "Monday", 3 => "Tuesday", 4 => "Wednesday", 5 => "Thursday", 6 => "Friday", 7 => "Saturday");
		$completeLines = array();
		$i = 1;

		while ($i <= 7) {
			if (isset($reportLines[$currentIndex]['TheInterval']) && $i == $reportLines[$currentIndex]['TheInterval']) {
				$completeLines[$i - 1] = $reportLines[$currentIndex];
				$currentIndex++;
			}
			else {
				$completeLines[$i - 1] = array("TheInterval" => $i, "TheIntervalName" => $days[$i], "Views" => 0, "Engages" => 0, "Revenue" => 0, "Clicks" => 0, "Subscribers" => 0, "Conversion" => 0);
			}

			$i++;
		}

		computeDataTableStats($completeLines, $realTimeCPV, false, true);
		return $completeLines;
	}

	function getTrendsDays($app, $selectedCampaignID, $campaignTypeID, $viewType, $whereClause, $realTimeCPV, $timezoneOffset, $startDate, $endDate) {
		$startDate = strtotime($startDate);
		$endDate = strtotime($endDate);
		$fields = array("DATE(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND)) AS TheInterval", "DATE_FORMAT(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND), '%m/%d/%Y') AS TheIntervalName", "SUM(IF(ViewDate IS NOT NULL AND IsDup=0,1,0)) AS Views", "SUM(IF(EngageDate,1,0)) AS Engages", "sum(COALESCE(c.Revenue,d2.Payout,0) * if(ConversionDate is null,0,1)) AS Revenue", "SUM(IF(ClickDate,1,0)) AS Clicks", "SUM(IF(SubscribeDate,1,0)) AS Subscribers", "SUM(IF(ConversionDate,1,0)) AS Conversion");
		$reportLines = $app->shared_db->get("clicks c left outer join destinations d2 on c.OfferID=d2.DestinationID", $fields, $count, $whereClause . " GROUP BY DATE(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND)),
					DATE_FORMAT(DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND), '%m/%d/%Y')", "TheInterval");
		$currentIndex = 0;
		$days = array();
		$completeLines = array();
		$currentDate = $startDate;

		while ($currentDate <= $endDate) {
			$days[] = date("Y-m-d", $currentDate);
			$currentDate = strtotime(date("Y-m-d", $currentDate) . " +1 day");
		}

		$countDays = count($days);
		$i = 0;

		while ($i < $countDays) {
			if (isset($reportLines[$currentIndex]['TheInterval']) && $days[$i] == $reportLines[$currentIndex]['TheInterval']) {
				$completeLines[] = $reportLines[$currentIndex];
				$currentIndex++;
			}
			else {
				$completeLines[] = array("TheInterval" => $i, "TheIntervalName" => date("m/d/Y", strtotime($days[$i])), "Views" => 0, "Engages" => 0, "Revenue" => 0, "Clicks" => 0, "Subscribers" => 0, "Conversion" => 0);
			}

			$i++;
		}

		computeDataTableStats($completeLines, $realTimeCPV, false, true);
		return $completeLines;
	}

	function getFilterTypeTarget($views, $clicks, $conversions, $ROI, $PPV, $topt1ROI, $topt1PPV, $topt4Views, $topt4Clicks, $topt5Views, $topt5Conversion, $topt6Views, $topt6ROI, $topt6PPV) {
		if (($topt1ROI != "" && $topt1ROI <= $ROI) || ($topt1PPV != "" && $topt1PPV <= $PPV)) {
			return "Winner";
		}


		if ((((($topt4Views != "" && $topt4Clicks != "") && $topt4Views < $views) && $clicks < $topt4Clicks) || ((($topt5Views != "" && $topt5Conversion != "") && $topt5Views < $views) && $conversions < $topt5Conversion)) || (($topt6Views != "" && $topt6Views < $views) && (($topt6ROI != "" && $ROI < $topt6ROI) || ($topt6PPV != "" && $PPV < $topt6PPV)))) {
			return "Remove";
		}

		return "In Progress";
	}

	function getFilterTypeLandign($views, $clicks, $conversions, $ROI, $PPV, $lopt2ROI, $lopt2PPV, $lopt7Views, $lopt7Clicks, $lopt8Views, $lopt8Conversion) {
		if (($lopt2ROI != "" && $lopt2ROI <= $ROI) || ($lopt2PPV != "" && $lopt2PPV <= $PPV)) {
			return "Winner";
		}


		if (((($lopt7Views != "" && $lopt7Clicks != "") && $lopt7Views < $views) && $clicks < $lopt7Clicks) || ((($lopt8Views != "" && $lopt8Conversion != "") && $lopt8Views < $views) && $conversions < $lopt8Conversion)) {
			return "Remove";
		}

		return "In Progress";
	}

	function getFilterTypeOffer($views, $clicks, $conversions, $ROI, $PPV, $oopt3ROI, $oopt9Views, $oopt9Conversion, $oopt10Visitors, $oopt10Conversion, $campaignTypeID, $destinationType) {
		if ($oopt3ROI != "" && $oopt3ROI <= $ROI) {
			return "Winner";
		}


		if (!(($campaignTypeID == 8 || $campaignTypeID == 9) && $destinationType == 2)) {
			if ((($oopt10Visitors != "" && $oopt10Conversion != "") && $oopt10Visitors < $clicks) && $conversions < $oopt10Conversion) {
				return "Remove";
			}
		}
		else {
			if ((($oopt9Views != "" && $oopt9Conversion != "") && $oopt9Views < $views) && $conversions < $oopt9Conversion) {
				return "Remove";
			}
		}

		return "In Progress";
	}

	function getFilterTypeTargetExtra($views, $PPV, &$currentAction, &$adjustedCpv, $txtLowerViews, $txtLowerPpv, $txtLowerCpv, $txtIncreaseViews, $txtIncreasePpv, $txtIncreaseCpv, $txtIncreaseCpvWinners) {
		if ((((($currentAction == "In Progress" && $txtLowerViews != "") && $txtLowerPpv != "") && $txtLowerCpv != "") && $txtLowerViews < $views) && $PPV < $txtLowerPpv) {
			$currentAction = "Lower";
			$adjustedCpv -= floatval($txtLowerCpv);
		}


		if ((((($currentAction == "In Progress" && $txtIncreaseViews != "") && $txtIncreasePpv != "") && $txtIncreaseCpv != "") && $txtIncreaseViews < $views) && $txtIncreasePpv < $PPV) {
			$currentAction = "Increase";
			$adjustedCpv += floatval($txtIncreaseCpv);
		}


		if ($currentAction == "Winner" && $txtIncreaseCpvWinners) {
			$adjustedCpv += floatval($txtIncreaseCpvWinners);
		}

	}

	function getVisitorStats($app, $selectedCampaignID, $whereClause, $sortField, $dirField, $selectedRecords, $selectedCountry, $GEOIP_REGION_NAME, $forExport = false) {
		if (($sortField == "Country" || $sortField == "State") || $sortField == "City") {
			$sortInDatabase = "";
		}
		else {
			$sortInDatabase = $sortField . " " . $dirField;
		}

		$recordsToRetrieve = ($selectedCountry != ""?0:$selectedRecords);
		$fields = array("ClickID", "CampaignID", "CampaignName", "Source", "ViewDate", "Keyword", "inet_ntoa(IPAddress) AS IP", "COALESCE(clpClickDate, ClickDate) AS ClickDate", "IF(clpClickDate IS NOT NULL OR ClickDate IS NOT NULL, 'X', '') AS ClickStatus", "Referrer", "ReferrerDomain", "DestinationID", "Offer", "Url", "ConversionDate", "IF(ConversionDate IS NOT NULL, 'X', '') AS ConversionStatus", "DestinationID2", "Offer2", "Url2", "SubId");
		$i = 1;

		while ($i <= 10) {
			$fields[] = "Extra" . $i;
			$i++;
		}

		$from = "(select rd.ClickID, rd.CampaignID, CampaignName, Source, ViewDate, Keyword, IPAddress,
				rd.ClickDate, clp.ClickDate AS clpClickDate, Referrer, ReferrerDomain, d.DestinationID, d.Offer, d.Url, ConversionDate,
				d2.DestinationID AS DestinationID2, COALESCE(d2.Offer,'') AS Offer2, COALESCE(d2.Url,'') AS Url2,
				CONCAT(s.SubId,COALESCE(af.SubIdSeparator,'_'),rd.CampaignID,COALESCE(af.SubIdSeparator,'_'),rd.ClickID) AS SubId,
				COALESCE(Extra1,'') AS Extra1, COALESCE(Extra2,'') AS Extra2, COALESCE(Extra3,'') AS Extra3, COALESCE(Extra4,'') AS Extra4,
				COALESCE(Extra5,'') AS Extra5, COALESCE(Extra6,'') AS Extra6, COALESCE(Extra7,'') AS Extra7, COALESCE(Extra8,'') AS Extra8,
				COALESCE(Extra9,'') AS Extra9, COALESCE(Extra10,'') AS Extra10
				from clicks rd inner join subids s on rd.SubIdID=s.SubIdID
				inner join campaigns ca on rd.CampaignID=ca.CampaignID
				inner join destinations d on rd.DestinationID=d.DestinationID
				left outer join destinations d2 on rd.OfferID=d2.DestinationID
				left outer join affiliatesources af on d2.AffiliateSourceID=af.AffiliateSourceID
				left outer join clickslp clp on rd.ClickID=clp.ClickID and clp.Level=2
				left outer join clicksextra ce on rd.ClickID=ce.ClickID
				inner join referrers r on rd.ReferrerID=r.ReferrerID
				inner join referrerdomains rr on r.ReferrerDomainID=rr.ReferrerDomainID
				where " . ($selectedCampaignID?"rd.CampaignID=" . $selectedCampaignID . " AND ":"") . "ca.DeleteDate is null" . $whereClause . " order by ViewDate DESC " . ($recordsToRetrieve?" limit 0," . $recordsToRetrieve:"") . ") a";
		$reportLines = $app->shared_db->get($from, $fields, $count, "", $sortInDatabase);
		$reportLines = GeoIpArray($reportLines, $GEOIP_REGION_NAME, $selectedCountry);
		$countReportLines = count($reportLines);
		$i = 0;

		while ($i < $countReportLines) {
			if (!$forExport) {
				$reportLines[$i]['Country'] = "<img src=\"images/flags/" . ($reportLines[$i]['CountryCode']?strtolower($reportLines[$i]['CountryCode']):"empty") . ".gif\" alt=\"" . $reportLines[$i]['Country'] . "\"/>&nbsp;" . $reportLines[$i]['Country'];

				if ($reportLines[$i]['ClickStatus']) {
					$reportLines[$i]['ClickStatus'] = "<img src=\"images/check_icon.png\"/>";
				}


				if ($reportLines[$i]['ConversionStatus']) {
					$reportLines[$i]['ConversionStatus'] = "<img src=\"images/check_icon.png\"/>";
				}
			}

			$i++;
		}


		if ($selectedRecords && $selectedRecords < $countReportLines) {
			$i = $selectedRecords;

			while ($i < $countReportLines) {
				unset($reportLines[$i]);
				$i++;
			}

			$reportLines = array_values($reportLines);
		}


		if ($sortInDatabase == "") {
			sortDataTable($reportLines, $sortField, $dirField);
		}

		return $reportLines;
	}

	function getConversionsList($app, $selectedCampaignID, $timezoneOffset, $whereClause, $sortField, $dirField, $GEOIP_REGION_NAME) {
		if ((((($sortField == "ConversionDate" || $sortField == "CampaignName") || $sortField == "Keyword") || $sortField == "IPAddress") || $sortField == "Offer") || $sortField == "Revenue") {
			$sortInDatabase = $sortField . " " . $dirField;
		}
		else {
			$sortInDatabase = "";
		}

		$reportLines = $app->shared_db->get("clicks c inner join destinations d on c.OfferID=d.DestinationID
					inner join subids s on c.SubIdID=s.SubIdID
					left outer join affiliatesources a on d.AffiliateSourceID =a.AffiliateSourceID
					inner join campaigns ca on c.CampaignID=ca.CampaignID", array("c.CampaignID", "DATE_ADD(ConversionDate, INTERVAL " . $timezoneOffset . " SECOND) AS ConversionDate", "inet_ntoa(IPAddress) AS IP", "Offer", "COALESCE(c.Revenue,Payout) AS Revenue", "SubId", "Keyword", "COALESCE(a.SubIdSeparator,'_') AS SubIdSeparator", "ClickID", "CampaignName"), $count, ($selectedCampaignID?"c.CampaignID=" . $selectedCampaignID . " AND ":"") . "ConversionDate is not null" . $whereClause, $sortInDatabase);
		$reportLines = GeoIpArray($reportLines, $GEOIP_REGION_NAME);

		if ($sortInDatabase == "") {
			sortDataTable($reportLines, $sortField, $dirField);
		}

		return $reportLines;
	}

	function getBlockedTraffic($app, $selectedCampaignID, $timezoneOffset, $whereClause, $sortField, $dirField) {
		if ((((($sortField == "ViewDate" || $sortField == "CampaignName") || $sortField == "Keyword") || $sortField == "BlockReason") || $sortField == "IPAddress") || $sortField == "UserAgent") {
			$sortInDatabase = $sortField . " " . $dirField;
		}
		else {
			$sortInDatabase = "";
		}

		$reportLines = $app->shared_db->get("blockedclicks c inner join subids s on c.SubIdID=s.SubIdID
					inner join campaigns ca on c.CampaignID=ca.CampaignID", array("DATE_ADD(ViewDate, INTERVAL " . $timezoneOffset . " SECOND) AS ViewDate", "inet_ntoa(IPAddress) AS IP", "BlockReason", "UserAgent", "Keyword", "ClickID", "CampaignName"), $count, ($selectedCampaignID?"c.CampaignID=" . $selectedCampaignID:"1=1") . $whereClause, $sortInDatabase);

		if ($sortInDatabase == "") {
			sortDataTable($reportLines, $sortField, $dirField);
		}

		return $reportLines;
	}

	function getFailedLogins($app, $timezoneOffset, $whereClause, $sortField, $dirField, $GEOIP_REGION_NAME) {
		if ((($sortField == "LoginDate" || $sortField == "IPAddress") || $sortField == "Username") || $sortField == "Password") {
			$sortInDatabase = $sortField . " " . $dirField;
		}
		else {
			$sortInDatabase = "";
		}

		$reportLines = $app->shared_db->get("logins", array("DATE_ADD(LoginDate, INTERVAL " . $timezoneOffset . " SECOND) AS LoginDate", "inet_ntoa(IPAddress) AS IP", "LoginID", "Username", "Password"), $count, "LoginDate is not null" . $whereClause, $sortInDatabase);
		$reportLines = GeoIpArray($reportLines, $GEOIP_REGION_NAME);

		if ($sortInDatabase == "") {
			sortDataTable($reportLines, $sortField, $dirField);
		}

		return $reportLines;
	}

	function getErrors($app, $timezoneOffset, $whereClause, $sortField, $dirField) {
		$sortInDatabase = $sortField . " " . $dirField;
		$reportLines = $app->shared_db->get("errors e inner join errortypes et on e.ErrorTypeID=et.ErrorTypeID", array("ErrorID, DATE_ADD(ErrorDate, INTERVAL " . $timezoneOffset . " SECOND) AS ErrorDate", "ErrorType", "UserID", "Page", "Context", "Error", "Query", "ErrorCode"), $count, "1=1" . $whereClause, $sortInDatabase);
		return $reportLines;
	}

	function getOverallStatsGeneral($app, $whereClause) {
		return $app->shared_db->get("clicks c inner join destinations d on c.DestinationID=d.DestinationID
					left outer join destinations d2 on c.OfferID=d2.DestinationID left outer join clickslp lp2 on c.ClickID=lp2.ClickID and lp2.Level=2
					inner join campaigns ca on c.CampaignID=ca.CampaignID and CampaignTypeID<>4", array("ca.CampaignID", "sum(if(ViewDate IS NOT NULL AND IsDup=0,1,0)) As Views", "sum(if(IsDup=0,ca.RealTimeCPV,0)) AS Cost", "sum(if(c.ConversionDate,1,0) * COALESCE(c.Revenue,d2.Payout,0)) As Revenue", "sum(if(c.ClickDate is null and lp2.ClickDate is null,0,1)) AS Clicks"), $count, "ca.DeleteDate is null and ca.CampaignName<>'' " . $whereClause . " group by ca.CampaignID");
	}

	function getOverallStats4($app, $whereClause) {
		return $app->shared_db->get("clicks c inner join campaigns ca on c.CampaignID=ca.CampaignID and CampaignTypeID=4", array("ca.CampaignID", "sum(if(ViewDate IS NOT NULL AND IsDup=0,1,0)) AS Views", "sum(if(IsDup=0,ca.RealTimeCPV,0)) AS Cost", "sum(if(SubscribeDate,1,0)) As Clicks"), $count, "ca.DeleteDate is null and ca.CampaignName<>'' " . $whereClause . "	 group by ca.CampaignID");
	}

	function getEarningsBox($app, $timezoneOffset, &$viewsToday, &$viewsYesterday, &$viewsThisWeek, &$viewsThisMonth, &$earningsToday, &$earningsYesterday, &$earningsThisWeek, &$earningsThisMonth) {
		$currentDateAdjusted = time() + $timezoneOffset;
		$startToday = strtotime(date("m/d/Y", $currentDateAdjusted)) - $timezoneOffset;
		$whereClause = "IsDup=0 AND ViewDate>='" . date("Y-m-d H:i:s", $startToday) . "'";
		$viewsToday = $app->shared_db->count_records("clicks", $whereClause);
		$whereClause = "rd.ConversionDate IS NOT NULL AND ConversionDate>='" . date("Y-m-d H:i:s", $startToday) . "'";
		$earningsToday = $app->shared_db->get("clicks rd left outer join destinations d on rd.OfferID=d.DestinationID", array("sum(if(rd.ConversionDate,1,0) * COALESCE(rd.Revenue,d.Payout)) As TotalRevenue"), $count, $whereClause);
		$startYesterday = strtotime(date("m/d/Y", mktime(0, 0, 0, date("m", $currentDateAdjusted), date("d", $currentDateAdjusted) - 1, date("Y", $currentDateAdjusted)))) - $timezoneOffset;
		$whereClause = "IsDup=0 AND ViewDate>='" . date("Y-m-d H:i:s", $startYesterday) . "' AND ViewDate<'" . date("Y-m-d H:i:s", $startToday) . "'";
		$viewsYesterday = $app->shared_db->count_records("clicks", $whereClause);
		$whereClause = "rd.ConversionDate IS NOT NULL AND rd.ConversionDate>='" . date("Y-m-d H:i:s", $startYesterday) . "' AND rd.ConversionDate<'" . date("Y-m-d H:i:s", $startToday) . "'";
		$earningsYesterday = $app->shared_db->get("clicks rd left outer join destinations d on rd.OfferID=d.DestinationID", array("sum(if(rd.ConversionDate,1,0) * COALESCE(rd.Revenue,d.Payout)) As TotalRevenue"), $count, $whereClause);
		$startThisWeek = strtotime(date("m/d/Y", mktime(0, 0, 0, date("m", $currentDateAdjusted), date("d", $currentDateAdjusted), date("Y", $currentDateAdjusted))));

		while (date("w", $startThisWeek) != 1) {
			$startThisWeek -= 60 * 60 * 24;
		}

		$startThisWeek -= $timezoneOffset;
		$whereClause = "IsDup=0 AND ViewDate>='" . date("Y-m-d H:i:s", $startThisWeek) . "'";
		$viewsThisWeek = $app->shared_db->count_records("clicks", $whereClause);
		$whereClause = "rd.ConversionDate IS NOT NULL AND rd.ConversionDate>='" . date("Y-m-d H:i:s", $startThisWeek) . "'";
		$earningsThisWeek = $app->shared_db->get("clicks rd left outer join destinations d on rd.OfferID=d.DestinationID", array("sum(if(rd.ConversionDate,1,0) * COALESCE(rd.Revenue,d.Payout)) As TotalRevenue"), $count, $whereClause);
		$startThisMonth = strtotime(date("m/d/Y", mktime(0, 0, 0, date("m", $currentDateAdjusted), 1, date("Y", $currentDateAdjusted)))) - $timezoneOffset;
		$whereClause = "IsDup=0 AND ViewDate>='" . date("Y-m-d H:i:s", $startThisMonth) . "'";
		$viewsThisMonth = $app->shared_db->count_records("clicks", $whereClause);
		$whereClause = "rd.ConversionDate IS NOT NULL AND rd.ConversionDate>='" . date("Y-m-d H:i:s", $startThisMonth) . "'";
		$earningsThisMonth = $app->shared_db->get("clicks rd left outer join destinations d on rd.OfferID=d.DestinationID", array("sum(if(rd.ConversionDate,1,0) * COALESCE(rd.Revenue,d.Payout)) As TotalRevenue"), $count, $whereClause);
	}

	function GeoIpArray($reportLines, $GEOIP_REGION_NAME, $selectedCountry = "") {
		$gi = geoip_open("./geoip/GeoLiteCity.dat", GEOIP_STANDARD);
		$countReportLines = count($reportLines);
		$i = 0;

		while ($i < $countReportLines) {
			$rsGeoData = geoip_record_by_addr($gi, $reportLines[$i]['IP']);
			$reportLines[$i]['Country'] = (isset($rsGeoData->country_name)?$rsGeoData->country_name:"");

			if ($selectedCountry != "" && stripos($reportLines[$i]['Country'], $selectedCountry) === false) {
				unset($reportLines[$i]);
				continue;
			}

			$reportLines[$i]['CountryCode'] = (isset($rsGeoData->country_code)?$rsGeoData->country_code:"");
			$reportLines[$i]['State'] = ((((isset($rsGeoData->country_code) && isset($rsGeoData->region)) && isset($GEOIP_REGION_NAME[$rsGeoData->country_code])) && $rsGeoData->region != "00")?$GEOIP_REGION_NAME[$rsGeoData->country_code][$rsGeoData->region]:"");
			$reportLines[$i]['City'] = (isset($rsGeoData->city)?$rsGeoData->city:"");
			$i++;
		}

		geoip_close($gi);

		if ($selectedCountry != "") {
			$reportLines = array_values($reportLines);
		}

		return $reportLines;
	}
}

?>
