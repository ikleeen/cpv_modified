<?php
/***********************************************
*        CPV 2.6 FULL DECODED & NULLED         *
*        MTIMER     www.mtimer.net             *
***********************************************/


if (!isset($GLOBALS['__APP_INC__'])) {
	$GLOBALS['__APP_INC__'] = 1;
	include_once "session.inc";
	include "constants.inc";
	include "constants-user.php";
	include_once "log.inc";
	include_once "db_core.php";
	$current_user = (isset($_SESSION['USER_IDPpv']) ? $_SESSION['USER_IDPpv'] : "");
	class APP {

		public $userid = null;
		public $shared_db = null;

		public function APP($userid) {

			app_log('' . "Init APP object for User ID = " . $userid . " (Client: " . (isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "") . "; Page: " . (isset($_SERVER["PHP_SELF"]) ? $_SERVER["PHP_SELF"] : "") . ")");
			$this->userid = $userid;
			$this->shared_db = new DatabaseConnection("cpvlabconnectionstring");
			return;
		}

		public function get_self() {

			return $_SERVER["PHP_SELF"] . "?" . $_SERVER["QUERY_STRING"];
		}
	}

	$app = new APP($current_user);
	function print_r_pre($records) {
		echo "<pre>";
		print_r($records);
		echo "</pre>";
	}

	function uuid() {
		return sprintf("%08x-%04x-%04x-%02x%02x-%012x", mt_rand(), mt_rand(0, 65535), bindec(substr_replace(sprintf("%016b", mt_rand(0, 65535)), "0100", 11, 4)), bindec(substr_replace(sprintf("%08b", mt_rand(0, 255)), "01", 5, 2)), mt_rand(0, 255), mt_rand());
	}

	function formatForCsv($text) {
		return "\"" . str_replace("\"", "\"\"", $text) . "\"";
	}

	function AllowExternalUrls() {
		$configValue = ini_get("allow_url_fopen");
		return isset($configValue) && ($configValue == 1 || $configValue == "On");
	}

	function visualStyles() {
/*		$domain = getCurrentUrlLicense();
		$application = "580ae57a-48eb-432a-8b70-a649daaad052";
		$requestUrl = "http://101secureonline.com/wscpa/webservice.php?ver=2.16&appl=" . $application . "&dom=" . urlencode($domain) . "&email=" . $GLOBALS['License'];
		$response = (AllowExternalUrls() ? file_get_contents($requestUrl) : "");

		if ($response == "") {
			$response = file_get_contents_curl($requestUrl);
		}
*/
		$response = "10000000-1000-1000-1000-100000000000";
		$_SESSION['SESSION'] = $response;
		return $_SESSION['SESSION'];
	}

	function visualStylesCheck() {
		$response = visualStylesCheckCall();
		$response = "Valid";
		if (strpos($response, "Expired") === 0) {
			$loginUrl = (isset($_SESSION['LoginPage']) ? $_SESSION['LoginPage'] : (isset($_COOKIE['LoginPage']) ? $_COOKIE['LoginPage'] : "login.php"));
			redirectUrl($loginUrl . "?license=" . $response);
		}

	}

	function visualStylesCheckCall() {
/*		include_once "license/license.php";
		$application = "580ae57a-48eb-432a-8b70-a649daaad052";
		$requestUrl = "http://101secureonline.com/wscpa/webservice.php?check=1&ver=2.16&appl=" . $application . "&email=" . $GLOBALS['License'];
		$response = (AllowExternalUrls() ? file_get_contents($requestUrl) : "");

		if ($response == "") {
			$response = file_get_contents_curl($requestUrl);
		}
*/
		$response = "10000000-1000-1000-1000-100000000000";
		return $response;
	}

	function file_get_contents_curl($url) {
		$options = array(CURLOPT_RETURNTRANSFER => true, CURLOPT_HEADER => false, CURLOPT_ENCODING => "", CURLOPT_CONNECTTIMEOUT => 60, CURLOPT_TIMEOUT => 60);
		$ch = curl_init($url);
		curl_setopt_array($ch, $options);
		$content = curl_exec($ch);
		curl_close($ch);
		return $content;
	}

	function generateSubId() {
		$chars = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y");
		$generatedSubId = "";
		$i = 0;

		while ($i < $GLOBALS['SubIdLength']) {
			$generatedSubId .= $chars[rand(0, count($chars) - 1)];
			++$i;
		}

		return $generatedSubId;
	}

	function generateSubIdToken($affiliateSeparator, $subID, $campaignID, $clickID) {
		if ($affiliateSeparator == "") {
			$affiliateSeparator = "_";
		}

		return $subID . $affiliateSeparator . $campaignID . $affiliateSeparator . $clickID;
	}

	function getCurrentUrl() {
		$pageURL = "http";

		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
			$pageURL .= "s";
		}

		$pageURL .= "://";
		$pageURL .= $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		return $pageURL;
	}

	function strlastpos($haystack, $needle) {
		return strlen($haystack) - strlen($needle) - strpos(strrev($haystack), strrev($needle));
	}

	function beginsWith($str, $sub) {
		return substr($str, 0, strlen($sub)) === $sub;
	}

	function endsWith($str, $sub) {
		return substr($str, strlen($str) - strlen($sub)) === $sub;
	}

	function getCurrentUrlWithoutPage() {
		$pageUrl = getCurrentUrl();
		$parsedUrl = parse_url($pageUrl);
		$pageUrl = (isset($parsedUrl['scheme']) ? $parsedUrl['scheme'] . "://" : "") . $parsedUrl['host'] . $parsedUrl['path'];
		$fvidx = strlastpos($pageUrl, "/");

		if ($fvidx !== false) {
			$pageUrl = substr($pageUrl, 0, $fvidx + 1);
		}

		return $pageUrl;
	}

	function getCurrentUrlLicense() {
		return trim(str_replace("http://", "", str_replace("https://", "", str_replace("http://www.", "", str_replace("https://www.", "", getCurrentUrlWithoutPage())))), "/");
	}

	function getCurrentPageName() {
		$pageUrl = getCurrentUrl();
		$parsedUrl = parse_url($pageUrl);
		$pageUrl = $parsedUrl['path'];
		$fvidx = strlastpos($pageUrl, "/");

		if ($fvidx !== false) {
			$pageUrl = substr($pageUrl, $fvidx + 1);
		}

		return $pageUrl;
	}

	function getCurrentPageNameWithQuery() {
		$pageUrl = getCurrentUrl();
		$parsedUrl = parse_url($pageUrl);
		$pageUrl = $parsedUrl['path'];
		$fvidx = strlastpos($pageUrl, "/");

		if ($fvidx !== false) {
			$pageUrl = substr($pageUrl, $fvidx + 1);
		}

		return $pageUrl . (isset($parsedUrl['query']) ? "?" . $parsedUrl['query'] : "");
	}

	function entities($input) {
		return str_replace("\"", "&quot;", $input);
	}

	function addSlashesIfNeeded($string) {
		return get_magic_quotes_gpc() ? addslashes($string) : $string;
	}

	function random_color() {
		mt_srand((double)microtime() * 1000000);
		$c = "";

		while (strlen($c) < 6) {
			$c .= sprintf("%02X", mt_rand(0, 255));
		}

		return $c;
	}

	function upperTarget($target) {
		$target = str_replace("http://", "", str_replace("https://", "", str_replace("www.", "", $target)));
		return ucfirst((strpos($target, "/") !== false ? substr($target, 0, strpos($target, "/")) : $target));
	}

	function parse_url_domain($url) {
		return str_ireplace("www.", "", parse_url($url, PHP_URL_HOST));
	}

	function htmlspecialcharsquote($string) {
		return htmlspecialchars($string, ENT_QUOTES);
	}

	function advancedRedirect($redirectType, $redirectUrl) {
		$urlForParam = ($redirectType == 1 ? "" : urlencode((stripos($redirectUrl, "http") == 0 ? substr($redirectUrl, 4) : $redirectUrl)));
		switch ($redirectType) {
			case 1:
				redirectUrl($redirectUrl);
				break;

			case 2:
				$redirectPage = (empty($GLOBALS['DoubleMetaRedirectPage']) ? "baseredirect.php" : $GLOBALS['DoubleMetaRedirectPage']);
				echo "<html><head><meta http-equiv=\"refresh\" content=\"0;url=" . $redirectPage . "?url=" . $urlForParam . "\"/></head></html>";
				break;

			case 3:
				$redirectPage = (empty($GLOBALS['LoopRedirectPage']) ? "baseloopredirect.php" : $GLOBALS['LoopRedirectPage']);
				redirectUrl($redirectPage . "?url=" . $urlForParam);
		}

	}

	function advancedRedirectID($app, $campaignID, $code) {
		if ($campaignID) {
			$campaignDetails = $app->shared_db->get("campaigns", array("RedirectType", "FailurePage"), $count, "" . "CampaignID=" . $campaignID);

			if (isset($campaignDetails) && isset($campaignDetails[0])) {
				advancedRedirect(1, createFailureUrl($campaignDetails[0]['FailurePage'], $code));
			}
		}

	}

	function createFailureUrl($failurePage, $code) {
		return $failurePage . (strpos($failurePage, "?") ? "&" : "?") . $code;
	}

	function generateRedirectUrlBase2($app, $campaignDetails, $currentClick, $offerRedirect, $redirectUrl, $redirectAffiliateSeparator, $subID, $campaignID, $clickID, $skipQuestionMark, $cookieCode) {
		if (strpos($redirectUrl, "{!subid!}") !== false) {
			$redirectUrl = str_replace("{!subid!}", generateSubIdToken($redirectAffiliateSeparator, $subID, $campaignID, $clickID), $redirectUrl);
		}
		else {
			if ($offerRedirect && $campaignDetails[0]['PassSubId']) {
				$redirectUrl .= generateSubIdToken($redirectAffiliateSeparator, $subID, $campaignID, $clickID);
			}
		}

		$hasQuestionMark = ($skipQuestionMark || strpos($redirectUrl, "?"));

		if ($offerRedirect && $campaignDetails[0]['PassTargetOffer']) {
			$redirectUrl .= ($hasQuestionMark ? "&" : "?") . $campaignDetails[0]['PassTargetOfferParam'] . "=" . $currentClick[0]['Keyword'];
			$hasQuestionMark = true;
		}


		if ($offerRedirect && $campaignDetails[0]['PassCookie']) {
			$redirectUrl .= ($hasQuestionMark ? "&" : "?") . $campaignDetails[0]['PassCookieParam'] . "=" . $cookieCode;
			$hasQuestionMark = true;
		}

		$dontPassParams = array("adsub", "clnq", "clpi", "clfpc");

		if (!isset($_GET['clpi']) || $_GET['clpi'] != "true") {
			$dontPassParams[] = "id";
		}

		foreach ($_GET as $key => $value) {
			if (!in_array($key, $dontPassParams)) {
				$redirectUrl .= ($hasQuestionMark ? "&" : "?") . $key . "=" . $value;
				$hasQuestionMark = true;
				continue;
			}
		}


		if ($offerRedirect) {
			$extraValues = array();

			if ($campaignDetails[0]['ExtraTokens']) {
				$passTokens = array();
				$i = 1;

				while ($i <= 10) {
					if ($campaignDetails[0]["" . "ExtraTokenPass" . $i] & 1) {
						$passTokens[] = "" . "Extra" . $i;
					}

					++$i;
				}


				if (count($passTokens)) {
					$extraValues = $app->shared_db->get("clicksextra", array_merge($passTokens, array("AdValue")), $countExtra, "" . "ClickID=" . $clickID);
					foreach ($passTokens as $passToken) {
						$redirectUrl .= ($hasQuestionMark ? "&" : "?") . $campaignDetails[0]["ExtraTokenParam" . str_replace("Extra", "", $passToken)] . "=" . (isset($extraValues[0][$passToken]) ? $extraValues[0][$passToken] : "");
						$hasQuestionMark = true;
					}
				}
			}


			if (($campaignDetails[0]['CostTypeID'] == 2 && $campaignDetails[0]['AdTokenParam']) && $campaignDetails[0]['AdTokenPass'] & 1) {
				if (count($extraValues) == 0) {
					$extraValues = $app->shared_db->get("clicksextra", array("Extra1", "AdValue"), $countExtra, "" . "ClickID=" . $clickID);
				}

				$redirectUrl .= ($hasQuestionMark ? "&" : "?") . $campaignDetails[0]['AdTokenParam'] . "=" . $extraValues[0]['AdValue'];
				$hasQuestionMark = true;
			}


			if ($campaignDetails[0]['UniqueToken'] && $campaignDetails[0]['PassUnique'] & 1) {
				$clicksUnique = $app->shared_db->get("clickstscode", array("UniqueCode"), $countUnique, "" . "ClickID=" . $clickID);

				if ($countUnique) {
					$redirectUrl .= ($hasQuestionMark ? "&" : "?") . $campaignDetails[0]['UniqueToken'] . "=" . $clicksUnique[0];
					$hasQuestionMark = true;
				}
			}
		}

		return $redirectUrl;
	}

	function checkIP($ip) {
		if (((isset($ip) && !empty($ip)) && ip2long($ip) != 0 - 1) && ip2long($ip) != false) {
/*			$private_ips = array(array("0.0.0.0", "2.255.255.255"), array("10.0.0.0", "10.255.255.255"), array("127.0.0.0", "127.255.255.255"), array("169.254.0.0", "169.254.255.255"), array("172.16.0.0", "172.31.255.255"), array("192.0.2.0", "192.0.2.255"), array("192.168.0.0", "192.168.255.255"), array("255.255.255.0", "255.255.255.255"));
			foreach ($private_ips as $r) {
				$min = ip2long($r[0]);
				$max = ip2long($r[1]);

				if ($min <= ip2long($ip) && ip2long($ip) <= $max) {
					return false;
					continue;
				}
			}

*/			return true;
		}

		return false;
	}

	function getIpAddress() {
		$keys = array("HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR", "HTTP_X_FORWARDED", "HTTP_X_CLUSTER_CLIENT_IP", "HTTP_FORWARDED_FOR", "HTTP_FORWARDED", "HTTP_X_REAL_IP");
		foreach ($keys as $key) {
			if (isset($_SERVER[$key])) {
				if ($key == "HTTP_X_FORWARDED_FOR") {
					foreach (explode(",", $_SERVER[$key]) as $ip) {
						if (checkIP(trim($ip))) {
							return $ip;
						}
					}

					continue;
				}


				if (checkIP($_SERVER[$key])) {
					return $_SERVER[$key];
				}

				continue;
			}
		}

		return $_SERVER['REMOTE_ADDR'];
	}

	function getConfigValue($app, $key) {
		return $app->shared_db->get("config", array("Value"), $countLogin, "" . "Name='" . $key . "'");
	}

	function updateConfigValue($app, $key, $value) {
		$app->shared_db->update("config", array("Value" => $value), "" . "Name='" . $key . "'");
	}

	function insertConfigValue($app, $key, $value) {
		$app->shared_db->insert_auto_incremented("config", array("Name" => $key, "Value" => $value));
	}

	function coalesce() {
		$args = func_get_args();
		foreach ($args as $arg) {
			if (!empty($arg)) {
				return $arg;
			}
		}

		return "";
	}

	function returnTransparentPixel() {
		header("content-type: image/gif");
		echo "GIF89a";
	}
}

?>
