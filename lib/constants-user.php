<?PHP
if (!isset($GLOBALS['_CONSTANTS_USER_INC'])) {
	$GLOBALS['_CONSTANTS_USER_INC']=1;

	// The pattern to display dates inside the application
	$GLOBALS['DateFormat'] = "m/d/Y g:i A";
	
	// The pattern to display dates in the short format inside the application
	$GLOBALS['DateShortFormat'] = "m/d/Y";
	
	// The characters used as thousands and decimal separators in the exported CSV reports
	$GLOBALS['ThousandsSeparatorCSV'] = ""; 
	$GLOBALS['DecimalSeparatorCSV'] = "."; 
	
	// The interval to keep the tracking cookie (in seconds)
	//   86400 -  1 day
	//  432000 -  5 days
	//  864000 - 10 days
	// 1728000 - 20 days
	// 2592000 - 30 days
	$GLOBALS['CookieTimeout'] = 2592000; 
	
	// Name of the page that performs the Double Meta Refresh Redirects
	$GLOBALS['DoubleMetaRedirectPage'] = "baseredirect.php"; 
	
	// Name of the page that performs the Base Loop Redirects
	$GLOBALS['LoopRedirectPage'] = "baseloopredirect.php"; 
	
	// The Clickbank Secret Key used in your Clickbank account
	$GLOBALS['ClickbankSecretKey'] = ""; 
	
	
	
} // end of inclusive if
?>