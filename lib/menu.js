

var timeout         = 200;
var closetimer		= 0;
var ddmenuitem      = 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';
}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose; 
// -->

function ShowPopUp(id) {
$('#' + id).fadeIn(700);
}
function HidePopUp(id) {
$('#' + id).fadeOut(700);
}

function onMouseOut(event) {
	//this is the original element the event handler was assigned to
		e = event.toElement || event.relatedTarget;
		if (e.parentNode == this || e.parentNode.parentNode == this || e == this) {
		   return;
		}
	HidePopUp(event.target.id);
	return false;
}

function hoverMenu(obj) { obj.src = obj.src.replace('.jpg','_mo.jpg').replace('.png','_mo.png'); }
function outMenu(obj) { obj.src = obj.src.replace('_mo.jpg','.jpg').replace('_mo.png','.png'); }
function controlBar() {
	var obj1 = $("#imgFloating1");
	var obj2 = $("#imgFloating2");
	if (obj1.prop("src").indexOf("images/close.png")>=0)
	{
		obj1.prop("src","images/pixel.gif");
		obj2.prop("src","images/Floating Nav Icons2.png");
		obj2.prop("useMap","#quicklinkshidden");
	}
	else
	{
		obj1.prop("src","images/close.png");
		obj2.prop("src","images/Floating Nav Icons.png");
		obj2.prop("useMap","#quicklinks");
	}
}
