<?php
/***********************************************
*        CPV 2.6 FULL DECODED & NULLED         *
*        MTIMER     www.mtimer.net             *
***********************************************/

include_once "lib/app.inc";
requiresLogin();
$newCampaignType = (isset($_REQUEST['type'])?$_REQUEST['type']:(isset($_REQUEST['btnSubmit'])?$_REQUEST['btnSubmit']:0));

if ($newCampaignType) {
	$defaultOptimizationProfile = $app->shared_db->get("optimizationprofiles", array("OptimizationProfileID"), $count, "DefaultProfile=1");
	$defaultOptimizationProfileID = (isset($defaultOptimizationProfile[0])?$defaultOptimizationProfile[0]:1);
	$currentDate = date("Y-m-d H:i:s");
	$fields = array("CampaignTypeID" => $newCampaignType, "KeyCode" => md5(generateSubId() . $currentDate), "OptimizationProfileID" => $defaultOptimizationProfileID, "CreateDate" => $currentDate, "CreateUserID" => $app->userid);
	$campaignID = $app->shared_db->insert_auto_incremented("campaigns", $fields);

	if ($campaignID) {
		$currentType = $app->shared_db->get("campaigntypes", array("CampaignType", "PageName"), $count, "CampaignTypeID=" . $newCampaignType);

		if ($currentType && $currentType[0]['PageName']) {
			redirectUrl($currentType[0]['PageName'] . "?id=" . $campaignID);
		}
		else {
			redirectUrl("campaigns.php");
		}
	}
	else {
		redirectUrl("campaigns.php");
	}
}

include_once "headerhead.php";
echo "    <title>Add Campaign</title>
  </head>
  <body>
    <form id=\"Form1\" method=\"post\">
      ";
include_once "header.php";
echo "	<h1>
	<img src=\"images/add_campaign_title.png\" alt=\"Add Campaign\"/>
	</h1>
    <div style=\"clear:both\">&nbsp;</div>
    <br/><br/>
	<div class=\"divBodyCentered\" style=\"width:835px\">
	<a href=\"javascript:;\" onclick=\"newcampaign(8)\"><img src=\"images/directvslp.png\" alt=\"Direct Link vs. Landing Page\" style=\"border:0\" onmouseover=\"hoverMenu(this)\" onmouseout=\"outMenu(this)\"/></a>
	<a href=\"javascript:;\" o";
echo "nclick=\"newcampaign(7)\" style=\"margin-left:20px\"><img src=\"images/multiplecta.png\" alt=\"Multiple CTA Landing Page\" onmouseover=\"hoverMenu(this)\" onmouseout=\"outMenu(this)\" style=\"border:0\"/></a>
	<a href=\"javascript:;\" onclick=\"newcampaign(5)\" style=\"margin-left:20px\"><img src=\"images/multiplepaths.png\" alt=\"Multiple Paths\" onmouseover=\"hoverMenu(this)\" onmouseout=\"outMenu(this)\" style=\"border:0\"/></a";
echo ">
	<br/><br/>
	<a href=\"javascript:;\" onclick=\"newcampaign(6)\"><img src=\"images/lpsequence.png\" alt=\"Landing Page Sequence\" style=\"border:0\" onmouseover=\"hoverMenu(this)\" onmouseout=\"outMenu(this)\"/></a>
	<a href=\"javascript:;\" onclick=\"newcampaign(4)\" style=\"margin-left:20px\"><img src=\"images/leadcapture.png\" alt=\"Lead Capture / Opt-In\" onmouseover=\"hoverMenu(this)\" onmouseout=\"outMenu(this)\" style=\"";
echo "border:0\"/></a>
	<a href=\"javascript:;\" onclick=\"newcampaign(9)\" style=\"margin-left:20px\"><img src=\"images/emailfollowup.png\" alt=\"Email Follow-Up\" onmouseover=\"hoverMenu(this)\" onmouseout=\"outMenu(this)\" style=\"border:0\"/></a>
	<br/>
	</div>
    <input type=\"submit\" name=\"btnSubmit\" id=\"btnSubmit\" style=\"display:none\"/>
    ";
include_once "footer.php";
echo "	";
echo "<s";
echo "cript type=\"text/javascript\">
	function newcampaign(id)
    {
		$(\"#btnSubmit\").val(id).click();
	}
	</script>
    </form>
  </body>
</html>
";
?>
