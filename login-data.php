<?php
/***********************************************
*        CPV 2.6 FULL DECODED & NULLED         *
*        MTIMER     www.mtimer.net             *
***********************************************/

include_once "lib/app.inc";
include_once "lib/app-pages.inc";
requiresLogin();
include_once "headerhead.php";
echo "    <title>Settings</title>
  </head>
  <body>
    <form id=\"Form1\" method=\"post\">
";
include_once "header.php";
echo "      <h1>
        <img src=\"images/settings_title.png\" alt=\"Settings\"/>
      </h1>
      <div style=\"clear:both\">&nbsp;</div>
";
$message = 0;

if (isset($_REQUEST['btnSubmit_x'])) {
	if (strpos($_REQUEST['txtUsername'], "\'") !== false) {
		$message = 7;
	}
	else {
		$currentAdmin = $app->shared_db->get("users", array("Username"), $countAdmin, "UserID=" . $app->userid . " AND Password='" . md5($_REQUEST['txtPassword']) . "'");

		if ($countAdmin) {
			$password = $_REQUEST['txtPassword'];

			if ($_REQUEST['txtNewPassword'] == $_REQUEST['txtConfirmPassword']) {
				if ($_REQUEST['txtNewPassword'] != "") {
					$password = $_REQUEST['txtNewPassword'];
					$app->shared_db->update("users", array("Username" => $_REQUEST['txtUsername'], "Password" => md5($password), "Timezone" => $_REQUEST['ddlTimezone'], "SessionTimeout" => $_REQUEST['txtSessionTimeout'], "DefaultPage" => $_REQUEST['ddlDefaultPage']), "UserID=" . $app->userid);
					$message = 3;
				}
				else {
					$app->shared_db->update("users", array("Username" => $_REQUEST['txtUsername'], "Timezone" => $_REQUEST['ddlTimezone'], "SessionTimeout" => $_REQUEST['txtSessionTimeout'], "DefaultPage" => $_REQUEST['ddlDefaultPage']), "UserID=" . $app->userid);
					$message = 4;
				}

				$loginPage = getConfigValue($app, "loginpage");

				if (isset($loginPage) && isset($loginPage[0])) {
					updateConfigValue($app, "loginpage", $_REQUEST['txtLoginPage']);
				}
				else {
					insertConfigValue($app, "loginpage", $_REQUEST['txtLoginPage']);
				}

				$cronCampaigns = getConfigValue($app, "croncampaigns");

				if (isset($cronCampaigns) && isset($cronCampaigns[0])) {
					updateConfigValue($app, "croncampaigns", (isset($_REQUEST['chkCronCampaigns'])?1:0));
				}
				else {
					insertConfigValue($app, "croncampaigns", (isset($_REQUEST['chkCronCampaigns'])?1:0));
				}

				$_SESSION['SessionTimeout'] = $_REQUEST['txtSessionTimeout'] * 60;
				$_SESSION['LoginPage'] = $_REQUEST['txtLoginPage'];
				setcookie("LoginPage", $_REQUEST['txtLoginPage'], time() + 24 * 3600);
			}
			else {
				$message = 2;
			}
		}
		else {
			$message = 1;
		}
	}
}

echo "      <div class=\"divMessage\">
";
switch ($message) {
	case 1:
		echo "The old password is not correct";
		break;

	case 2:
		echo "The new password doesn't match the confirmation password";
		break;

	case 3:
		echo "User settings and password have been changed";
		break;

	case 4:
		echo "User settings have been changed";
		break;

	case 7:
		echo "The username cannot contain single quotes (')";
}

$currentAdmin = $app->shared_db->get("users", array("Username", "Timezone", "SessionTimeout", "CookieTimeout", "DefaultPage"), $countAdmin, "UserID=" . $app->userid);
$loginPage = getConfigValue($app, "loginpage");
$cronCampaigns = getConfigValue($app, "croncampaigns");
$cookieTimeout = (isset($GLOBALS['CookieTimeout'])?$GLOBALS['CookieTimeout'] / (60 * 60 * 24):$currentAdmin[0]['CookieTimeout'] / (60 * 24));
echo "      </div>
      <div style=\"clear:both\">&nbsp;</div>
      <div class=\"divBodyCentered\" style=\"width:550px;\">
        <div class=\"fdLabel\">
          Username:
        </div>
        <div style=\"float:left\">
			<input name=\"txtUsername\" type=\"text\" class=\"inputs\" value=\"";
echo $currentAdmin[0]['Username'];
echo "\" />
          </div>
          <div style=\"clear:both\">&nbsp;</div>
          <div class=\"fdLabel\">
            Password:
          </div>
          <div style=\"float:left;\">
            <input type=\"password\" name=\"txtPassword\"/>
        </div>
        <div style=\"clear:both\">&nbsp;</div>
        <div>
          Optional: Change password below and confirm.
        </div>
        <div style=\"clea";
echo "r:both\">&nbsp;</div>
          <div class=\"fdLabel\">
            New Password:
          </div>
          <div style=\"float:left;\">
            <input type=\"password\" name=\"txtNewPassword\"/>
        </div>
        <div style=\"clear:both\">&nbsp;</div>
          <div class=\"fdLabel\">
            Confirm Password:
          </div>
          <div style=\"float:left;\">
            <input type=\"password\"";
echo " name=\"txtConfirmPassword\"/>
        </div>
        <div style=\"clear:both\">&nbsp;</div>
		<div class=\"fdLabel\">
		  Select Time Zone:
		</div>
		<div style=\"float:left\">
		";
echo "<s";
echo "elect name=\"ddlTimezone\" style=\"max-width:390px\">
";
generateTimezoneOptions($currentAdmin[0]['Timezone']);
echo "		</select>
		<br/>Server timezone is ";
echo date_default_timezone_get();
echo "		</div>
        <div style=\"clear:both\">&nbsp;</div>
          <div class=\"fdLabel\">
            Session Timeout:
          </div>
          <div style=\"float:left;\">
            <input type=\"text\" name=\"txtSessionTimeout\" value=\"";
echo $currentAdmin[0]['SessionTimeout'];
echo "\" onblur=\"validateInt(this,1,null,30)\" style=\"width:70px\" class=\"center\"/>&nbsp;minutes
        </div>
        <div style=\"clear:both\">&nbsp;</div>
          <div class=\"fdLabel\">
            Cookie Length:
          </div>
          <div style=\"float:left;\">
            <input type=\"text\" disabled=\"disabled\" name=\"txtCookieTimeout\" value=\"";
echo $cookieTimeout;
echo "\" style=\"width:70px\" class=\"center\"/>&nbsp;days
        </div>
		<div class=\"clear italic\">
		Starting with version 2.16 the Cookie Length can be set by editing the<br/>
		'CookieTimeout' key from the 'constants-user.php' file inside the 'lib' folder
		</div>
        <div style=\"clear:both\">&nbsp;</div>
          <div class=\"fdLabel\">
            Login Page Name:
          </div>
          <div s";
echo "tyle=\"float:left;\">
            <input type=\"text\" name=\"txtLoginPage\" value=\"";
echo isset($loginPage[0])?$loginPage[0]:"login.php";
echo "\"/>
        </div>
        <div style=\"clear:both\">&nbsp;</div>
          <div class=\"fdLabel\">
            Default Page:
          </div>
          <div style=\"float:left;\">
            ";
echo "<s";
echo "elect name=\"ddlDefaultPage\">
			";
$defaultPages = array("campaigns.php", "stats.php", "reports.php", "trends.php", "optimize.php", "visitor-stats.php", "conversions-list.php");
$currentDefaultPage = $currentAdmin[0]['DefaultPage'];
foreach ($defaultPages as $defaultPage) {
	echo "<option value=\"" . $defaultPage . "\"" . ($currentDefaultPage == $defaultPage?" selected=\"selected\"":"") . ">" . $defaultPage . "</option>";
}

echo "			</select>
        </div>
        <div style=\"clear:both\">&nbsp;</div>
          <div class=\"fdLabel\">
            &nbsp;
          </div>
          <div style=\"float:left;\">
			<input type=\"checkbox\" name=\"chkCronCampaigns\" id=\"chkCronCampaigns\"";

if (isset($cronCampaigns) && $cronCampaigns[0]) {
	echo " checked=\"checked\"";
}

echo "/>
			<label for=\"chkCronCampaigns\">Use Cron Job for data on campaigns.php</label>
        </div>
        <div style=\"clear:both\">&nbsp;</div>
        <div class=\"fullwidth center\">
          <input type=\"image\" name=\"btnSubmit\" src=\"images/bt_save.png\" onmouseover=\"hoverMenu(this)\" onmouseout=\"outMenu(this)\"/>&nbsp;&nbsp;&nbsp;&nbsp;
          <input type=\"image\" name=\"btnCancel\" src=\"images/bt_can";
echo "cel.png\" onclick=\"return redirectToCampaigns()\" onmouseover=\"hoverMenu(this)\" onmouseout=\"outMenu(this)\"/>
        </div>
        <div style=\"clear:both\">&nbsp;</div>
		<br/>
		<br/>
		<h1 class=\"left\"><img src=\"images/backup_database.png\" alt=\"Backup Database\"/></h1>
		<div>
			Click Save Below to generate a backup file of your database.<br/>Use this file when moving to a new server.
		</div>
		<div";
echo " style=\"clear:both\">&nbsp;</div>
		<div class=\"fdLabel\">
			&nbsp;
		</div>
		<div style=\"float:left;\">
			<input type=\"image\" onclick=\"window.location.href='database-backup.php'; return false\" src=\"images/bt_save.png\" onmouseover=\"hoverMenu(this)\" onmouseout=\"outMenu(this)\"/>
		</div>
		<div style=\"clear:both\">&nbsp;</div>
      </div>
      <div style=\"clear:both\">
        &nbsp;
      </div>
";
include_once "footer.php";
echo "    </form>
  </body>
</html>


";
?>
