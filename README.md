# Prepare to install

Install packages:
```sh
$ apt-get install apache2 libapache2-mod-php5 mysql-server php5 mysql-client php5-mysql phpmyadmin 
$ service apache2 restart
```
Install ioncube libs (for x86):
```sh
$ wget http://downloads2.ioncube.com/loader_downloads/ioncube_loaders_lin_x86.tar.bz2
$ tar -xjf ioncube_loaders_lin_x86.tar.bz2 -C /usr/lib/php5/
```
Check php version:
```sh
$ php -v
```
Add ioncube libs to config:
```sh
$ nano /etc/php5/apache2/php.ini

[PHP]
zend_extension_ts = /usr/lib/php5/ioncube/ioncube_loader_lin_5.3_ts.so
zend_extension = /usr/lib/php5/ioncube/ioncube_loader_lin_5.3.so
```
Create test file:
```sh
$ echo "<?php phpinfo(); ?>" | tee /var/www/info.php
```
Restart apache2:
```sh
$ sudo service apache2 restart
```
Check:
http://127.0.0.1/info.php

"This program makes use of the Zend Scripting Language Engine:
Zend Engine v2.3.0, Copyright (c) 1998-2012 Zend Technologies
    with the ionCube PHP Loader (enabled) + Intrusion Protection from ioncube24.com (unconfigured) v5.0.17, Copyright (c) 2002-2015, by ionCube Ltd."
# Install CPVLab
```sh
$ cd /var/www
$ git clone https://bitbucket.org/ikleeen/cpv_modified
```
Now create database "cpv_base" (I'm used phpmyadmin)
And import tables from /var/www/cpv_modified/cpv_my.sql

Create user for db (check parameters from /var/www/cpv_modified/lib/db_params.php)

And try login http://127.0.0.1/cpv_modified/login.php

User/Password for login.php: user/admin