<?php
/***********************************************
*        CPV 2.6 FULL DECODED & NULLED         *
*        MTIMER     www.mtimer.net             *
***********************************************/

include_once "lib/app.inc";
include_once "lib/app-pages.inc";
include_once "license/license.php";
echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
  <head>
    <title>Login</title>
	<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
echo getCurrentUrlWithoutPage();
echo "favicon.ico\" />
	<meta name=\"robots\" content=\"noindex, nofollow\" />
	<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />
	<link href=\"style/login-box.css\" rel=\"stylesheet\" type=\"text/css\" />
  </head>
  <body>
    <form id=\"Form1\" method=\"post\">
";

if (!isset($_REQUEST['btnSubmit_x']) && isset($_GET['logout'])) {
	AppLogout();
}

echo "		";
echo "<s";
echo "cript type=\"text/javascript\">
function showExpired(value)
			{
			document.getElementById('spanExpired').innerHTML=value;
			document.getElementById('divExpired').style.display='block';
			}
</script>
	<div align=\"center\" style=\"padding: 100px 0 0 0px;\">
";
clearstatcache();
$existingFiles = "";

if (file_exists("install.php")) {
	$existingFiles = "'install.php'";
}


if (file_exists("upgrade.php")) {
	$existingFiles .= ($existingFiles == ""?"":" and ") . "'upgrade.php'";
}


if ($existingFiles) {
	echo "<div class=\"divMessage red\"><br/>Security Warning: You have not removed the " . $existingFiles . " file" . (strpos($existingFiles, " and ")?"s":"") . "!</div>";
}

$existingFiles = "";
$filePerm = substr(sprintf("%o", fileperms("lib/db_params.php")), 0 - 4);

if ($filePerm == "0666" || $filePerm == "0777") {
	$existingFiles = "'lib/db_params.php'";
}

$filePerm = substr(sprintf("%o", fileperms("license/license.php")), 0 - 4);

if ($filePerm == "0666" || $filePerm == "0777") {
	$existingFiles .= ($existingFiles == ""?"":" and ") . "'license/license.php'";
}


if ($existingFiles) {
	echo "<div class=\"divMessage red\"><br/>Security Warning: You have to reset permissions to 644 for the " . $existingFiles . " file" . (strpos($existingFiles, " and ")?"s":"") . "!</div>";
}

echo "  <div id=\"divExpired\" class=\"divMessage red\" style=\"display:none\"><br/>This version is not accessible. Your Annual Update Renewal is expired.<br/>
				<a href=\"http://cpvlab.com/update_license\">Click Here</a> to Update your License and Run this Version.
				To Rollback to a previous version, download your max version level: ";
echo "<s";
echo "pan id=\"spanExpired\"></span>
			<br/>Login here: <a href=\"http://cpvlab.com/users\">http://cpvlab.com/users</a> & Run the Update Process
  </div>
";

if (isset($_GET['license']) && strpos($_GET['license'], "Expired") === 0) {
	echo "<script type=\"text/javascript\">showExpired(\"" . str_replace("Expired|", "", $_GET['license']) . "\");</script>";
}

echo "  <div id=\"login-box\">
    <h2>&nbsp;</h2>
    <div id=\"login-box-name\" style=\"margin-top:20px;\">Username:</div>
	<div id=\"login-box-field\" style=\"margin-top:20px;\">
		<input name=\"txtUsername\" class=\"form-login\" title=\"Username\" value=\"\" size=\"30\" maxlength=\"255\" />
	</div>
<div id=\"login-box-name\">Password:</div>
<div id=\"login-box-field\">
<input name=\"txtPassword\" type=\"password\" class=\"form-login\"";
echo " title=\"Password\" value=\"\" size=\"30\" maxlength=\"255\" />
</div>
<br />
<br />
<input name=\"btnSubmit\" type=\"image\" src=\"images/login-btn.png\" value=\"Enter\" style=\"margin-left:90px;width:103px;height:42px\" />
<br/><br/>
";

if (isset($_REQUEST['btnSubmit_x'])) {
	if (strpos($_REQUEST['txtUsername'], "\'") !== false) {
		echo "	<div class=\"divMessage\">Invalid login!</div>
		";
	}
	else {
		$fields = array("UserID", "LastLogin", "SessionTimeout", "DefaultPage");
		$user = $app->shared_db->get("users", $fields, $count, "Username=" . $app->shared_db->_sql_quote($_REQUEST['txtUsername']) . " AND Password=" . $app->shared_db->_sql_quote(md5($_REQUEST['txtPassword'])));

		if (isset($user[0]) && $user[0]['UserID']) {
			$fullDays = datediff("d", $user[0]['LastLogin'], date("Y-m-d H:i:s"));
			$fullDays = 1;

			if ($fullDays <= 0) {
				$style = $_SESSION['SESSION'] = "00000000-0000-0000-0000-000000000000";
			}
			else {
				$style = visualStyles();
			}


			if ($style == "Maximum") {
				echo "			<div class=\"divMessage\">The maximum number of domains for this application has been reached</div>
				";
			}
			else {
					if (strpos($style, "Expired") === 0) {
						echo "	";
						echo "<s";
						echo "cript type=\"text/javascript\">showExpired('";
						echo str_replace("Expired|", "", $style);
						echo "');</script>
				";
					}
					else {
							$_SESSION['USER_IDPpv'] = $app->userid = $user[0]['UserID'];
							$_SESSION['LASTLOGIN'] = $user[0]['LastLogin'];
							$_SESSION['SessionTimeout'] = $user[0]['SessionTimeout'] * 60;
							$loginPage = getConfigValue($app, "loginpage");
							$loginPageName = ((isset($loginPage) && isset($loginPage[0]))?$loginPage[0]:"login.php");
							$_SESSION['LoginPage'] = $loginPageName;
							setcookie("LoginPage", $loginPageName, time() + 24 * 3600);
							$app->shared_db->update("users", array("LastLogin" => date("Y-m-d H:i:s")), "UserID=" . $user[0]['UserID']);
							$redirectPage = urldecode((isset($_REQUEST['ret'])?$_REQUEST['ret']:""));

							if (!$redirectPage) {
								$redirectPage = ((isset($user[0]['DefaultPage']) && $user[0]['DefaultPage'])?$user[0]['DefaultPage']:"campaigns.php");
							}

							redirectUrl($redirectPage);
						
					}
				
			}
		}
		else {
			$ipAddress = ip2long(getIpAddress());

			if (!$ipAddress) {
				$ipAddress = 2130706433;
			}

			$app->shared_db->insert_auto_incremented("logins", array("LoginDate" => date("Y-m-d H:i:s"), "IPAddress" => sprintf("%u", $ipAddress), "Username" => $_REQUEST['txtUsername'], "Password" => $_REQUEST['txtPassword']));
			echo "	<div class=\"divMessage\">Invalid login</div>
			";
		}
	}
}

echo "</div>
</div>

";

if ($handle = opendir("images/")) {
	while (false !== $file = readdir($handle)) {
		if ((((((((((($file != "." && $file != "..") && $file != "login-box-backg.png") && $file != "login-btn.png") && $file != "header_center.jpg") && $file != "footer_center.jpg") && $file != "Thumbs.db") && $file != "vssver2.scc") && $file != "backgrounds") && $file != "flags") && $file != "prettyPhoto") && $file != "index.htm") {
			echo "<img src=\"images/" . $file . "\" class=\"hidden\"/>";
			continue;
		}
	}

	closedir($handle);
}

echo "    </form>
  </body>
</html>";
?>
