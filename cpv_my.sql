-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 08, 2015 at 09:13 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cpv_base`
--

-- --------------------------------------------------------

--
-- Table structure for table `affiliatesources`
--

CREATE TABLE IF NOT EXISTS `affiliatesources` (
  `AffiliateSourceID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Affiliate` varchar(255) NOT NULL,
  `RevenueParam` varchar(45) NOT NULL DEFAULT 'revenue',
  `SubIdSeparator` varchar(20) NOT NULL DEFAULT '_',
  `DateAdded` datetime NOT NULL,
  PRIMARY KEY (`AffiliateSourceID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `affiliatesources`
--

INSERT INTO `affiliatesources` (`AffiliateSourceID`, `Affiliate`, `RevenueParam`, `SubIdSeparator`, `DateAdded`) VALUES
(1, 'Azoogle', 'revenue', '_', '2010-01-01 00:00:00'),
(2, 'Clickbank', 'revenue', 'z', '2010-01-01 00:00:00'),
(3, 'Clickbooth', 'revenue', '_', '2010-01-01 00:00:00'),
(4, 'Copeac', 'revenue', '_', '2010-01-01 00:00:00'),
(5, 'EWA', 'AffiliateCommission', '_', '2010-01-01 00:00:00'),
(6, 'Hydra', 'revenue', '_', '2010-01-01 00:00:00'),
(7, 'NeverBlue', 'revenue', '_', '2010-01-01 00:00:00'),
(8, 'PKM', 'revenue', '_', '2010-01-01 00:00:00'),
(9, 'Wolf Storm Media', 'revenue', '_', '2010-01-01 00:00:00'),
(10, 'Adsimilis', 'revenue', '_', '2015-08-23 09:21:28');

-- --------------------------------------------------------

--
-- Table structure for table `alertprofiles`
--

CREATE TABLE IF NOT EXISTS `alertprofiles` (
  `AlertProfileID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `AlertProfileName` varchar(200) NOT NULL,
  `DefaultProfile` tinyint(1) NOT NULL DEFAULT '0',
  `alert1Views` varchar(10) DEFAULT NULL,
  `alert1Conversion` varchar(10) DEFAULT NULL,
  `alert2Views` varchar(10) DEFAULT NULL,
  `alert2Clicks` varchar(10) DEFAULT NULL,
  `alert3Views` varchar(10) DEFAULT NULL,
  `alert3Subscribers` varchar(10) DEFAULT NULL,
  `alert4Views` varchar(10) DEFAULT NULL,
  `alert4SR` varchar(10) DEFAULT NULL,
  `alert5Views` varchar(10) DEFAULT NULL,
  `alert5CTR` varchar(10) DEFAULT NULL,
  `alert6Views` varchar(10) DEFAULT NULL,
  `alert6CR` varchar(10) DEFAULT NULL,
  `calert1Views` varchar(10) DEFAULT NULL,
  `calert2Views` varchar(10) DEFAULT NULL,
  `CreateDate` datetime NOT NULL,
  `ModifyDate` datetime DEFAULT NULL,
  PRIMARY KEY (`AlertProfileID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `alertprofiles`
--

INSERT INTO `alertprofiles` (`AlertProfileID`, `AlertProfileName`, `DefaultProfile`, `alert1Views`, `alert1Conversion`, `alert2Views`, `alert2Clicks`, `alert3Views`, `alert3Subscribers`, `alert4Views`, `alert4SR`, `alert5Views`, `alert5CTR`, `alert6Views`, `alert6CR`, `calert1Views`, `calert2Views`, `CreateDate`, `ModifyDate`) VALUES
(1, 'Default Alert Profile', 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-12-01 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `alerts`
--

CREATE TABLE IF NOT EXISTS `alerts` (
  `AlertID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AlertName` varchar(45) NOT NULL,
  `AlertValue` varchar(45) NOT NULL,
  PRIMARY KEY (`AlertID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `alerts`
--

INSERT INTO `alerts` (`AlertID`, `AlertName`, `AlertValue`) VALUES
(1, 'alert1Views', ''),
(2, 'alert1Conversion', ''),
(3, 'alert2Views', ''),
(4, 'alert2Clicks', ''),
(5, 'alert3Views', ''),
(6, 'alert3Subscribers', ''),
(7, 'alert4Views', ''),
(8, 'alert4SR', ''),
(9, 'calert1Views', ''),
(10, 'calert2Views', ''),
(11, 'alert5Views', ''),
(12, 'alert5CTR', ''),
(13, 'alert6Views', ''),
(14, 'alert6CR', '');

-- --------------------------------------------------------

--
-- Table structure for table `blockedclicks`
--

CREATE TABLE IF NOT EXISTS `blockedclicks` (
  `ClickID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CampaignID` int(10) unsigned DEFAULT NULL,
  `SubIdID` int(10) unsigned DEFAULT NULL,
  `ViewDate` datetime DEFAULT NULL,
  `BlockReason` tinyint(4) DEFAULT '1',
  `IPAddress` int(10) unsigned NOT NULL,
  `UserAgent` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ClickID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blockrules`
--

CREATE TABLE IF NOT EXISTS `blockrules` (
  `BlockRuleID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `BlockName` varchar(100) DEFAULT NULL,
  `StartIP` int(10) unsigned DEFAULT NULL,
  `EndIP` int(10) unsigned DEFAULT NULL,
  `BlockUA` varchar(500) DEFAULT NULL,
  `Active` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`BlockRuleID`),
  KEY `idxStartIPEndIP` (`StartIP`,`EndIP`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `blockrules`
--

INSERT INTO `blockrules` (`BlockRuleID`, `BlockName`, `StartIP`, `EndIP`, `BlockUA`, `Active`) VALUES
(1, 'Google IP 1', 1208926208, 1208942591, NULL, 1),
(2, 'Google IP 2', 3512041472, 3512074239, NULL, 1),
(3, 'Google IP 3', 1123631104, 1123639295, NULL, 1),
(4, 'Google IP 4', 1089052672, 1089060863, NULL, 1),
(5, 'Google IP 5', 782925824, 782893057, NULL, 1),
(6, 'Google IP 6', 1113980928, 1113985023, NULL, 1),
(7, 'Google IP 7', 1249705984, 1249771519, NULL, 1),
(8, 'Google IP 8', 3639549952, 3639558143, NULL, 1),
(9, 'MSN IP', 1093926912, 1094189055, NULL, 1),
(10, 'Microsoft IP', 3475898368, 3475963903, NULL, 1),
(11, 'Bing IP 1', 1074003968, 1074020351, NULL, 1),
(12, 'Bing IP 2', 2637561856, 2638020607, NULL, 1),
(13, 'Bing IP 3', 3477372928, 3477393407, NULL, 1),
(14, 'Yahoo IP 1', 3640418304, 3640426495, NULL, 1),
(15, 'Yahoo IP 2', 3515031552, 3515039743, NULL, 1),
(16, 'Yahoo IP 3', 3633393664, 3633397759, NULL, 1),
(17, 'Yahoo IP 4', 1209925632, 1209991167, NULL, 1),
(18, 'Yahoo IP 5', 1241907200, 1241972735, NULL, 1),
(19, 'Yahoo IP 6', 135041024, 135041279, NULL, 1),
(20, 'Yahoo IP 7', 1120157696, 1120174079, NULL, 1),
(21, 'Yahoo IP 8', 1122279424, 1122287615, NULL, 1),
(22, 'Yahoo IP 9', 1136852992, 1136918527, NULL, 1),
(23, 'Yahoo IP 10', 1150205952, 1150222335, NULL, 1),
(24, 'Yahoo IP 11', 3399528448, 3399532543, NULL, 1),
(25, 'Yahoo IP 12', 3518971904, 3518988287, NULL, 1),
(26, 'Yahoo IP 13', 3279950355, 3279950355, NULL, 1),
(27, 'L3C IP', 1094189056, 1094451199, NULL, 1),
(28, 'Google UA', NULL, NULL, 'Googlebot', 1),
(29, 'Yahoo UA 1', NULL, NULL, 'Yahoo! Slurp', 1),
(30, 'Yahoo UA 2', NULL, NULL, 'YahooSeeker', 1),
(31, 'MSN UA', NULL, NULL, 'msnbot', 1),
(32, 'Bing UA', NULL, NULL, 'bingbot', 1),
(33, 'PSI IP', 637534208, 654311423, NULL, 1),
(34, 'AdOn IP', 204456961, 204457215, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `campaigngroups`
--

CREATE TABLE IF NOT EXISTS `campaigngroups` (
  `CampaignGroupID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `CampaignGroup` varchar(100) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`CampaignGroupID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `campaigngroups`
--

INSERT INTO `campaigngroups` (`CampaignGroupID`, `CampaignGroup`, `CreateDate`) VALUES
(1, 'ACTIVE', '2015-08-23 09:24:42'),
(2, 'inactive', '2015-08-23 09:24:53');

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE IF NOT EXISTS `campaigns` (
  `CampaignID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CampaignName` varchar(250) NOT NULL DEFAULT '',
  `CampaignTypeID` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `SourceID` varchar(100) NOT NULL DEFAULT '',
  `Source` varchar(250) NOT NULL DEFAULT '',
  `KeywordToken` varchar(100) NOT NULL DEFAULT '',
  `UniqueToken` varchar(100) NOT NULL DEFAULT '',
  `PassUnique` tinyint(4) NOT NULL DEFAULT '0',
  `AppendToken` varchar(255) NOT NULL DEFAULT '',
  `EngageSeconds` smallint(5) unsigned NOT NULL DEFAULT '0',
  `CreateDate` datetime NOT NULL,
  `CreateUserID` int(10) unsigned NOT NULL,
  `ModifyDate` datetime DEFAULT NULL,
  `ModifyUserID` int(10) unsigned DEFAULT NULL,
  `DeleteDate` datetime DEFAULT NULL,
  `DeleteUserID` int(10) unsigned DEFAULT NULL,
  `LastViews` int(11) NOT NULL DEFAULT '0',
  `LastViewsNew` int(11) NOT NULL DEFAULT '0',
  `LastProfit` double NOT NULL DEFAULT '0',
  `LastProfitNew` double NOT NULL DEFAULT '0',
  `LastROI` double NOT NULL DEFAULT '0',
  `LastReportUpdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `RealTimeCPV` double NOT NULL DEFAULT '0',
  `CostTypeID` tinyint(4) NOT NULL DEFAULT '1',
  `DestinationType` tinyint(4) NOT NULL DEFAULT '1',
  `TrackingType` tinyint(4) NOT NULL DEFAULT '2',
  `AssignedTo` int(11) NOT NULL DEFAULT '0',
  `CaptureEmbed` tinyint(1) NOT NULL DEFAULT '0',
  `CapturePopUp` tinyint(1) NOT NULL DEFAULT '0',
  `CaptureExitPop` tinyint(1) NOT NULL DEFAULT '0',
  `PassTarget` tinyint(1) NOT NULL DEFAULT '0',
  `PassTargetParam` varchar(45) NOT NULL DEFAULT 'target',
  `PassTargetOffer` tinyint(1) NOT NULL DEFAULT '0',
  `PassTargetOfferParam` varchar(45) NOT NULL DEFAULT 'target',
  `PassSubIdLP` tinyint(1) NOT NULL DEFAULT '0',
  `PassSubId` tinyint(1) NOT NULL DEFAULT '1',
  `PassCookie` tinyint(1) NOT NULL DEFAULT '0',
  `PassCookieParam` varchar(45) NOT NULL DEFAULT 'cookie',
  `RedirectType` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `KeyCode` varchar(45) NOT NULL DEFAULT '',
  `FailurePage` varchar(255) NOT NULL DEFAULT '',
  `GroupID` smallint(5) unsigned NOT NULL DEFAULT '0',
  `Inactive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SplitShare` tinyint(4) NOT NULL DEFAULT '50',
  `ShareLanding` tinyint(4) NOT NULL DEFAULT '0',
  `ShareOffer` tinyint(4) NOT NULL DEFAULT '0',
  `Priority` smallint(5) unsigned NOT NULL DEFAULT '1',
  `OptimizationProfileID` smallint(5) unsigned NOT NULL DEFAULT '1',
  `AlertProfileID` smallint(5) unsigned NOT NULL DEFAULT '1',
  `CaptureReferrer` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ExtraTokens` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ExtraTokenName1` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenUrl1` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenParam1` varchar(100) NOT NULL DEFAULT '',
  `ExtraTokenPass1` tinyint(4) NOT NULL DEFAULT '2',
  `ExtraTokenName2` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenUrl2` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenParam2` varchar(100) NOT NULL DEFAULT '',
  `ExtraTokenPass2` tinyint(4) NOT NULL DEFAULT '2',
  `ExtraTokenName3` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenUrl3` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenParam3` varchar(100) NOT NULL DEFAULT '',
  `ExtraTokenPass3` tinyint(4) NOT NULL DEFAULT '2',
  `ExtraTokenName4` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenUrl4` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenParam4` varchar(100) NOT NULL DEFAULT '',
  `ExtraTokenPass4` tinyint(4) NOT NULL DEFAULT '2',
  `ExtraTokenName5` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenUrl5` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenParam5` varchar(100) NOT NULL DEFAULT '',
  `ExtraTokenPass5` tinyint(4) NOT NULL DEFAULT '2',
  `ExtraTokenName6` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenUrl6` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenParam6` varchar(100) NOT NULL DEFAULT '',
  `ExtraTokenPass6` tinyint(4) NOT NULL DEFAULT '2',
  `ExtraTokenName7` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenUrl7` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenParam7` varchar(100) NOT NULL DEFAULT '',
  `ExtraTokenPass7` tinyint(4) NOT NULL DEFAULT '2',
  `ExtraTokenName8` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenUrl8` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenParam8` varchar(100) NOT NULL DEFAULT '',
  `ExtraTokenPass8` tinyint(4) NOT NULL DEFAULT '2',
  `ExtraTokenName9` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenUrl9` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenParam9` varchar(100) NOT NULL DEFAULT '',
  `ExtraTokenPass9` tinyint(4) NOT NULL DEFAULT '2',
  `ExtraTokenName10` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenUrl10` varchar(250) NOT NULL DEFAULT '',
  `ExtraTokenParam10` varchar(100) NOT NULL DEFAULT '',
  `ExtraTokenPass10` tinyint(4) NOT NULL DEFAULT '2',
  `AdTokenName` varchar(250) NOT NULL DEFAULT '',
  `AdTokenUrl` varchar(250) NOT NULL DEFAULT '',
  `AdTokenParam` varchar(100) NOT NULL DEFAULT '',
  `AdTokenPass` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CampaignID`),
  KEY `idxCampaignTypeID` (`CampaignTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`CampaignID`, `CampaignName`, `CampaignTypeID`, `SourceID`, `Source`, `KeywordToken`, `UniqueToken`, `PassUnique`, `AppendToken`, `EngageSeconds`, `CreateDate`, `CreateUserID`, `ModifyDate`, `ModifyUserID`, `DeleteDate`, `DeleteUserID`, `LastViews`, `LastViewsNew`, `LastProfit`, `LastProfitNew`, `LastROI`, `LastReportUpdate`, `RealTimeCPV`, `CostTypeID`, `DestinationType`, `TrackingType`, `AssignedTo`, `CaptureEmbed`, `CapturePopUp`, `CaptureExitPop`, `PassTarget`, `PassTargetParam`, `PassTargetOffer`, `PassTargetOfferParam`, `PassSubIdLP`, `PassSubId`, `PassCookie`, `PassCookieParam`, `RedirectType`, `KeyCode`, `FailurePage`, `GroupID`, `Inactive`, `SplitShare`, `ShareLanding`, `ShareOffer`, `Priority`, `OptimizationProfileID`, `AlertProfileID`, `CaptureReferrer`, `ExtraTokens`, `ExtraTokenName1`, `ExtraTokenUrl1`, `ExtraTokenParam1`, `ExtraTokenPass1`, `ExtraTokenName2`, `ExtraTokenUrl2`, `ExtraTokenParam2`, `ExtraTokenPass2`, `ExtraTokenName3`, `ExtraTokenUrl3`, `ExtraTokenParam3`, `ExtraTokenPass3`, `ExtraTokenName4`, `ExtraTokenUrl4`, `ExtraTokenParam4`, `ExtraTokenPass4`, `ExtraTokenName5`, `ExtraTokenUrl5`, `ExtraTokenParam5`, `ExtraTokenPass5`, `ExtraTokenName6`, `ExtraTokenUrl6`, `ExtraTokenParam6`, `ExtraTokenPass6`, `ExtraTokenName7`, `ExtraTokenUrl7`, `ExtraTokenParam7`, `ExtraTokenPass7`, `ExtraTokenName8`, `ExtraTokenUrl8`, `ExtraTokenParam8`, `ExtraTokenPass8`, `ExtraTokenName9`, `ExtraTokenUrl9`, `ExtraTokenParam9`, `ExtraTokenPass9`, `ExtraTokenName10`, `ExtraTokenUrl10`, `ExtraTokenParam10`, `ExtraTokenPass10`, `AdTokenName`, `AdTokenUrl`, `AdTokenParam`, `AdTokenPass`) VALUES
(32, 'dk erovie juicy POP', 8, 'nad', 'Ad On Network', 'keyword', '', 0, '', 0, '2015-09-08 00:00:00', 1, '2015-09-08 06:07:57', 1, NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0.03, 2, 1, 2, 0, 0, 0, 0, 0, 'target', 0, 'target', 0, 1, 0, 'cookie', 2, '', '', 1, 0, 50, 0, 0, 1, 1, 1, 1, 0, '', '', '', 2, '', '', '', 0, '', '', '', 0, '', '', '', 0, '', '', '', 0, '', '', '', 0, '', '', '', 0, '', '', '', 0, '', '', '', 0, '', '', '', 0, '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `campaigntypes`
--

CREATE TABLE IF NOT EXISTS `campaigntypes` (
  `CampaignTypeID` tinyint(3) unsigned NOT NULL,
  `CampaignType` varchar(100) NOT NULL,
  `CampaignTypeShort` varchar(100) DEFAULT NULL,
  `OrderIndex` tinyint(3) unsigned NOT NULL,
  `PageName` varchar(100) DEFAULT NULL,
  `SubTitleImage` varchar(100) DEFAULT NULL,
  `AnchorText` varchar(100) DEFAULT NULL,
  `MenuText` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CampaignTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campaigntypes`
--

INSERT INTO `campaigntypes` (`CampaignTypeID`, `CampaignType`, `CampaignTypeShort`, `OrderIndex`, `PageName`, `SubTitleImage`, `AnchorText`, `MenuText`) VALUES
(4, 'Lead Capture / Opt-In Campaign', 'Lead Capture', 5, 'lead-capture-campaign.php', 'lead_cap_sub_title.png', 'LeadCapture', 'Lead Capture'),
(5, 'Multiple Path Campaign', 'Multiple Path', 3, 'multiple-path-campaign.php', 'multiple path sub title original.png', 'MultiplePaths', 'Multiple Paths'),
(6, 'Landing Page Sequence Campaign', 'Landing Page Sequence', 4, 'landing-page-sequence-campaign.php', 'landing_page_sequence_campaign_title.png', 'LPSequence', 'Landing Page Sequence'),
(7, 'Multiple Option Campaign', 'Multiple Option', 2, 'multiple-cta-campaign.php', 'multiple_cta_campaign_title.png', 'MultipleCta', 'Multiple Options'),
(8, 'Direct Link & Landing Page Campaign', 'Direct Link & Landing Page', 1, 'direct-and-landing-campaign.php', 'directlink_vs_lp_sub_title.png', 'DirectAndLanding', 'Direct Link & Landing Page'),
(9, 'Email Follow-Up Campaign', 'Email Follow-up', 6, 'email-follow-up-campaign.php', 'email_followup_sub_title.png', 'EmailFollowUp', 'Email Follow-Up');

-- --------------------------------------------------------

--
-- Table structure for table `clicks`
--

CREATE TABLE IF NOT EXISTS `clicks` (
  `ClickID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CampaignID` int(10) unsigned NOT NULL,
  `SubIdID` int(10) unsigned NOT NULL,
  `DestinationID` int(10) unsigned NOT NULL,
  `OfferID` int(10) unsigned DEFAULT NULL,
  `ViewDate` datetime NOT NULL,
  `EngageDate` datetime DEFAULT NULL,
  `ClickDate` datetime DEFAULT NULL,
  `ConversionDate` datetime DEFAULT NULL,
  `SubscribeDate` datetime DEFAULT NULL,
  `ConversionDateReport` datetime DEFAULT NULL,
  `IPAddress` int(10) unsigned NOT NULL DEFAULT '2130706433',
  `Revenue` double DEFAULT NULL,
  `IsDup` tinyint(4) NOT NULL DEFAULT '0',
  `ReferrerID` int(10) unsigned NOT NULL DEFAULT '1',
  `SiteCategoryID` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ClickID`),
  KEY `idxCampaignID` (`CampaignID`),
  KEY `idxCampaignIDViewDate` (`CampaignID`,`ViewDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clicksextra`
--

CREATE TABLE IF NOT EXISTS `clicksextra` (
  `ClickID` bigint(20) unsigned NOT NULL,
  `Extra1` varchar(500) NOT NULL DEFAULT '',
  `Extra2` varchar(500) NOT NULL DEFAULT '',
  `Extra3` varchar(500) NOT NULL DEFAULT '',
  `Extra4` varchar(500) NOT NULL DEFAULT '',
  `Extra5` varchar(500) NOT NULL DEFAULT '',
  `Extra6` varchar(500) NOT NULL DEFAULT '',
  `Extra7` varchar(500) NOT NULL DEFAULT '',
  `Extra8` varchar(500) NOT NULL DEFAULT '',
  `Extra9` varchar(500) NOT NULL DEFAULT '',
  `Extra10` varchar(500) NOT NULL DEFAULT '',
  `AdValue` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`ClickID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clickslp`
--

CREATE TABLE IF NOT EXISTS `clickslp` (
  `ClickID` bigint(20) unsigned NOT NULL,
  `Level` tinyint(4) NOT NULL,
  `DestinationID` int(10) unsigned DEFAULT NULL,
  `ClickDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ClickID`,`Level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clicksopt`
--

CREATE TABLE IF NOT EXISTS `clicksopt` (
  `ClickID` bigint(20) unsigned NOT NULL,
  `OptEmbedDate` datetime DEFAULT NULL,
  `OptPopUpDate` datetime DEFAULT NULL,
  `OptExitPopDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ClickID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clickstscode`
--

CREATE TABLE IF NOT EXISTS `clickstscode` (
  `ClickID` bigint(20) unsigned NOT NULL,
  `UniqueCode` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`ClickID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `columns`
--

CREATE TABLE IF NOT EXISTS `columns` (
  `ColumnID` tinyint(3) unsigned NOT NULL,
  `ColumnName` varchar(200) NOT NULL,
  `ColumnSort` varchar(200) NOT NULL,
  `ClassName` varchar(200) NOT NULL DEFAULT '',
  `ColumnColor` varchar(10) NOT NULL DEFAULT '034CB5',
  `DecimalPlaces` tinyint(4) NOT NULL DEFAULT '-1',
  `IsPercent` tinyint(1) NOT NULL DEFAULT '0',
  `IsDollar` tinyint(1) NOT NULL DEFAULT '0',
  `IsDateTime` tinyint(1) NOT NULL DEFAULT '0',
  `IsSortable` tinyint(1) NOT NULL DEFAULT '1',
  `IsDatabaseSortable` tinyint(1) NOT NULL DEFAULT '0',
  `IsFixed` tinyint(1) NOT NULL DEFAULT '0',
  `IsTotal` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ColumnID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `columns`
--

INSERT INTO `columns` (`ColumnID`, `ColumnName`, `ColumnSort`, `ClassName`, `ColumnColor`, `DecimalPlaces`, `IsPercent`, `IsDollar`, `IsDateTime`, `IsSortable`, `IsDatabaseSortable`, `IsFixed`, `IsTotal`) VALUES
(1, 'CampaignID', 'CampaignID', '', '034CB5', -1, 0, 0, 0, 0, 0, 0, 0),
(2, 'Offer', 'Offer', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(3, 'Url', 'Url', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(4, 'Keyword', 'Keyword', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(5, 'Views', 'Views', '', '034CB5', 0, 0, 0, 0, 1, 1, 0, 1),
(6, 'Cost', 'Cost', '', '034CB5', 2, 0, 1, 0, 1, 0, 0, 1),
(7, 'CPV', 'CPV', '', '034CB5', 3, 0, 1, 0, 1, 0, 0, 1),
(8, 'Clicks', 'Clicks', '', '034CB5', 0, 0, 0, 0, 1, 1, 0, 1),
(9, 'CTR', 'CTR', '', '034CB5', 2, 1, 0, 0, 1, 0, 0, 1),
(10, 'CPC', 'CPC', '', '034CB5', 2, 0, 1, 0, 1, 0, 0, 1),
(11, 'Conversion', 'Conversion', '', '034CB5', 0, 0, 0, 0, 1, 1, 0, 1),
(12, 'CR', 'CR', '', '034CB5', 2, 1, 0, 0, 1, 0, 0, 1),
(13, 'Revenue', 'Revenue', '', '034CB5', 2, 0, 1, 0, 1, 1, 0, 1),
(14, 'EPV', 'EPV', '', '034CB5', 3, 0, 1, 0, 1, 0, 0, 1),
(15, 'PPV', 'PPV', '', '034CB5', 3, 0, 1, 0, 1, 0, 0, 1),
(16, 'eCPM', 'eCPM', '', '034CB5', 2, 0, 1, 0, 1, 0, 0, 1),
(17, 'Profit', 'Profit', '', '034CB5', 2, 0, 1, 0, 1, 0, 0, 1),
(18, 'ROI', 'ROI', '', '034CB5', 2, 1, 0, 0, 1, 0, 0, 1),
(19, 'EngageRate', 'EngageRate', '', '034CB5', 2, 1, 0, 0, 1, 0, 0, 1),
(20, 'TheIntervalName', 'TheIntervalName', 'left bold', '034CB5', -1, 0, 0, 0, 0, 0, 0, 0),
(21, 'Offer2', 'Offer2', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(22, 'Url2', 'Url2', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(23, 'CostRevenueProfit', 'CostRevenueProfit', '', '034CB5', -1, 0, 1, 0, 0, 0, 0, 0),
(24, 'Sent', 'Sent', '', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(25, 'EPS', 'EPS', '', '034CB5', 2, 0, 1, 0, 1, 0, 0, 0),
(26, 'Embed', 'Embed', '', '034CB5', 0, 0, 0, 0, 0, 0, 0, 1),
(27, 'EmbedPercent', 'EmbedPercent', '', '034CB5', 2, 1, 0, 0, 0, 0, 0, 1),
(28, 'Popup', 'Popup', '', '034CB5', 0, 0, 0, 0, 0, 0, 0, 1),
(29, 'PopupPercent', 'PopupPercent', '', '034CB5', 2, 1, 0, 0, 0, 0, 0, 1),
(30, 'Exit', 'Exit', '', '034CB5', 0, 0, 0, 0, 0, 0, 0, 1),
(31, 'ExitPercent', 'ExitPercent', '', '034CB5', 2, 1, 0, 0, 0, 0, 0, 1),
(32, 'DestinationID', 'DestinationID', '', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(33, 'Email', 'Email', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(34, 'Subscribers', 'Subscribers', '', '034CB5', 0, 0, 0, 0, 1, 1, 0, 1),
(35, 'SR', 'SR', '', '034CB5', 2, 1, 0, 0, 1, 0, 0, 1),
(36, 'CPSUB', 'CPSUB', '', '034CB5', 2, 0, 1, 0, 1, 0, 0, 1),
(37, 'Extra1', 'Extra1', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(38, 'Extra2', 'Extra2', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(39, 'Extra3', 'Extra3', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(40, 'Extra4', 'Extra4', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(41, 'Extra5', 'Extra5', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(42, 'Extra6', 'Extra6', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(43, 'Extra7', 'Extra7', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(44, 'Extra8', 'Extra8', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(45, 'Extra9', 'Extra9', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(46, 'Extra10', 'Extra10', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(47, 'CPA', 'CPA', '', '034CB5', 2, 0, 1, 0, 1, 0, 0, 1),
(48, 'ViewDate', 'ViewDate', '', '034CB5', -1, 0, 0, 1, 1, 0, 0, 0),
(49, 'Source', 'Source', 'left', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(50, 'CampaignName', 'CampaignName', 'left', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(51, 'IP', 'IPAddress', 'left', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(52, 'ClickDate', 'ClickDate', '', '034CB5', -1, 0, 0, 1, 1, 0, 0, 0),
(53, 'ClickStatus', 'ClickStatus', '', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(54, 'Referrer', 'Referrer', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(55, 'ReferrerDomain', 'ReferrerDomain', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(56, 'ConversionDate', 'ConversionDate', '', '034CB5', -1, 0, 0, 1, 1, 0, 0, 0),
(57, 'ConversionStatus', 'ConversionStatus', '', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(58, 'SubId', 'SubId', '', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(59, 'Country', 'Country', 'left', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(60, 'State', 'State', 'left', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(61, 'City', 'City', 'left', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(62, 'LandingPageID', 'LandingPageID', '', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(63, 'SiteCategory', 'SiteCategory', 'left', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(64, 'AdValue', 'AdValue', 'left', '034CB5', -1, 0, 0, 0, 1, 1, 0, 0),
(65, 'Inactive', 'Inactive', '', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(66, 'CampaignGroup', 'CampaignGroup', '', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(67, 'CostTypeID', 'CostTypeID', '', '034CB5', -1, 0, 0, 0, 1, 0, 0, 0),
(68, 'CreateDate', 'CreateDate', '', '034CB5', -1, 0, 0, 1, 1, 0, 0, 0),
(69, 'LastReportUpdate', 'LastReportUpdate', '', '034CB5', -1, 0, 0, 1, 1, 0, 0, 0),
(70, 'ViewDay', 'ViewDay', '', '034CB5', 0, 0, 0, 1, 1, 1, 0, 0),
(71, 'ViewHour', 'ViewHour', '', '034CB5', 0, 0, 0, 0, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `columnscharts`
--

CREATE TABLE IF NOT EXISTS `columnscharts` (
  `ColumnID` tinyint(3) unsigned NOT NULL,
  `CampaignTypeID` tinyint(3) unsigned NOT NULL,
  `ReportTypeID` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `ColumnCaption` varchar(100) NOT NULL,
  `ColumnCaption2` varchar(200) NOT NULL,
  `ColumnIndex` smallint(5) unsigned NOT NULL,
  `IsVisible` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`ColumnID`,`CampaignTypeID`,`ReportTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `columnscharts`
--

INSERT INTO `columnscharts` (`ColumnID`, `CampaignTypeID`, `ReportTypeID`, `ColumnCaption`, `ColumnCaption2`, `ColumnIndex`, `IsVisible`) VALUES
(5, 4, 5, 'Views', 'Visitors', 100, 1),
(5, 4, 7, 'Views', 'Visitors', 100, 1),
(5, 4, 8, 'Views', 'Visitors', 100, 1),
(5, 4, 13, 'Views', 'Visitors', 100, 1),
(5, 5, 5, 'Views', 'Visitors', 100, 1),
(5, 5, 7, 'Views', 'Visitors', 100, 1),
(5, 5, 8, 'Views', 'Visitors', 100, 1),
(5, 5, 13, 'Views', 'Visitors', 100, 1),
(5, 6, 5, 'Views', 'Visitors', 100, 1),
(5, 6, 7, 'Views', 'Visitors', 100, 1),
(5, 6, 8, 'Views', 'Visitors', 100, 1),
(5, 6, 11, 'Visitors', 'Visitors', 500, 1),
(5, 6, 13, 'Views', 'Visitors', 100, 1),
(5, 7, 5, 'Views', 'Visitors', 100, 1),
(5, 7, 7, 'Views', 'Visitors', 100, 1),
(5, 7, 8, 'Views', 'Visitors', 100, 1),
(5, 7, 13, 'Views', 'Visitors', 100, 1),
(5, 8, 5, 'Views', 'Visitors', 100, 1),
(5, 8, 7, 'Views', 'Visitors', 100, 1),
(5, 8, 8, 'Views', 'Visitors', 100, 1),
(5, 8, 13, 'Views', 'Visitors', 100, 1),
(5, 9, 5, 'Views', 'Views', 100, 1),
(5, 9, 7, 'Views', 'Views', 100, 1),
(5, 9, 8, 'Views', 'Views', 100, 1),
(5, 9, 13, 'Views', 'Views', 100, 1),
(8, 4, 5, 'Clicks', 'LP Clicks', 300, 0),
(8, 4, 6, 'Visitors', 'Visitors', 200, 1),
(8, 5, 5, 'Clicks', 'LP Clicks', 300, 0),
(8, 5, 6, 'Visitors', 'Visitors', 200, 1),
(8, 5, 7, 'Clicks', 'LP Clicks', 200, 0),
(8, 5, 8, 'Clicks', 'LP Clicks', 200, 0),
(8, 5, 13, 'Clicks', 'LP Clicks', 200, 0),
(8, 6, 5, 'Clicks', 'LP Clicks', 300, 0),
(8, 6, 6, 'Visitors', 'Visitors', 200, 1),
(8, 6, 7, 'Clicks', 'LP Clicks', 200, 0),
(8, 6, 8, 'Clicks', 'LP Clicks', 200, 0),
(8, 6, 13, 'Clicks', 'LP Clicks', 200, 0),
(8, 7, 5, 'Clicks', 'LP Clicks', 300, 0),
(8, 7, 6, 'Visitors', 'Visitors', 200, 1),
(8, 7, 7, 'Clicks', 'LP Clicks', 200, 0),
(8, 7, 8, 'Clicks', 'LP Clicks', 200, 0),
(8, 7, 13, 'Clicks', 'LP Clicks', 200, 0),
(8, 8, 5, 'Clicks', 'LP Clicks', 300, 0),
(8, 8, 6, 'Visitors', 'Visitors', 200, 1),
(8, 8, 7, 'Clicks', 'LP Clicks', 200, 0),
(8, 8, 8, 'Clicks', 'LP Clicks', 200, 0),
(8, 8, 13, 'Clicks', 'LP Clicks', 200, 0),
(8, 9, 4, 'Clicks', 'Clicks', 100, 1),
(8, 9, 5, 'Clicks', 'Clicks', 300, 0),
(8, 9, 6, 'Visitors', 'Visitors', 100, 1),
(8, 9, 7, 'Clicks', 'Clicks', 200, 0),
(8, 9, 8, 'Clicks', 'Clicks', 200, 0),
(8, 9, 13, 'Clicks', 'Clicks', 200, 0),
(9, 4, 5, 'CTR%', 'LP CTR%', 400, 0),
(9, 5, 5, 'CTR%', 'LP CTR%', 400, 0),
(9, 5, 7, 'CTR', 'LP CTR', 300, 0),
(9, 5, 8, 'CTR', 'LP CTR', 300, 0),
(9, 5, 13, 'CTR', 'LP CTR', 300, 0),
(9, 6, 5, 'CTR%', 'LP CTR%', 400, 0),
(9, 6, 7, 'CTR', 'LP CTR', 300, 0),
(9, 6, 8, 'CTR', 'LP CTR', 300, 0),
(9, 6, 13, 'CTR', 'LP CTR', 300, 0),
(9, 7, 5, 'CTR%', 'LP CTR%', 400, 0),
(9, 7, 7, 'CTR', 'LP CTR', 300, 0),
(9, 7, 8, 'CTR', 'LP CTR', 300, 0),
(9, 7, 13, 'CTR', 'LP CTR', 300, 0),
(9, 8, 5, 'CTR%', 'LP CTR%', 400, 0),
(9, 8, 7, 'CTR', 'LP CTR', 300, 0),
(9, 8, 8, 'CTR', 'LP CTR', 300, 0),
(9, 8, 13, 'CTR', 'LP CTR', 300, 0),
(9, 9, 4, 'CTR%', 'CTR%', 200, 0),
(9, 9, 5, 'CTR%', 'CTR%', 400, 0),
(9, 9, 7, 'CTR', 'CTR', 300, 0),
(9, 9, 8, 'CTR', 'CTR', 300, 0),
(9, 9, 13, 'CTR', 'CTR', 300, 0),
(10, 4, 5, 'CPC', 'LP CPC', 500, 0),
(10, 4, 6, 'CPC', 'CPC', 300, 0),
(10, 5, 5, 'CPC', 'LP CPC', 500, 0),
(10, 5, 6, 'CPC', 'CPC', 300, 0),
(10, 6, 5, 'CPC', 'LP CPC', 500, 0),
(10, 6, 6, 'CPC', 'CPC', 300, 0),
(10, 7, 5, 'CPC', 'LP CPC', 500, 0),
(10, 7, 6, 'CPC', 'CPC', 300, 0),
(10, 8, 5, 'CPC', 'LP CPC', 500, 0),
(10, 8, 6, 'CPC', 'CPC', 300, 0),
(11, 4, 5, 'Conversions', 'Conversions', 900, 0),
(11, 4, 6, 'Conversions', 'Conversions', 400, 0),
(11, 4, 7, 'Conversions', 'Conversions', 300, 0),
(11, 4, 8, 'Conversions', 'Conversions', 300, 0),
(11, 4, 13, 'Conversions', 'Conversions', 300, 0),
(11, 5, 5, 'Conversions', 'Conversions', 600, 0),
(11, 5, 6, 'Conversions', 'Conversions', 400, 0),
(11, 5, 7, 'Conversions', 'Conversions', 400, 0),
(11, 5, 8, 'Conversions', 'Conversions', 400, 0),
(11, 5, 13, 'Conversions', 'Conversions', 400, 0),
(11, 6, 5, 'Conversions', 'Conversions', 600, 0),
(11, 6, 6, 'Conversions', 'Conversions', 400, 0),
(11, 6, 7, 'Conversions', 'Conversions', 400, 0),
(11, 6, 8, 'Conversions', 'Conversions', 400, 0),
(11, 6, 13, 'Conversions', 'Conversions', 400, 0),
(11, 7, 5, 'Conversions', 'Conversions', 600, 0),
(11, 7, 6, 'Conversions', 'Conversions', 400, 0),
(11, 7, 7, 'Conversions', 'Conversions', 400, 0),
(11, 7, 8, 'Conversions', 'Conversions', 400, 0),
(11, 7, 13, 'Conversions', 'Conversions', 400, 0),
(11, 8, 5, 'Conversions', 'Conversions', 600, 0),
(11, 8, 6, 'Conversions', 'Conversions', 400, 0),
(11, 8, 7, 'Conversions', 'Conversions', 400, 0),
(11, 8, 8, 'Conversions', 'Conversions', 400, 0),
(11, 8, 13, 'Conversions', 'Conversions', 400, 0),
(11, 9, 4, 'Conversions', 'Conversions', 300, 0),
(11, 9, 6, 'Conversions', 'Conversions', 200, 0),
(11, 9, 7, 'Conversions', 'Conversions', 400, 0),
(11, 9, 8, 'Conversions', 'Conversions', 400, 0),
(11, 9, 13, 'Conversions', 'Conversions', 400, 0),
(12, 4, 5, 'CR%', 'CR%', 1000, 0),
(12, 4, 6, 'CR%', 'CR%', 600, 0),
(12, 5, 5, 'CR%', 'CR%', 700, 0),
(12, 5, 6, 'CR%', 'CR%', 600, 0),
(12, 5, 7, 'CR', 'CR', 500, 0),
(12, 5, 8, 'CR', 'CR', 500, 0),
(12, 5, 13, 'CR', 'CR', 500, 0),
(12, 6, 5, 'CR%', 'CR%', 700, 0),
(12, 6, 6, 'CR%', 'CR%', 600, 0),
(12, 6, 7, 'CR', 'CR', 500, 0),
(12, 6, 8, 'CR', 'CR', 500, 0),
(12, 6, 13, 'CR', 'CR', 500, 0),
(12, 7, 5, 'CR%', 'CR%', 700, 0),
(12, 7, 6, 'CR%', 'CR%', 600, 0),
(12, 7, 7, 'CR', 'CR', 500, 0),
(12, 7, 8, 'CR', 'CR', 500, 0),
(12, 7, 13, 'CR', 'CR', 500, 0),
(12, 8, 5, 'CR%', 'CR%', 700, 0),
(12, 8, 6, 'CR%', 'CR%', 600, 0),
(12, 8, 7, 'CR', 'CR', 500, 0),
(12, 8, 8, 'CR', 'CR', 500, 0),
(12, 8, 13, 'CR', 'CR', 500, 0),
(12, 9, 4, 'CR%', 'CR%', 400, 0),
(12, 9, 6, 'CR%', 'CR%', 300, 0),
(12, 9, 7, 'CR', 'CR', 500, 0),
(12, 9, 8, 'CR', 'CR', 500, 0),
(12, 9, 13, 'CR', 'CR', 500, 0),
(13, 9, 4, 'Revenue', 'Revenue', 600, 0),
(13, 9, 5, 'Revenue', 'Revenue', 500, 0),
(13, 9, 6, 'Revenue', 'Revenue', 500, 0),
(13, 9, 7, 'Revenue', 'Revenue', 800, 0),
(13, 9, 8, 'Revenue', 'Revenue', 800, 0),
(13, 9, 13, 'Revenue', 'Revenue', 800, 0),
(14, 4, 5, 'EPV', 'EPC', 1200, 0),
(14, 5, 5, 'EPV', 'EPC', 900, 0),
(14, 5, 6, 'EPV', 'EPV', 800, 0),
(14, 5, 7, 'EPV', 'EPC', 600, 0),
(14, 5, 8, 'EPV', 'EPC', 600, 0),
(14, 5, 13, 'EPV', 'EPC', 600, 0),
(14, 6, 5, 'EPV', 'EPC', 900, 0),
(14, 6, 7, 'EPV', 'EPC', 600, 0),
(14, 6, 8, 'EPV', 'EPC', 600, 0),
(14, 6, 13, 'EPV', 'EPC', 600, 0),
(14, 7, 5, 'EPV', 'EPC', 900, 0),
(14, 7, 6, 'EPV', 'EPV', 800, 0),
(14, 7, 7, 'EPV', 'EPC', 600, 0),
(14, 7, 8, 'EPV', 'EPC', 600, 0),
(14, 7, 13, 'EPV', 'EPC', 600, 0),
(14, 8, 5, 'EPV', 'EPC', 900, 0),
(14, 8, 6, 'EPV', 'EPV', 800, 0),
(14, 8, 7, 'EPV', 'EPC', 600, 0),
(14, 8, 8, 'EPV', 'EPC', 600, 0),
(14, 8, 13, 'EPV', 'EPC', 600, 0),
(14, 9, 7, 'EPV', 'EPV', 600, 0),
(14, 9, 8, 'EPV', 'EPV', 600, 0),
(14, 9, 13, 'EPV', 'EPV', 600, 0),
(15, 4, 5, 'PPV', 'PPC', 1300, 0),
(15, 5, 5, 'PPV', 'PPC', 1000, 0),
(15, 5, 6, 'PPV', 'PPV', 900, 0),
(15, 5, 7, 'PPV', 'PPC', 700, 0),
(15, 5, 8, 'PPV', 'PPC', 700, 0),
(15, 5, 13, 'PPV', 'PPC', 700, 0),
(15, 6, 5, 'PPV', 'PPC', 1000, 0),
(15, 6, 7, 'PPV', 'PPC', 700, 0),
(15, 6, 8, 'PPV', 'PPC', 700, 0),
(15, 6, 13, 'PPV', 'PPC', 700, 0),
(15, 7, 5, 'PPV', 'PPC', 1000, 0),
(15, 7, 6, 'PPV', 'PPV', 900, 0),
(15, 7, 7, 'PPV', 'PPC', 700, 0),
(15, 7, 8, 'PPV', 'PPC', 700, 0),
(15, 7, 13, 'PPV', 'PPC', 700, 0),
(15, 8, 5, 'PPV', 'PPC', 1000, 0),
(15, 8, 6, 'PPV', 'PPV', 900, 0),
(15, 8, 7, 'PPV', 'PPC', 700, 0),
(15, 8, 8, 'PPV', 'PPC', 700, 0),
(15, 8, 13, 'PPV', 'PPC', 700, 0),
(15, 9, 7, 'PPV', 'PPV', 700, 0),
(15, 9, 8, 'PPV', 'PPV', 700, 0),
(15, 9, 13, 'PPV', 'PPV', 700, 0),
(16, 5, 6, 'eCPM', 'eCPM', 500, 0),
(16, 7, 6, 'eCPM', 'eCPM', 500, 0),
(16, 8, 6, 'eCPM', 'eCPM', 500, 0),
(17, 4, 7, 'P/L', 'P/L', 600, 0),
(17, 4, 8, 'P/L', 'P/L', 600, 0),
(17, 4, 13, 'P/L', 'P/L', 600, 0),
(17, 5, 7, 'P/L', 'P/L', 800, 0),
(17, 5, 8, 'P/L', 'P/L', 800, 0),
(17, 5, 13, 'P/L', 'P/L', 800, 0),
(17, 6, 7, 'P/L', 'P/L', 800, 0),
(17, 6, 8, 'P/L', 'P/L', 800, 0),
(17, 6, 13, 'P/L', 'P/L', 800, 0),
(17, 7, 7, 'P/L', 'P/L', 800, 0),
(17, 7, 8, 'P/L', 'P/L', 800, 0),
(17, 7, 13, 'P/L', 'P/L', 800, 0),
(17, 8, 7, 'P/L', 'P/L', 800, 0),
(17, 8, 8, 'P/L', 'P/L', 800, 0),
(17, 8, 13, 'P/L', 'P/L', 800, 0),
(18, 4, 5, 'ROI%', 'ROI%', 1400, 0),
(18, 4, 6, 'ROI%', 'ROI%', 1000, 0),
(18, 5, 5, 'ROI%', 'ROI%', 1100, 0),
(18, 5, 6, 'ROI%', 'ROI%', 1000, 0),
(18, 6, 5, 'ROI%', 'ROI%', 1100, 0),
(18, 6, 6, 'ROI%', 'ROI%', 1000, 0),
(18, 7, 5, 'ROI%', 'ROI%', 1100, 0),
(18, 7, 6, 'ROI%', 'ROI%', 1000, 0),
(18, 8, 5, 'ROI%', 'ROI%', 1100, 0),
(18, 8, 6, 'ROI%', 'ROI%', 1000, 0),
(19, 4, 5, 'Engage%', 'Engage%', 200, 0),
(19, 5, 5, 'Engage%', 'Engage%', 200, 0),
(19, 6, 5, 'Engage%', 'Engage%', 200, 0),
(19, 7, 5, 'Engage%', 'Engage%', 200, 0),
(19, 8, 5, 'Engage%', 'Engage%', 200, 0),
(19, 9, 5, 'Engage%', 'Engage%', 200, 0),
(23, 4, 5, 'Revenue-Cost-P/L', 'Revenue-Cost-P/L', 1500, 0),
(23, 4, 6, 'Revenue-Cost-P/L', 'Revenue-Cost-P/L', 1200, 0),
(23, 5, 5, 'Revenue-Cost-P/L', 'Revenue-Cost-P/L', 1200, 0),
(23, 5, 6, 'Revenue-Cost-P/L', 'Revenue-Cost-P/L', 1200, 0),
(23, 6, 5, 'Revenue-Cost-P/L', 'Revenue-Cost-P/L', 1200, 0),
(23, 6, 6, 'Revenue-Cost-P/L', 'Revenue-Cost-P/L', 1200, 0),
(23, 7, 5, 'Revenue-Cost-P/L', 'Revenue-Cost-P/L', 1200, 0),
(23, 7, 6, 'Revenue-Cost-P/L', 'Revenue-Cost-P/L', 1200, 0),
(23, 8, 5, 'Revenue-Cost-P/L', 'Revenue-Cost-P/L', 1200, 0),
(23, 8, 6, 'Revenue-Cost-P/L', 'Revenue-Cost-P/L', 1200, 0),
(26, 6, 11, 'Embed', 'Embed', 600, 0),
(27, 6, 11, 'Embed%', 'Embed%', 700, 0),
(28, 6, 11, 'Popup', 'Popup', 800, 0),
(29, 6, 11, 'Popup%', 'Popup%', 900, 0),
(30, 6, 11, 'Exit', 'Exit', 1000, 0),
(31, 6, 11, 'Exit%', 'Exit%', 1100, 0),
(34, 4, 5, 'Subscribers', 'Subscribers', 600, 0),
(34, 4, 7, 'Subscribers', 'Subscribers', 200, 0),
(34, 4, 8, 'Subscribers', 'Subscribers', 200, 0),
(34, 4, 13, 'Subscribers', 'Subscribers', 200, 0),
(34, 6, 11, 'Subscribers', 'Subscribers', 1200, 0),
(35, 4, 5, 'SR%', 'SR%', 700, 0),
(35, 4, 7, 'SR%', 'SR%', 400, 0),
(35, 4, 8, 'SR%', 'SR%', 400, 0),
(35, 4, 13, 'SR%', 'SR%', 400, 0),
(35, 6, 11, 'SR', 'SR', 1300, 0),
(36, 4, 5, 'CPSUB', 'CPSUB', 800, 0),
(36, 4, 7, 'CPSUB', 'CPSUB', 500, 0),
(36, 4, 8, 'CPSUB', 'CPSUB', 500, 0),
(36, 4, 13, 'CPSUB', 'CPSUB', 500, 0),
(47, 4, 5, 'CPA', 'CPA', 1100, 0),
(47, 4, 6, 'CPA', 'CPA', 700, 0),
(47, 5, 5, 'CPA', 'CPA', 800, 0),
(47, 5, 6, 'CPA', 'CPA', 700, 0),
(47, 6, 5, 'CPA', 'CPA', 800, 0),
(47, 6, 6, 'CPA', 'CPA', 700, 0),
(47, 7, 5, 'CPA', 'CPA', 800, 0),
(47, 7, 6, 'CPA', 'CPA', 700, 0),
(47, 8, 5, 'CPA', 'CPA', 800, 0),
(47, 8, 6, 'CPA', 'CPA', 700, 0),
(47, 9, 4, 'CPA', 'CPA', 500, 0),
(47, 9, 6, 'CPA', 'CPA', 400, 0);

-- --------------------------------------------------------

--
-- Table structure for table `columnstypes`
--

CREATE TABLE IF NOT EXISTS `columnstypes` (
  `ColumnID` tinyint(3) unsigned NOT NULL,
  `CampaignTypeID` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `ReportTypeID` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `ColumnCaption` varchar(200) NOT NULL,
  `ColumnCaption2` varchar(200) NOT NULL,
  `ColumnIndex` smallint(5) unsigned NOT NULL,
  `IsVisible` tinyint(1) NOT NULL,
  `DefaultState` tinyint(1) NOT NULL,
  PRIMARY KEY (`ColumnID`,`CampaignTypeID`,`ReportTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `columnstypes`
--

INSERT INTO `columnstypes` (`ColumnID`, `CampaignTypeID`, `ReportTypeID`, `ColumnCaption`, `ColumnCaption2`, `ColumnIndex`, `IsVisible`, `DefaultState`) VALUES
(1, 1, 14, 'ID', 'ID', 300, 0, 0),
(1, 4, 1, 'ID', 'ID', 100, 1, 1),
(1, 4, 4, 'ID', 'ID', 100, 1, 1),
(1, 4, 9, 'ID', 'ID', 100, 1, 1),
(1, 4, 10, 'ID', 'ID', 100, 1, 1),
(1, 4, 12, 'ID', 'ID', 100, 1, 1),
(1, 5, 1, 'ID', 'ID', 100, 1, 1),
(1, 5, 4, 'ID', 'ID', 100, 1, 1),
(1, 5, 9, 'ID', 'ID', 100, 1, 1),
(1, 5, 10, 'ID', 'ID', 100, 1, 1),
(1, 6, 1, 'ID', 'ID', 100, 1, 1),
(1, 6, 4, 'ID', 'ID', 100, 1, 1),
(1, 6, 9, 'ID', 'ID', 100, 1, 1),
(1, 6, 10, 'ID', 'ID', 100, 1, 1),
(1, 6, 12, 'ID', 'ID', 100, 1, 1),
(1, 7, 1, 'ID', 'ID', 100, 1, 1),
(1, 7, 4, 'ID', 'ID', 100, 1, 1),
(1, 7, 9, 'ID', 'ID', 100, 1, 1),
(1, 7, 10, 'ID', 'ID', 100, 1, 1),
(1, 8, 1, 'ID', 'ID', 100, 1, 1),
(1, 8, 4, 'ID', 'ID', 100, 1, 1),
(1, 8, 9, 'ID', 'ID', 100, 1, 1),
(1, 8, 10, 'ID', 'ID', 100, 1, 1),
(1, 9, 1, 'ID', 'ID', 100, 1, 1),
(1, 9, 4, 'ID', 'ID', 100, 1, 1),
(1, 9, 9, 'ID', 'ID', 100, 1, 1),
(1, 9, 10, 'ID', 'ID', 100, 1, 1),
(2, 1, 14, 'Page', 'Page', 2100, 1, 1),
(2, 4, 4, 'Page', 'Page', 200, 0, 0),
(2, 4, 5, 'Page', 'Page', 200, 1, 1),
(2, 4, 6, 'Offer', 'Offer', 200, 1, 1),
(2, 4, 9, 'Name', 'Name', 200, 1, 1),
(2, 4, 10, 'Name', 'Name', 200, 1, 1),
(2, 4, 12, 'Name', 'Name', 200, 1, 1),
(2, 5, 4, 'Page', 'Page', 200, 0, 0),
(2, 5, 5, 'Page', 'Page', 200, 1, 1),
(2, 5, 6, 'Offer', 'Offer', 200, 1, 1),
(2, 5, 9, 'Name', 'Name', 200, 1, 1),
(2, 5, 10, 'Name', 'Name', 200, 1, 1),
(2, 6, 4, 'Page', 'Page', 200, 0, 0),
(2, 6, 5, 'Page', 'Page', 200, 1, 1),
(2, 6, 6, 'Offer', 'Offer', 200, 1, 1),
(2, 6, 9, 'Name', 'Name', 200, 1, 1),
(2, 6, 10, 'Name', 'Name', 200, 1, 1),
(2, 6, 11, 'Page', 'Page', 200, 1, 1),
(2, 6, 12, 'Name', 'Name', 200, 1, 1),
(2, 7, 4, 'Page', 'Page', 200, 0, 0),
(2, 7, 5, 'Page', 'Page', 200, 1, 1),
(2, 7, 6, 'Offer', 'Offer', 200, 1, 1),
(2, 7, 9, 'Name', 'Name', 200, 1, 1),
(2, 7, 10, 'Name', 'Name', 200, 1, 1),
(2, 8, 4, 'Page', 'Page', 200, 0, 0),
(2, 8, 5, 'Page', 'Page', 200, 1, 1),
(2, 8, 6, 'Offer', 'Offer', 200, 1, 1),
(2, 8, 9, 'Name', 'Name', 200, 1, 1),
(2, 8, 10, 'Name', 'Name', 200, 1, 1),
(2, 9, 5, 'Page', 'Page', 300, 1, 1),
(2, 9, 6, 'Offer', 'Offer', 300, 1, 1),
(2, 9, 9, 'Name', 'Name', 100, 1, 1),
(2, 9, 10, 'Name', 'Name', 200, 1, 1),
(3, 1, 14, 'URL', 'URL', 2200, 0, 0),
(3, 4, 4, 'URL', 'URL', 300, 0, 0),
(3, 4, 5, 'URL', 'URL', 300, 0, 0),
(3, 4, 6, 'URL', 'URL', 300, 0, 0),
(3, 5, 4, 'URL', 'URL', 300, 0, 0),
(3, 5, 5, 'URL', 'URL', 300, 0, 0),
(3, 5, 6, 'URL', 'URL', 300, 0, 0),
(3, 6, 4, 'URL', 'URL', 300, 0, 0),
(3, 6, 5, 'URL', 'URL', 300, 0, 0),
(3, 6, 6, 'URL', 'URL', 300, 0, 0),
(3, 6, 11, 'URL', 'URL', 300, 0, 0),
(3, 7, 4, 'URL', 'URL', 300, 0, 0),
(3, 7, 5, 'URL', 'URL', 300, 0, 0),
(3, 7, 6, 'URL', 'URL', 300, 0, 0),
(3, 8, 4, 'URL', 'URL', 300, 0, 0),
(3, 8, 5, 'URL', 'URL', 300, 0, 0),
(3, 8, 6, 'URL', 'URL', 300, 0, 0),
(3, 9, 5, 'URL', 'URL', 400, 0, 0),
(3, 9, 6, 'URL', 'URL', 400, 0, 0),
(4, 1, 14, 'Keyword/Target', 'Keyword/Target', 500, 1, 1),
(4, 4, 1, 'Target', 'Target', 400, 1, 1),
(4, 4, 4, 'Target', 'Target', 400, 1, 1),
(4, 5, 1, 'Target', 'Target', 400, 1, 1),
(4, 5, 4, 'Target', 'Target', 400, 1, 1),
(4, 6, 1, 'Target', 'Target', 400, 1, 1),
(4, 6, 4, 'Target', 'Target', 400, 1, 1),
(4, 7, 1, 'Target', 'Target', 400, 1, 1),
(4, 7, 4, 'Target', 'Target', 400, 1, 1),
(4, 8, 1, 'Target', 'Target', 400, 1, 1),
(4, 8, 4, 'Target', 'Target', 400, 1, 1),
(5, 4, 1, 'Views', 'Visitors', 500, 1, 1),
(5, 4, 4, 'Views', 'Visitors', 1900, 1, 1),
(5, 4, 5, 'Views', 'Visitors', 500, 1, 1),
(5, 4, 7, 'Views', 'Visitors', 500, 1, 1),
(5, 4, 8, 'Views', 'Visitors', 500, 1, 1),
(5, 4, 9, 'Views', 'Visitors', 500, 1, 1),
(5, 4, 10, 'Views', 'Visitors', 500, 1, 1),
(5, 4, 12, 'Visitors', 'Visitors', 500, 1, 1),
(5, 4, 13, 'Views', 'Visitors', 500, 1, 1),
(5, 4, 16, 'Views', 'Ad Clicks', 1900, 1, 1),
(5, 5, 1, 'Views', 'Visitors', 500, 1, 1),
(5, 5, 4, 'Views', 'Visitors', 1900, 1, 1),
(5, 5, 5, 'Views', 'Visitors', 500, 1, 1),
(5, 5, 7, 'Views', 'Visitors', 500, 1, 1),
(5, 5, 8, 'Views', 'Visitors', 500, 1, 1),
(5, 5, 9, 'Views', 'Visitors', 500, 1, 1),
(5, 5, 10, 'Views', 'Visitors', 500, 1, 1),
(5, 5, 13, 'Views', 'Visitors', 500, 1, 1),
(5, 5, 16, 'Views', 'Ad Clicks', 1900, 1, 1),
(5, 6, 1, 'Views', 'Visitors', 500, 1, 1),
(5, 6, 4, 'Views', 'Visitors', 1900, 1, 1),
(5, 6, 5, 'Views', 'Visitors', 500, 1, 1),
(5, 6, 7, 'Views', 'Visitors', 500, 1, 1),
(5, 6, 8, 'Views', 'Visitors', 500, 1, 1),
(5, 6, 9, 'Views', 'Visitors', 500, 1, 1),
(5, 6, 10, 'Views', 'Visitors', 500, 1, 1),
(5, 6, 11, 'Visitors', 'Visitors', 500, 1, 1),
(5, 6, 12, 'Visitors', 'Visitors', 500, 1, 1),
(5, 6, 13, 'Views', 'Visitors', 500, 1, 1),
(5, 6, 16, 'Views', 'Ad Clicks', 1900, 1, 1),
(5, 7, 1, 'Views', 'Visitors', 500, 1, 1),
(5, 7, 4, 'Views', 'Visitors', 1900, 1, 1),
(5, 7, 5, 'Views', 'Visitors', 500, 1, 1),
(5, 7, 7, 'Views', 'Visitors', 500, 1, 1),
(5, 7, 8, 'Views', 'Visitors', 500, 1, 1),
(5, 7, 9, 'Views', 'Visitors', 500, 1, 1),
(5, 7, 10, 'Views', 'Visitors', 500, 1, 1),
(5, 7, 13, 'Views', 'Visitors', 500, 1, 1),
(5, 7, 16, 'Views', 'Ad Clicks', 1900, 1, 1),
(5, 8, 1, 'Views', 'Visitors', 500, 1, 1),
(5, 8, 4, 'Views', 'Visitors', 1900, 1, 1),
(5, 8, 5, 'Views', 'Visitors', 500, 1, 1),
(5, 8, 7, 'Views', 'Visitors', 500, 1, 1),
(5, 8, 8, 'Views', 'Visitors', 500, 1, 1),
(5, 8, 9, 'Views', 'Visitors', 500, 1, 1),
(5, 8, 10, 'Views', 'Visitors', 500, 1, 1),
(5, 8, 13, 'Views', 'Visitors', 500, 1, 1),
(5, 8, 16, 'Views', 'Ad Clicks', 1900, 1, 1),
(5, 9, 1, '# Sent', '# Sent', 500, 1, 1),
(5, 9, 4, '# Sent', '# Sent', 800, 1, 1),
(5, 9, 5, 'Views', 'Views', 600, 1, 1),
(5, 9, 7, 'Views', 'Views', 500, 1, 1),
(5, 9, 8, 'Views', 'Views', 500, 1, 1),
(5, 9, 9, '# Sent', '# Sent', 400, 1, 1),
(5, 9, 10, '# Sent', '# Sent', 400, 1, 1),
(5, 9, 13, 'Views', 'Views', 500, 1, 1),
(6, 4, 1, 'Cost', 'Cost', 700, 1, 1),
(6, 4, 4, 'Cost', 'Cost', 2100, 1, 1),
(6, 4, 5, 'Cost', 'Cost', 700, 1, 1),
(6, 4, 6, 'Cost', 'Cost', 900, 1, 1),
(6, 4, 7, 'Cost', 'Cost', 700, 1, 1),
(6, 4, 8, 'Cost', 'Cost', 700, 1, 1),
(6, 4, 9, 'Cost', 'Cost', 700, 1, 1),
(6, 4, 10, 'Cost', 'Cost', 700, 1, 1),
(6, 4, 12, 'Cost', 'Cost', 700, 1, 1),
(6, 4, 13, 'Cost', 'Cost', 700, 1, 1),
(6, 4, 16, 'Cost', 'Cost', 2100, 1, 1),
(6, 5, 1, 'Cost', 'Cost', 700, 1, 1),
(6, 5, 4, 'Cost', 'Cost', 2100, 1, 1),
(6, 5, 5, 'Cost', 'Cost', 700, 1, 1),
(6, 5, 6, 'Cost', 'Cost', 900, 1, 1),
(6, 5, 7, 'Cost', 'Cost', 700, 1, 1),
(6, 5, 8, 'Cost', 'Cost', 700, 1, 1),
(6, 5, 9, 'Cost', 'Cost', 700, 1, 1),
(6, 5, 10, 'Cost', 'Cost', 700, 1, 1),
(6, 5, 13, 'Cost', 'Cost', 700, 1, 1),
(6, 5, 16, 'Cost', 'Cost', 2100, 1, 1),
(6, 6, 1, 'Cost', 'Cost', 700, 1, 1),
(6, 6, 4, 'Cost', 'Cost', 2100, 1, 1),
(6, 6, 5, 'Cost', 'Cost', 700, 1, 1),
(6, 6, 6, 'Cost', 'Cost', 900, 1, 1),
(6, 6, 7, 'Cost', 'Cost', 700, 1, 1),
(6, 6, 8, 'Cost', 'Cost', 700, 1, 1),
(6, 6, 9, 'Cost', 'Cost', 700, 1, 1),
(6, 6, 10, 'Cost', 'Cost', 700, 1, 1),
(6, 6, 12, 'Cost', 'Cost', 700, 1, 1),
(6, 6, 13, 'Cost', 'Cost', 700, 1, 1),
(6, 6, 16, 'Cost', 'Cost', 2100, 1, 1),
(6, 7, 1, 'Cost', 'Cost', 700, 1, 1),
(6, 7, 4, 'Cost', 'Cost', 2100, 1, 1),
(6, 7, 5, 'Cost', 'Cost', 700, 1, 1),
(6, 7, 6, 'Cost', 'Cost', 900, 1, 1),
(6, 7, 7, 'Cost', 'Cost', 700, 1, 1),
(6, 7, 8, 'Cost', 'Cost', 700, 1, 1),
(6, 7, 9, 'Cost', 'Cost', 700, 1, 1),
(6, 7, 10, 'Cost', 'Cost', 700, 1, 1),
(6, 7, 13, 'Cost', 'Cost', 700, 1, 1),
(6, 7, 16, 'Cost', 'Cost', 2100, 1, 1),
(6, 8, 1, 'Cost', 'Cost', 700, 1, 1),
(6, 8, 4, 'Cost', 'Cost', 2100, 1, 1),
(6, 8, 5, 'Cost', 'Cost', 700, 1, 1),
(6, 8, 6, 'Cost', 'Cost', 900, 1, 1),
(6, 8, 7, 'Cost', 'Cost', 700, 1, 1),
(6, 8, 8, 'Cost', 'Cost', 700, 1, 1),
(6, 8, 9, 'Cost', 'Cost', 700, 1, 1),
(6, 8, 10, 'Cost', 'Cost', 700, 1, 1),
(6, 8, 13, 'Cost', 'Cost', 700, 1, 1),
(6, 8, 16, 'Cost', 'Cost', 2100, 1, 1),
(7, 4, 1, 'CPV', 'CPC', 800, 1, 1),
(7, 4, 4, 'CPV', 'CPC', 2200, 1, 1),
(7, 4, 5, 'CPV', 'CPC', 800, 1, 1),
(7, 4, 7, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 4, 8, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 4, 13, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 4, 16, 'CPV', 'CPC', 2200, 1, 1),
(7, 5, 1, 'CPV', 'CPC', 800, 1, 1),
(7, 5, 4, 'CPV', 'CPC', 2200, 1, 1),
(7, 5, 5, 'CPV', 'CPC', 800, 1, 1),
(7, 5, 7, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 5, 8, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 5, 13, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 5, 16, 'CPV', 'CPC', 2200, 1, 1),
(7, 6, 1, 'CPV', 'CPC', 800, 1, 1),
(7, 6, 4, 'CPV', 'CPC', 2200, 1, 1),
(7, 6, 5, 'CPV', 'CPC', 800, 1, 1),
(7, 6, 7, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 6, 8, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 6, 13, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 6, 16, 'CPV', 'CPC', 2200, 1, 1),
(7, 7, 1, 'CPV', 'CPC', 800, 1, 1),
(7, 7, 4, 'CPV', 'CPC', 2200, 1, 1),
(7, 7, 5, 'CPV', 'CPC', 800, 1, 1),
(7, 7, 7, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 7, 8, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 7, 13, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 7, 16, 'CPV', 'CPC', 2200, 1, 1),
(7, 8, 1, 'CPV', 'CPC', 800, 1, 1),
(7, 8, 4, 'CPV', 'CPC', 2200, 1, 1),
(7, 8, 5, 'CPV', 'CPC', 800, 1, 1),
(7, 8, 7, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 8, 8, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 8, 13, 'CPV', 'Ad CPC', 800, 1, 1),
(7, 8, 16, 'CPV', 'CPC', 2200, 1, 1),
(8, 4, 1, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 4, 5, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 4, 6, 'Visitors', 'Visitors', 700, 1, 1),
(8, 4, 12, 'Clicks', 'Clicks', 800, 1, 1),
(8, 5, 1, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 5, 4, 'Clicks', 'LP Clicks', 2300, 1, 1),
(8, 5, 5, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 5, 6, 'Visitors', 'Visitors', 700, 1, 1),
(8, 5, 7, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 5, 8, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 5, 13, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 5, 16, 'Clicks', 'LP Clicks', 2300, 1, 1),
(8, 6, 1, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 6, 4, 'Clicks', 'LP Clicks', 2300, 1, 1),
(8, 6, 5, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 6, 6, 'Visitors', 'Visitors', 700, 1, 1),
(8, 6, 7, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 6, 8, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 6, 12, 'Clicks', 'Clicks', 800, 1, 1),
(8, 6, 13, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 6, 16, 'Clicks', 'LP Clicks', 2300, 1, 1),
(8, 7, 1, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 7, 4, 'Clicks', 'LP Clicks', 2300, 1, 1),
(8, 7, 5, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 7, 6, 'Visitors', 'Visitors', 700, 1, 1),
(8, 7, 7, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 7, 8, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 7, 13, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 7, 16, 'Clicks', 'LP Clicks', 2300, 1, 1),
(8, 8, 1, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 8, 4, 'Clicks', 'LP Clicks', 2300, 1, 1),
(8, 8, 5, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 8, 6, 'Visitors', 'Visitors', 700, 1, 1),
(8, 8, 7, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 8, 8, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 8, 13, 'Clicks', 'LP Clicks', 900, 1, 1),
(8, 8, 16, 'Clicks', 'LP Clicks', 2300, 1, 1),
(8, 9, 1, 'Clicks', 'Clicks', 900, 1, 1),
(8, 9, 4, 'Clicks', 'Clicks', 1200, 1, 1),
(8, 9, 5, 'Clicks', 'Clicks', 1000, 1, 1),
(8, 9, 6, 'Visitors', 'Visitors', 800, 1, 1),
(8, 9, 7, 'Clicks', 'Clicks', 900, 1, 1),
(8, 9, 8, 'Clicks', 'Clicks', 900, 1, 1),
(8, 9, 9, 'Clicks', 'Clicks', 900, 1, 1),
(8, 9, 10, 'Clicks', 'Clicks', 900, 1, 1),
(8, 9, 13, 'Clicks', 'Clicks', 900, 1, 1),
(9, 4, 1, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 4, 5, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 4, 12, 'CTR', 'CTR', 900, 1, 1),
(9, 5, 1, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 5, 4, 'CTR', 'LP CTR', 2400, 1, 1),
(9, 5, 5, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 5, 7, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 5, 8, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 5, 13, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 5, 16, 'CTR', 'LP CTR', 2400, 1, 1),
(9, 6, 1, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 6, 4, 'CTR', 'LP CTR', 2400, 1, 1),
(9, 6, 5, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 6, 7, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 6, 8, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 6, 12, 'CTR', 'CTR', 900, 1, 1),
(9, 6, 13, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 6, 16, 'CTR', 'LP CTR', 2400, 1, 1),
(9, 7, 1, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 7, 4, 'CTR', 'LP CTR', 2400, 1, 1),
(9, 7, 5, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 7, 7, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 7, 8, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 7, 13, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 7, 16, 'CTR', 'LP CTR', 2400, 1, 1),
(9, 8, 1, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 8, 4, 'CTR', 'LP CTR', 2400, 1, 1),
(9, 8, 5, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 8, 7, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 8, 8, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 8, 13, 'CTR', 'LP CTR', 1000, 1, 1),
(9, 8, 16, 'CTR', 'LP CTR', 2400, 1, 1),
(9, 9, 1, 'CTR', 'CTR', 1000, 1, 1),
(9, 9, 4, 'CTR', 'CTR', 1300, 1, 1),
(9, 9, 5, 'CTR', 'CTR', 1100, 1, 1),
(9, 9, 7, 'CTR', 'CTR', 1000, 1, 1),
(9, 9, 8, 'CTR', 'CTR', 1000, 1, 1),
(9, 9, 9, 'CTR', 'CTR', 1000, 1, 1),
(9, 9, 10, 'CTR', 'CTR', 1000, 1, 1),
(9, 9, 13, 'CTR', 'CTR', 1000, 1, 1),
(10, 4, 1, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 4, 5, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 4, 6, 'CPC', 'CPC', 1100, 1, 1),
(10, 5, 1, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 5, 4, 'CPC', 'LP CPC', 2500, 1, 1),
(10, 5, 5, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 5, 6, 'CPC', 'CPC', 1100, 1, 1),
(10, 5, 7, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 5, 8, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 5, 13, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 5, 16, 'CPC', 'LP CPC', 2500, 1, 1),
(10, 6, 1, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 6, 4, 'CPC', 'LP CPC', 2500, 1, 1),
(10, 6, 5, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 6, 6, 'CPC', 'CPC', 1100, 1, 1),
(10, 6, 7, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 6, 8, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 6, 13, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 6, 16, 'CPC', 'LP CPC', 2500, 1, 1),
(10, 7, 1, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 7, 4, 'CPC', 'LP CPC', 2500, 1, 1),
(10, 7, 5, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 7, 6, 'CPC', 'CPC', 1100, 1, 1),
(10, 7, 7, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 7, 8, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 7, 13, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 7, 16, 'CPC', 'LP CPC', 2500, 1, 1),
(10, 8, 1, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 8, 4, 'CPC', 'LP CPC', 2500, 1, 1),
(10, 8, 5, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 8, 6, 'CPC', 'CPC', 1100, 1, 1),
(10, 8, 7, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 8, 8, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 8, 13, 'CPC', 'LP CPC', 1100, 1, 1),
(10, 8, 16, 'CPC', 'LP CPC', 2500, 1, 1),
(11, 4, 1, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 4, 4, 'Conversions', 'Conversions', 2600, 1, 1),
(11, 4, 5, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 4, 6, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 4, 7, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 4, 8, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 4, 12, 'Conversions', 'Conversions', 1100, 1, 1),
(11, 4, 13, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 4, 16, 'Conversions', 'Conversions', 2600, 1, 1),
(11, 5, 1, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 5, 4, 'Conversions', 'Conversions', 2600, 1, 1),
(11, 5, 5, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 5, 6, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 5, 7, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 5, 8, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 5, 13, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 5, 16, 'Conversions', 'Conversions', 2600, 1, 1),
(11, 6, 1, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 6, 4, 'Conversions', 'Conversions', 2900, 1, 1),
(11, 6, 5, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 6, 6, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 6, 7, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 6, 8, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 6, 12, 'Conversions', 'Conversions', 1100, 1, 1),
(11, 6, 13, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 6, 16, 'Conversions', 'Conversions', 2900, 1, 1),
(11, 7, 1, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 7, 4, 'Conversions', 'Conversions', 2600, 1, 1),
(11, 7, 5, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 7, 6, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 7, 7, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 7, 8, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 7, 13, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 7, 16, 'Conversions', 'Conversions', 2600, 1, 1),
(11, 8, 1, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 8, 4, 'Conversions', 'Conversions', 2600, 1, 1),
(11, 8, 5, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 8, 6, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 8, 7, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 8, 8, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 8, 13, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 8, 16, 'Conversions', 'Conversions', 2600, 1, 1),
(11, 9, 1, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 9, 4, 'Conversions', 'Conversions', 1500, 1, 1),
(11, 9, 5, 'Conversions', 'Conversions', 1300, 1, 1),
(11, 9, 6, 'Conversions', 'Conversions', 1300, 1, 1),
(11, 9, 7, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 9, 8, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 9, 9, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 9, 10, 'Conversions', 'Conversions', 1200, 1, 1),
(11, 9, 13, 'Conversions', 'Conversions', 1200, 1, 1),
(12, 4, 1, 'CR', 'CR', 1300, 1, 1),
(12, 4, 4, 'CR', 'CR', 2700, 1, 1),
(12, 4, 5, 'CR', 'CR', 1300, 1, 1),
(12, 4, 6, 'CR', 'CR', 1300, 1, 1),
(12, 4, 7, 'CR', 'CR', 1300, 1, 1),
(12, 4, 8, 'CR', 'CR', 1300, 1, 1),
(12, 4, 12, 'CR', 'CR', 1200, 1, 1),
(12, 4, 13, 'CR', 'CR', 1300, 1, 1),
(12, 4, 16, 'CR', 'CR', 2700, 1, 1),
(12, 5, 1, 'CR', 'CR', 1300, 1, 1),
(12, 5, 4, 'CR', 'CR', 2700, 1, 1),
(12, 5, 5, 'CR', 'CR', 1300, 1, 1),
(12, 5, 6, 'CR', 'CR', 1300, 1, 1),
(12, 5, 7, 'CR', 'CR', 1300, 1, 1),
(12, 5, 8, 'CR', 'CR', 1300, 1, 1),
(12, 5, 13, 'CR', 'CR', 1300, 1, 1),
(12, 5, 16, 'CR', 'CR', 2700, 1, 1),
(12, 6, 1, 'CR', 'CR', 1300, 1, 1),
(12, 6, 4, 'CR', 'CR', 3000, 1, 1),
(12, 6, 5, 'CR', 'CR', 1300, 1, 1),
(12, 6, 6, 'CR', 'CR', 1300, 1, 1),
(12, 6, 7, 'CR', 'CR', 1300, 1, 1),
(12, 6, 8, 'CR', 'CR', 1300, 1, 1),
(12, 6, 12, 'CR', 'CR', 1200, 1, 1),
(12, 6, 13, 'CR', 'CR', 1300, 1, 1),
(12, 6, 16, 'CR', 'CR', 3000, 1, 1),
(12, 7, 1, 'CR', 'CR', 1300, 1, 1),
(12, 7, 4, 'CR', 'CR', 2700, 1, 1),
(12, 7, 5, 'CR', 'CR', 1300, 1, 1),
(12, 7, 6, 'CR', 'CR', 1300, 1, 1),
(12, 7, 7, 'CR', 'CR', 1300, 1, 1),
(12, 7, 8, 'CR', 'CR', 1300, 1, 1),
(12, 7, 13, 'CR', 'CR', 1300, 1, 1),
(12, 7, 16, 'CR', 'CR', 2700, 1, 1),
(12, 8, 1, 'CR', 'CR', 1300, 1, 1),
(12, 8, 4, 'CR', 'CR', 2700, 1, 1),
(12, 8, 5, 'CR', 'CR', 1300, 1, 1),
(12, 8, 6, 'CR', 'CR', 1300, 1, 1),
(12, 8, 7, 'CR', 'CR', 1300, 1, 1),
(12, 8, 8, 'CR', 'CR', 1300, 1, 1),
(12, 8, 13, 'CR', 'CR', 1300, 1, 1),
(12, 8, 16, 'CR', 'CR', 2700, 1, 1),
(12, 9, 1, 'CR', 'CR', 1300, 1, 1),
(12, 9, 4, 'CR', 'CR', 1600, 1, 1),
(12, 9, 5, 'CR', 'CR', 1400, 1, 1),
(12, 9, 6, 'CR', 'CR', 1400, 1, 1),
(12, 9, 7, 'CR', 'CR', 1300, 1, 1),
(12, 9, 8, 'CR', 'CR', 1300, 1, 1),
(12, 9, 9, 'CR', 'CR', 1300, 1, 1),
(12, 9, 10, 'CR', 'CR', 1300, 1, 1),
(12, 9, 13, 'CR', 'CR', 1300, 1, 1),
(13, 4, 1, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 4, 4, 'Revenue', 'Revenue', 3100, 1, 1),
(13, 4, 5, 'Revenue', 'Revenue', 1700, 1, 1),
(13, 4, 6, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 4, 7, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 4, 8, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 4, 9, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 4, 10, 'Revenue', 'Revenue', 800, 1, 1),
(13, 4, 12, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 4, 13, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 4, 16, 'Revenue', 'Revenue', 3100, 1, 1),
(13, 5, 1, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 5, 4, 'Revenue', 'Revenue', 3100, 1, 1),
(13, 5, 5, 'Revenue', 'Revenue', 1700, 1, 1),
(13, 5, 6, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 5, 7, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 5, 8, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 5, 9, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 5, 10, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 5, 13, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 5, 16, 'Revenue', 'Revenue', 3100, 1, 1),
(13, 6, 1, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 6, 4, 'Revenue', 'Revenue', 3400, 1, 1),
(13, 6, 5, 'Revenue', 'Revenue', 1700, 1, 1),
(13, 6, 6, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 6, 7, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 6, 8, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 6, 9, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 6, 10, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 6, 12, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 6, 13, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 6, 16, 'Revenue', 'Revenue', 3400, 1, 1),
(13, 7, 1, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 7, 4, 'Revenue', 'Revenue', 3100, 1, 1),
(13, 7, 5, 'Revenue', 'Revenue', 1700, 1, 1),
(13, 7, 6, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 7, 7, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 7, 8, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 7, 9, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 7, 10, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 7, 13, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 7, 16, 'Revenue', 'Revenue', 3100, 1, 1),
(13, 8, 1, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 8, 4, 'Revenue', 'Revenue', 3100, 1, 1),
(13, 8, 5, 'Revenue', 'Revenue', 1700, 1, 1),
(13, 8, 6, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 8, 7, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 8, 8, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 8, 9, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 8, 10, 'Revenue', 'Revenue', 1400, 1, 1),
(13, 8, 13, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 8, 16, 'Revenue', 'Revenue', 3100, 1, 1),
(13, 9, 1, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 9, 4, 'Revenue', 'Revenue', 2000, 1, 1),
(13, 9, 5, 'Revenue', 'Revenue', 1800, 1, 1),
(13, 9, 6, 'Revenue', 'Revenue', 1600, 1, 1),
(13, 9, 7, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 9, 8, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 9, 9, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 9, 10, 'Revenue', 'Revenue', 1500, 1, 1),
(13, 9, 13, 'Revenue', 'Revenue', 1500, 1, 1),
(14, 4, 1, 'EPV', 'EPC', 1600, 1, 1),
(14, 4, 4, 'EPV', 'EPC', 3200, 1, 1),
(14, 4, 5, 'EPV', 'EPC', 1800, 1, 1),
(14, 4, 7, 'EPV', 'EPC', 1600, 1, 1),
(14, 4, 8, 'EPV', 'EPC', 1600, 1, 1),
(14, 4, 9, 'EPV', 'EPC', 1500, 1, 1),
(14, 4, 13, 'EPV', 'EPC', 1600, 1, 1),
(14, 4, 16, 'EPV', 'EPC', 3200, 1, 1),
(14, 5, 1, 'EPV', 'EPC', 1600, 1, 1),
(14, 5, 4, 'EPV', 'EPC', 3200, 1, 1),
(14, 5, 5, 'EPV', 'EPC', 1800, 1, 1),
(14, 5, 7, 'EPV', 'EPC', 1600, 1, 1),
(14, 5, 8, 'EPV', 'EPC', 1600, 1, 1),
(14, 5, 9, 'EPV', 'EPC', 1500, 1, 1),
(14, 5, 10, 'EPV', 'EPC', 1500, 1, 1),
(14, 5, 13, 'EPV', 'EPC', 1600, 1, 1),
(14, 5, 16, 'EPV', 'EPC', 3200, 1, 1),
(14, 6, 1, 'EPV', 'EPC', 1600, 1, 1),
(14, 6, 4, 'EPV', 'EPC', 3500, 1, 1),
(14, 6, 5, 'EPV', 'EPC', 1800, 1, 1),
(14, 6, 7, 'EPV', 'EPC', 1600, 1, 1),
(14, 6, 8, 'EPV', 'EPC', 1600, 1, 1),
(14, 6, 9, 'EPV', 'EPC', 1500, 1, 1),
(14, 6, 10, 'EPV', 'EPC', 1500, 1, 1),
(14, 6, 13, 'EPV', 'EPC', 1600, 1, 1),
(14, 6, 16, 'EPV', 'EPC', 3500, 1, 1),
(14, 7, 1, 'EPV', 'EPC', 1600, 1, 1),
(14, 7, 4, 'EPV', 'EPC', 3200, 1, 1),
(14, 7, 5, 'EPV', 'EPC', 1800, 1, 1),
(14, 7, 7, 'EPV', 'EPC', 1600, 1, 1),
(14, 7, 8, 'EPV', 'EPC', 1600, 1, 1),
(14, 7, 9, 'EPV', 'EPC', 1500, 1, 1),
(14, 7, 10, 'EPV', 'EPC', 1500, 1, 1),
(14, 7, 13, 'EPV', 'EPC', 1600, 1, 1),
(14, 7, 16, 'EPV', 'EPC', 3200, 1, 1),
(14, 8, 1, 'EPV', 'EPC', 1600, 1, 1),
(14, 8, 4, 'EPV', 'EPC', 3200, 1, 1),
(14, 8, 5, 'EPV', 'EPC', 1800, 1, 1),
(14, 8, 7, 'EPV', 'EPC', 1600, 1, 1),
(14, 8, 8, 'EPV', 'EPC', 1600, 1, 1),
(14, 8, 9, 'EPV', 'EPC', 1500, 1, 1),
(14, 8, 10, 'EPV', 'EPC', 1500, 1, 1),
(14, 8, 13, 'EPV', 'EPC', 1600, 1, 1),
(14, 8, 16, 'EPV', 'EPC', 3200, 1, 1),
(15, 4, 1, 'PPV', 'PPC', 1700, 1, 1),
(15, 4, 4, 'PPV', 'PPC', 3300, 1, 1),
(15, 4, 5, 'PPV', 'PPC', 1900, 1, 1),
(15, 4, 7, 'PPV', 'PPC', 1700, 1, 1),
(15, 4, 8, 'PPV', 'PPC', 1700, 1, 1),
(15, 4, 9, 'PPV', 'PPV', 1600, 1, 1),
(15, 4, 13, 'PPV', 'PPC', 1700, 1, 1),
(15, 4, 16, 'PPV', 'PPC', 3300, 1, 1),
(15, 5, 1, 'PPV', 'PPC', 1700, 1, 1),
(15, 5, 4, 'PPV', 'PPC', 3300, 1, 1),
(15, 5, 5, 'PPV', 'PPC', 1900, 1, 1),
(15, 5, 7, 'PPV', 'PPC', 1700, 1, 1),
(15, 5, 8, 'PPV', 'PPC', 1700, 1, 1),
(15, 5, 9, 'PPV', 'PPC', 1600, 1, 1),
(15, 5, 10, 'PPV', 'PPC', 1600, 1, 1),
(15, 5, 13, 'PPV', 'PPC', 1700, 1, 1),
(15, 5, 16, 'PPV', 'PPC', 3300, 1, 1),
(15, 6, 1, 'PPV', 'PPC', 1700, 1, 1),
(15, 6, 4, 'PPV', 'PPC', 3600, 1, 1),
(15, 6, 5, 'PPV', 'PPC', 1900, 1, 1),
(15, 6, 7, 'PPV', 'PPC', 1700, 1, 1),
(15, 6, 8, 'PPV', 'PPC', 1700, 1, 1),
(15, 6, 9, 'PPV', 'PPC', 1600, 1, 1),
(15, 6, 10, 'PPV', 'PPC', 1600, 1, 1),
(15, 6, 13, 'PPV', 'PPC', 1700, 1, 1),
(15, 6, 16, 'PPV', 'PPC', 3600, 1, 1),
(15, 7, 1, 'PPV', 'PPC', 1700, 1, 1),
(15, 7, 4, 'PPV', 'PPC', 3300, 1, 1),
(15, 7, 5, 'PPV', 'PPC', 1900, 1, 1),
(15, 7, 7, 'PPV', 'PPC', 1700, 1, 1),
(15, 7, 8, 'PPV', 'PPC', 1700, 1, 1),
(15, 7, 9, 'PPV', 'PPC', 1600, 1, 1),
(15, 7, 10, 'PPV', 'PPC', 1600, 1, 1),
(15, 7, 13, 'PPV', 'PPC', 1700, 1, 1),
(15, 7, 16, 'PPV', 'PPC', 3300, 1, 1),
(15, 8, 1, 'PPV', 'PPC', 1700, 1, 1),
(15, 8, 4, 'PPV', 'PPC', 3300, 1, 1),
(15, 8, 5, 'PPV', 'PPC', 1900, 1, 1),
(15, 8, 7, 'PPV', 'PPC', 1700, 1, 1),
(15, 8, 8, 'PPV', 'PPC', 1700, 1, 1),
(15, 8, 9, 'PPV', 'PPC', 1600, 1, 1),
(15, 8, 10, 'PPV', 'PPC', 1600, 1, 1),
(15, 8, 13, 'PPV', 'PPC', 1700, 1, 1),
(15, 8, 16, 'PPV', 'PPC', 3300, 1, 1),
(16, 4, 1, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 4, 4, 'eCPM', 'eCPM', 3500, 1, 1),
(16, 4, 5, 'eCPM', 'eCPM', 2100, 1, 1),
(16, 4, 7, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 4, 8, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 4, 13, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 4, 16, 'eCPM', 'eCPM', 3500, 1, 1),
(16, 5, 1, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 5, 4, 'eCPM', 'eCPM', 3500, 1, 1),
(16, 5, 5, 'eCPM', 'eCPM', 2100, 1, 1),
(16, 5, 7, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 5, 8, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 5, 13, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 5, 16, 'eCPM', 'eCPM', 3500, 1, 1),
(16, 6, 1, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 6, 4, 'eCPM', 'eCPM', 3800, 1, 1),
(16, 6, 5, 'eCPM', 'eCPM', 2100, 1, 1),
(16, 6, 7, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 6, 8, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 6, 13, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 6, 16, 'eCPM', 'eCPM', 3800, 1, 1),
(16, 7, 1, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 7, 4, 'eCPM', 'eCPM', 3500, 1, 1),
(16, 7, 5, 'eCPM', 'eCPM', 2100, 1, 1),
(16, 7, 7, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 7, 8, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 7, 13, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 7, 16, 'eCPM', 'eCPM', 3500, 1, 1),
(16, 8, 1, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 8, 4, 'eCPM', 'eCPM', 3500, 1, 1),
(16, 8, 5, 'eCPM', 'eCPM', 2100, 1, 1),
(16, 8, 7, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 8, 8, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 8, 13, 'eCPM', 'eCPM', 1900, 1, 1),
(16, 8, 16, 'eCPM', 'eCPM', 3500, 1, 1),
(17, 4, 1, 'P/L', 'P/L', 1800, 1, 1),
(17, 4, 4, 'P/L', 'P/L', 3400, 1, 1),
(17, 4, 5, 'P/L', 'P/L', 2000, 1, 1),
(17, 4, 6, 'P/L', 'P/L', 1800, 1, 1),
(17, 4, 7, 'P/L', 'P/L', 1800, 1, 1),
(17, 4, 8, 'P/L', 'P/L', 1800, 1, 1),
(17, 4, 9, 'P/L', 'P/L', 1700, 1, 1),
(17, 4, 10, 'P/L', 'P/L', 900, 1, 1),
(17, 4, 13, 'P/L', 'P/L', 1800, 1, 1),
(17, 4, 16, 'P/L', 'P/L', 3400, 1, 1),
(17, 5, 1, 'P/L', 'P/L', 1800, 1, 1),
(17, 5, 4, 'P/L', 'P/L', 3400, 1, 1),
(17, 5, 5, 'P/L', 'P/L', 2000, 1, 1),
(17, 5, 6, 'P/L', 'P/L', 1800, 1, 1),
(17, 5, 7, 'P/L', 'P/L', 1800, 1, 1),
(17, 5, 8, 'P/L', 'P/L', 1800, 1, 1),
(17, 5, 9, 'P/L', 'P/L', 1700, 1, 1),
(17, 5, 10, 'P/L', 'P/L', 1700, 1, 1),
(17, 5, 13, 'P/L', 'P/L', 1800, 1, 1),
(17, 5, 16, 'P/L', 'P/L', 3400, 1, 1),
(17, 6, 1, 'P/L', 'P/L', 1800, 1, 1),
(17, 6, 4, 'P/L', 'P/L', 3700, 1, 1),
(17, 6, 5, 'P/L', 'P/L', 2000, 1, 1),
(17, 6, 6, 'P/L', 'P/L', 1800, 1, 1),
(17, 6, 7, 'P/L', 'P/L', 1800, 1, 1),
(17, 6, 8, 'P/L', 'P/L', 1800, 1, 1),
(17, 6, 9, 'P/L', 'P/L', 1700, 1, 1),
(17, 6, 10, 'P/L', 'P/L', 1700, 1, 1),
(17, 6, 13, 'P/L', 'P/L', 1800, 1, 1),
(17, 6, 16, 'P/L', 'P/L', 3700, 1, 1),
(17, 7, 1, 'P/L', 'P/L', 1800, 1, 1),
(17, 7, 4, 'P/L', 'P/L', 3400, 1, 1),
(17, 7, 5, 'P/L', 'P/L', 2000, 1, 1),
(17, 7, 6, 'P/L', 'P/L', 1800, 1, 1),
(17, 7, 7, 'P/L', 'P/L', 1800, 1, 1),
(17, 7, 8, 'P/L', 'P/L', 1800, 1, 1),
(17, 7, 9, 'P/L', 'P/L', 1700, 1, 1),
(17, 7, 10, 'P/L', 'P/L', 1700, 1, 1),
(17, 7, 13, 'P/L', 'P/L', 1800, 1, 1),
(17, 7, 16, 'P/L', 'P/L', 3400, 1, 1),
(17, 8, 1, 'P/L', 'P/L', 1800, 1, 1),
(17, 8, 4, 'P/L', 'P/L', 3400, 1, 1),
(17, 8, 5, 'P/L', 'P/L', 2000, 1, 1),
(17, 8, 6, 'P/L', 'P/L', 1800, 1, 1),
(17, 8, 7, 'P/L', 'P/L', 1800, 1, 1),
(17, 8, 8, 'P/L', 'P/L', 1800, 1, 1),
(17, 8, 9, 'P/L', 'P/L', 1700, 1, 1),
(17, 8, 10, 'P/L', 'P/L', 1700, 1, 1),
(17, 8, 13, 'P/L', 'P/L', 1800, 1, 1),
(17, 8, 16, 'P/L', 'P/L', 3400, 1, 1),
(18, 4, 1, 'ROI', 'ROI', 2000, 1, 1),
(18, 4, 4, 'ROI', 'ROI', 3600, 1, 1),
(18, 4, 5, 'ROI', 'ROI', 2200, 1, 1),
(18, 4, 6, 'ROI', 'ROI', 2000, 1, 1),
(18, 4, 7, 'ROI', 'ROI', 2000, 1, 1),
(18, 4, 8, 'ROI', 'ROI', 2000, 1, 1),
(18, 4, 9, 'ROI', 'ROI', 1900, 1, 1),
(18, 4, 10, 'ROI', 'ROI', 1000, 1, 1),
(18, 4, 13, 'ROI', 'ROI', 2000, 1, 1),
(18, 4, 16, 'ROI', 'ROI', 3600, 1, 1),
(18, 5, 1, 'ROI', 'ROI', 2000, 1, 1),
(18, 5, 4, 'ROI', 'ROI', 3600, 1, 1),
(18, 5, 5, 'ROI', 'ROI', 2200, 1, 1),
(18, 5, 6, 'ROI', 'ROI', 2000, 1, 1),
(18, 5, 7, 'ROI', 'ROI', 2000, 1, 1),
(18, 5, 8, 'ROI', 'ROI', 2000, 1, 1),
(18, 5, 9, 'ROI', 'ROI', 1900, 1, 1),
(18, 5, 10, 'ROI', 'ROI', 1900, 1, 1),
(18, 5, 13, 'ROI', 'ROI', 2000, 1, 1),
(18, 5, 16, 'ROI', 'ROI', 3600, 1, 1),
(18, 6, 1, 'ROI', 'ROI', 2000, 1, 1),
(18, 6, 4, 'ROI', 'ROI', 3900, 1, 1),
(18, 6, 5, 'ROI', 'ROI', 2200, 1, 1),
(18, 6, 6, 'ROI', 'ROI', 2000, 1, 1),
(18, 6, 7, 'ROI', 'ROI', 2000, 1, 1),
(18, 6, 8, 'ROI', 'ROI', 2000, 1, 1),
(18, 6, 9, 'ROI', 'ROI', 1900, 1, 1),
(18, 6, 10, 'ROI', 'ROI', 1900, 1, 1),
(18, 6, 13, 'ROI', 'ROI', 2000, 1, 1),
(18, 6, 16, 'ROI', 'ROI', 3900, 1, 1),
(18, 7, 1, 'ROI', 'ROI', 2000, 1, 1),
(18, 7, 4, 'ROI', 'ROI', 3600, 1, 1),
(18, 7, 5, 'ROI', 'ROI', 2200, 1, 1),
(18, 7, 6, 'ROI', 'ROI', 2000, 1, 1),
(18, 7, 7, 'ROI', 'ROI', 2000, 1, 1),
(18, 7, 8, 'ROI', 'ROI', 2000, 1, 1),
(18, 7, 9, 'ROI', 'ROI', 1900, 1, 1),
(18, 7, 10, 'ROI', 'ROI', 1900, 1, 1),
(18, 7, 13, 'ROI', 'ROI', 2000, 1, 1),
(18, 7, 16, 'ROI', 'ROI', 3600, 1, 1),
(18, 8, 1, 'ROI', 'ROI', 2000, 1, 1),
(18, 8, 4, 'ROI', 'ROI', 3600, 1, 1),
(18, 8, 5, 'ROI', 'ROI', 2200, 1, 1),
(18, 8, 6, 'ROI', 'ROI', 2000, 1, 1),
(18, 8, 7, 'ROI', 'ROI', 2000, 1, 1),
(18, 8, 8, 'ROI', 'ROI', 2000, 1, 1),
(18, 8, 9, 'ROI', 'ROI', 1900, 1, 1),
(18, 8, 10, 'ROI', 'ROI', 1900, 1, 1),
(18, 8, 13, 'ROI', 'ROI', 2000, 1, 1),
(18, 8, 16, 'ROI', 'ROI', 3600, 1, 1),
(19, 4, 4, 'Engage%', 'Engage%', 2000, 1, 1),
(19, 4, 5, 'Engage%', 'Engage%', 600, 1, 1),
(19, 4, 7, 'Engage%', 'Engage%', 600, 1, 1),
(19, 4, 8, 'Engage%', 'Engage%', 600, 1, 1),
(19, 4, 13, 'Engage%', 'Engage%', 600, 1, 1),
(19, 4, 16, 'Engage%', 'Engage%', 2000, 1, 1),
(19, 5, 4, 'Engage%', 'Engage%', 2000, 1, 1),
(19, 5, 5, 'Engage%', 'Engage%', 600, 1, 1),
(19, 5, 7, 'Engage%', 'Engage%', 600, 1, 1),
(19, 5, 8, 'Engage%', 'Engage%', 600, 1, 1),
(19, 5, 13, 'Engage%', 'Engage%', 600, 1, 1),
(19, 5, 16, 'Engage%', 'Engage%', 2000, 1, 1),
(19, 6, 4, 'Engage%', 'Engage%', 2000, 1, 1),
(19, 6, 5, 'Engage%', 'Engage%', 600, 1, 1),
(19, 6, 7, 'Engage%', 'Engage%', 600, 1, 1),
(19, 6, 8, 'Engage%', 'Engage%', 600, 1, 1),
(19, 6, 13, 'Engage%', 'Engage%', 600, 1, 1),
(19, 6, 16, 'Engage%', 'Engage%', 2000, 1, 1),
(19, 7, 4, 'Engage%', 'Engage%', 2000, 1, 1),
(19, 7, 5, 'Engage%', 'Engage%', 600, 1, 1),
(19, 7, 7, 'Engage%', 'Engage%', 600, 1, 1),
(19, 7, 8, 'Engage%', 'Engage%', 600, 1, 1),
(19, 7, 13, 'Engage%', 'Engage%', 600, 1, 1),
(19, 7, 16, 'Engage%', 'Engage%', 2000, 1, 1),
(19, 8, 4, 'Engage%', 'Engage%', 2000, 1, 1),
(19, 8, 5, 'Engage%', 'Engage%', 600, 1, 1),
(19, 8, 7, 'Engage%', 'Engage%', 600, 1, 1),
(19, 8, 8, 'Engage%', 'Engage%', 600, 1, 1),
(19, 8, 13, 'Engage%', 'Engage%', 600, 1, 1),
(19, 8, 16, 'Engage%', 'Engage%', 2000, 1, 1),
(19, 9, 5, 'Engage%', 'Engage%', 700, 1, 1),
(19, 9, 7, 'Engage%', 'Engage%', 600, 1, 1),
(19, 9, 8, 'Engage%', 'Engage%', 600, 1, 1),
(19, 9, 13, 'Engage%', 'Engage%', 600, 1, 1),
(20, 4, 7, ' ', ' ', 100, 1, 1),
(20, 4, 8, ' ', ' ', 100, 1, 1),
(20, 4, 13, ' ', ' ', 100, 1, 1),
(20, 5, 7, ' ', ' ', 100, 1, 1),
(20, 5, 8, ' ', ' ', 100, 1, 1),
(20, 5, 13, ' ', ' ', 100, 1, 1),
(20, 6, 7, ' ', ' ', 100, 1, 1),
(20, 6, 8, ' ', ' ', 100, 1, 1),
(20, 6, 13, ' ', ' ', 100, 1, 1),
(20, 7, 7, ' ', ' ', 100, 1, 1),
(20, 7, 8, ' ', ' ', 100, 1, 1),
(20, 7, 13, ' ', ' ', 100, 1, 1),
(20, 8, 7, ' ', ' ', 100, 1, 1),
(20, 8, 8, ' ', ' ', 100, 1, 1),
(20, 8, 13, ' ', ' ', 100, 1, 1),
(20, 9, 7, ' ', ' ', 100, 1, 1),
(20, 9, 8, ' ', ' ', 100, 1, 1),
(20, 9, 13, ' ', ' ', 100, 1, 1),
(21, 1, 14, 'Offer', 'Offer', 2500, 1, 1),
(21, 4, 4, 'Offer', 'Offer', 2900, 0, 0),
(21, 4, 5, 'Offer', 'Offer', 1500, 0, 0),
(21, 5, 4, 'Offer', 'Offer', 2900, 0, 0),
(21, 5, 5, 'Offer', 'Offer', 1500, 0, 0),
(21, 6, 4, 'Offer', 'Offer', 3200, 0, 0),
(21, 6, 5, 'Offer', 'Offer', 1500, 0, 0),
(21, 7, 4, 'Offer', 'Offer', 2900, 0, 0),
(21, 7, 5, 'Offer', 'Offer', 1500, 0, 0),
(21, 8, 4, 'Offer', 'Offer', 2900, 0, 0),
(21, 8, 5, 'Offer', 'Offer', 1500, 0, 0),
(21, 9, 4, 'Offer', 'Offer', 1800, 0, 0),
(21, 9, 5, 'Offer', 'Offer', 1600, 0, 0),
(22, 1, 14, 'Offer Url', 'Offer Url', 2600, 0, 0),
(22, 4, 4, 'Offer Url', 'Offer Url', 3000, 0, 0),
(22, 4, 5, 'Offer Url', 'Offer Url', 1600, 0, 0),
(22, 5, 4, 'Offer Url', 'Offer Url', 3000, 0, 0),
(22, 5, 5, 'Offer Url', 'Offer Url', 1600, 0, 0),
(22, 6, 4, 'Offer Url', 'Offer Url', 3300, 0, 0),
(22, 6, 5, 'Offer Url', 'Offer Url', 1600, 0, 0),
(22, 7, 4, 'Offer Url', 'Offer Url', 3000, 0, 0),
(22, 7, 5, 'Offer Url', 'Offer Url', 1600, 0, 0),
(22, 8, 4, 'Offer Url', 'Offer Url', 3000, 0, 0),
(22, 8, 5, 'Offer Url', 'Offer Url', 1600, 0, 0),
(22, 9, 4, 'Offer Url', 'Offer Url', 1900, 0, 0),
(22, 9, 5, 'Offer Url', 'Offer Url', 1700, 0, 0),
(25, 9, 1, 'EPS', 'EPS', 1900, 1, 1),
(25, 9, 4, 'EPS', 'EPS', 2200, 1, 1),
(25, 9, 9, 'EPS', 'EPS', 1900, 1, 1),
(25, 9, 10, 'EPS', 'EPS', 1900, 1, 1),
(26, 6, 11, 'Embed', 'Embed', 600, 1, 1),
(27, 6, 11, 'Embed%', 'Embed%', 700, 1, 1),
(28, 6, 11, 'Popup', 'Popup', 800, 1, 1),
(29, 6, 11, 'Popup%', 'Popup%', 900, 1, 1),
(30, 6, 11, 'Exit', 'Exit', 1000, 1, 1),
(31, 6, 11, 'Exit%', 'Exit%', 1100, 1, 1),
(33, 9, 1, 'Email', 'Email', 200, 1, 1),
(33, 9, 4, 'Email', 'Email', 200, 1, 1),
(33, 9, 5, 'Email', 'Email', 200, 1, 1),
(33, 9, 6, 'Email', 'Email', 200, 1, 1),
(34, 4, 4, 'Subscribers', 'Subscribers', 2300, 1, 1),
(34, 4, 5, 'Subscribers', 'Subscribers', 900, 1, 1),
(34, 4, 7, 'Subscribers', 'Subscribers', 900, 1, 1),
(34, 4, 8, 'Subscribers', 'Subscribers', 900, 1, 1),
(34, 4, 10, 'Subscribers', 'Subscribers', 1200, 1, 1),
(34, 4, 13, 'Subscribers', 'Subscribers', 900, 1, 1),
(34, 4, 16, 'Subscribers', 'Subscribers', 2300, 1, 1),
(34, 6, 4, 'Subscribers', 'Subscribers', 2600, 1, 1),
(34, 6, 11, 'Subscribers', 'Subscribers', 1200, 1, 1),
(34, 6, 16, 'Subscribers', 'Subscribers', 2600, 1, 1),
(35, 4, 4, 'SR', 'SR', 2400, 1, 1),
(35, 4, 5, 'SR', 'SR', 1000, 1, 1),
(35, 4, 7, 'SR', 'SR', 1000, 1, 1),
(35, 4, 8, 'SR', 'SR', 1000, 1, 1),
(35, 4, 10, 'SR', 'SR', 1300, 1, 1),
(35, 4, 13, 'SR', 'SR', 1000, 1, 1),
(35, 4, 16, 'SR', 'SR', 2400, 1, 1),
(35, 6, 4, 'SR', 'SR', 2700, 1, 1),
(35, 6, 11, 'SR', 'SR', 1300, 1, 1),
(35, 6, 16, 'SR', 'SR', 2700, 1, 1),
(36, 4, 4, 'CPSUB', 'CPSUB', 2500, 1, 1),
(36, 4, 5, 'CPSUB', 'CPSUB', 1100, 1, 1),
(36, 4, 7, 'CPSUB', 'CPSUB', 1100, 1, 1),
(36, 4, 8, 'CPSUB', 'CPSUB', 1100, 1, 1),
(36, 4, 10, 'CPSUB', 'CPSUB', 1400, 1, 1),
(36, 4, 13, 'CPSUB', 'CPSUB', 1100, 1, 1),
(36, 4, 16, 'CPSUB', 'CPSUB', 2500, 1, 1),
(36, 6, 4, 'CPSUB', 'CPSUB', 2800, 1, 1),
(36, 6, 16, 'CPSUB', 'CPSUB', 2800, 1, 1),
(37, 1, 14, 'Extra1', 'Extra1', 600, 0, 0),
(37, 4, 4, 'Extra1', 'Extra1', 900, 1, 1),
(37, 4, 16, 'Extra1', 'Extra1', 900, 1, 1),
(37, 5, 4, 'Extra1', 'Extra1', 900, 1, 1),
(37, 5, 16, 'Extra1', 'Extra1', 900, 1, 1),
(37, 6, 4, 'Extra1', 'Extra1', 900, 1, 1),
(37, 6, 16, 'Extra1', 'Extra1', 900, 1, 1),
(37, 7, 4, 'Extra1', 'Extra1', 900, 1, 1),
(37, 7, 16, 'Extra1', 'Extra1', 900, 1, 1),
(37, 8, 4, 'Extra1', 'Extra1', 900, 1, 1),
(37, 8, 16, 'Extra1', 'Extra1', 900, 1, 1),
(38, 1, 14, 'Extra2', 'Extra2', 700, 0, 0),
(38, 4, 4, 'Extra2', 'Extra2', 1000, 1, 1),
(38, 4, 16, 'Extra2', 'Extra2', 1000, 1, 1),
(38, 5, 4, 'Extra2', 'Extra2', 1000, 1, 1),
(38, 5, 16, 'Extra2', 'Extra2', 1000, 1, 1),
(38, 6, 4, 'Extra2', 'Extra2', 1000, 1, 1),
(38, 6, 16, 'Extra2', 'Extra2', 1000, 1, 1),
(38, 7, 4, 'Extra2', 'Extra2', 1000, 1, 1),
(38, 7, 16, 'Extra2', 'Extra2', 1000, 1, 1),
(38, 8, 4, 'Extra2', 'Extra2', 1000, 1, 1),
(38, 8, 16, 'Extra2', 'Extra2', 1000, 1, 1),
(39, 1, 14, 'Extra3', 'Extra3', 800, 0, 0),
(39, 4, 4, 'Extra3', 'Extra3', 1100, 1, 1),
(39, 4, 16, 'Extra3', 'Extra3', 1100, 1, 1),
(39, 5, 4, 'Extra3', 'Extra3', 1100, 1, 1),
(39, 5, 16, 'Extra3', 'Extra3', 1100, 1, 1),
(39, 6, 4, 'Extra3', 'Extra3', 1100, 1, 1),
(39, 6, 16, 'Extra3', 'Extra3', 1100, 1, 1),
(39, 7, 4, 'Extra3', 'Extra3', 1100, 1, 1),
(39, 7, 16, 'Extra3', 'Extra3', 1100, 1, 1),
(39, 8, 4, 'Extra3', 'Extra3', 1100, 1, 1),
(39, 8, 16, 'Extra3', 'Extra3', 1100, 1, 1),
(40, 1, 14, 'Extra4', 'Extra4', 900, 0, 0),
(40, 4, 4, 'Extra4', 'Extra4', 1200, 1, 1),
(40, 4, 16, 'Extra4', 'Extra4', 1200, 1, 1),
(40, 5, 4, 'Extra4', 'Extra4', 1200, 1, 1),
(40, 5, 16, 'Extra4', 'Extra4', 1200, 1, 1),
(40, 6, 4, 'Extra4', 'Extra4', 1200, 1, 1),
(40, 6, 16, 'Extra4', 'Extra4', 1200, 1, 1),
(40, 7, 4, 'Extra4', 'Extra4', 1200, 1, 1),
(40, 7, 16, 'Extra4', 'Extra4', 1200, 1, 1),
(40, 8, 4, 'Extra4', 'Extra4', 1200, 1, 1),
(40, 8, 16, 'Extra4', 'Extra4', 1200, 1, 1),
(41, 1, 14, 'Extra5', 'Extra5', 1000, 0, 0),
(41, 4, 4, 'Extra5', 'Extra5', 1300, 1, 1),
(41, 4, 16, 'Extra5', 'Extra5', 1300, 1, 1),
(41, 5, 4, 'Extra5', 'Extra5', 1300, 1, 1),
(41, 5, 16, 'Extra5', 'Extra5', 1300, 1, 1),
(41, 6, 4, 'Extra5', 'Extra5', 1300, 1, 1),
(41, 6, 16, 'Extra5', 'Extra5', 1300, 1, 1),
(41, 7, 4, 'Extra5', 'Extra5', 1300, 1, 1),
(41, 7, 16, 'Extra5', 'Extra5', 1300, 1, 1),
(41, 8, 4, 'Extra5', 'Extra5', 1300, 1, 1),
(41, 8, 16, 'Extra5', 'Extra5', 1300, 1, 1),
(42, 1, 14, 'Extra6', 'Extra6', 1100, 0, 0),
(42, 4, 4, 'Extra6', 'Extra6', 1400, 1, 1),
(42, 4, 16, 'Extra6', 'Extra6', 1400, 1, 1),
(42, 5, 4, 'Extra6', 'Extra6', 1400, 1, 1),
(42, 5, 16, 'Extra6', 'Extra6', 1400, 1, 1),
(42, 6, 4, 'Extra6', 'Extra6', 1400, 1, 1),
(42, 6, 16, 'Extra6', 'Extra6', 1400, 1, 1),
(42, 7, 4, 'Extra6', 'Extra6', 1400, 1, 1),
(42, 7, 16, 'Extra6', 'Extra6', 1400, 1, 1),
(42, 8, 4, 'Extra6', 'Extra6', 1400, 1, 1),
(42, 8, 16, 'Extra6', 'Extra6', 1400, 1, 1),
(43, 1, 14, 'Extra7', 'Extra7', 1200, 0, 0),
(43, 4, 4, 'Extra7', 'Extra7', 1500, 1, 1),
(43, 4, 16, 'Extra7', 'Extra7', 1500, 1, 1),
(43, 5, 4, 'Extra7', 'Extra7', 1500, 1, 1),
(43, 5, 16, 'Extra7', 'Extra7', 1500, 1, 1),
(43, 6, 4, 'Extra7', 'Extra7', 1500, 1, 1),
(43, 6, 16, 'Extra7', 'Extra7', 1500, 1, 1),
(43, 7, 4, 'Extra7', 'Extra7', 1500, 1, 1),
(43, 7, 16, 'Extra7', 'Extra7', 1500, 1, 1),
(43, 8, 4, 'Extra7', 'Extra7', 1500, 1, 1),
(43, 8, 16, 'Extra7', 'Extra7', 1500, 1, 1),
(44, 1, 14, 'Extra8', 'Extra8', 1300, 0, 0),
(44, 4, 4, 'Extra8', 'Extra8', 1600, 1, 1),
(44, 4, 16, 'Extra8', 'Extra8', 1600, 1, 1),
(44, 5, 4, 'Extra8', 'Extra8', 1600, 1, 1),
(44, 5, 16, 'Extra8', 'Extra8', 1600, 1, 1),
(44, 6, 4, 'Extra8', 'Extra8', 1600, 1, 1),
(44, 6, 16, 'Extra8', 'Extra8', 1600, 1, 1),
(44, 7, 4, 'Extra8', 'Extra8', 1600, 1, 1),
(44, 7, 16, 'Extra8', 'Extra8', 1600, 1, 1),
(44, 8, 4, 'Extra8', 'Extra8', 1600, 1, 1),
(44, 8, 16, 'Extra8', 'Extra8', 1600, 1, 1),
(45, 1, 14, 'Extra9', 'Extra9', 1400, 0, 0),
(45, 4, 4, 'Extra9', 'Extra9', 1700, 1, 1),
(45, 4, 16, 'Extra9', 'Extra9', 1700, 1, 1),
(45, 5, 4, 'Extra9', 'Extra9', 1700, 1, 1),
(45, 5, 16, 'Extra9', 'Extra9', 1700, 1, 1),
(45, 6, 4, 'Extra9', 'Extra9', 1700, 1, 1),
(45, 6, 16, 'Extra9', 'Extra9', 1700, 1, 1),
(45, 7, 4, 'Extra9', 'Extra9', 1700, 1, 1),
(45, 7, 16, 'Extra9', 'Extra9', 1700, 1, 1),
(45, 8, 4, 'Extra9', 'Extra9', 1700, 1, 1),
(45, 8, 16, 'Extra9', 'Extra9', 1700, 1, 1),
(46, 1, 14, 'Extra10', 'Extra10', 1500, 0, 0),
(46, 4, 4, 'Extra10', 'Extra10', 1800, 1, 1),
(46, 4, 16, 'Extra10', 'Extra10', 1800, 1, 1),
(46, 5, 4, 'Extra10', 'Extra10', 1800, 1, 1),
(46, 5, 16, 'Extra10', 'Extra10', 1800, 1, 1),
(46, 6, 4, 'Extra10', 'Extra10', 1800, 1, 1),
(46, 6, 16, 'Extra10', 'Extra10', 1800, 1, 1),
(46, 7, 4, 'Extra10', 'Extra10', 1800, 1, 1),
(46, 7, 16, 'Extra10', 'Extra10', 1800, 1, 1),
(46, 8, 4, 'Extra10', 'Extra10', 1800, 1, 1),
(46, 8, 16, 'Extra10', 'Extra10', 1800, 1, 1),
(47, 4, 1, 'CPA', 'CPA', 1400, 1, 1),
(47, 4, 4, 'CPA', 'CPA', 2800, 1, 1),
(47, 4, 5, 'CPA', 'CPA', 1400, 1, 1),
(47, 4, 6, 'CPA', 'CPA', 1400, 1, 1),
(47, 4, 7, 'CPA', 'CPA', 1400, 1, 1),
(47, 4, 8, 'CPA', 'CPA', 1400, 1, 1),
(47, 4, 12, 'CPA', 'CPA', 1300, 1, 1),
(47, 4, 13, 'CPA', 'CPA', 1400, 1, 1),
(47, 4, 16, 'CPA', 'CPA', 2800, 1, 1),
(47, 5, 1, 'CPA', 'CPA', 1400, 1, 1),
(47, 5, 4, 'CPA', 'CPA', 2800, 1, 1),
(47, 5, 5, 'CPA', 'CPA', 1400, 1, 1),
(47, 5, 6, 'CPA', 'CPA', 1400, 1, 1),
(47, 5, 7, 'CPA', 'CPA', 1400, 1, 1),
(47, 5, 8, 'CPA', 'CPA', 1400, 1, 1),
(47, 5, 13, 'CPA', 'CPA', 1400, 1, 1),
(47, 5, 16, 'CPA', 'CPA', 2800, 1, 1),
(47, 6, 1, 'CPA', 'CPA', 1400, 1, 1),
(47, 6, 4, 'CPA', 'CPA', 3100, 1, 1),
(47, 6, 5, 'CPA', 'CPA', 1400, 1, 1),
(47, 6, 6, 'CPA', 'CPA', 1400, 1, 1),
(47, 6, 7, 'CPA', 'CPA', 1400, 1, 1),
(47, 6, 8, 'CPA', 'CPA', 1400, 1, 1),
(47, 6, 12, 'CPA', 'CPA', 1300, 1, 1),
(47, 6, 13, 'CPA', 'CPA', 1400, 1, 1),
(47, 6, 16, 'CPA', 'CPA', 3100, 1, 1),
(47, 7, 1, 'CPA', 'CPA', 1400, 1, 1),
(47, 7, 4, 'CPA', 'CPA', 2800, 1, 1),
(47, 7, 5, 'CPA', 'CPA', 1400, 1, 1),
(47, 7, 6, 'CPA', 'CPA', 1400, 1, 1),
(47, 7, 7, 'CPA', 'CPA', 1400, 1, 1),
(47, 7, 8, 'CPA', 'CPA', 1400, 1, 1),
(47, 7, 13, 'CPA', 'CPA', 1400, 1, 1),
(47, 7, 16, 'CPA', 'CPA', 2800, 1, 1),
(47, 8, 1, 'CPA', 'CPA', 1400, 1, 1),
(47, 8, 4, 'CPA', 'CPA', 2800, 1, 1),
(47, 8, 5, 'CPA', 'CPA', 1400, 1, 1),
(47, 8, 6, 'CPA', 'CPA', 1400, 1, 1),
(47, 8, 7, 'CPA', 'CPA', 1400, 1, 1),
(47, 8, 8, 'CPA', 'CPA', 1400, 1, 1),
(47, 8, 13, 'CPA', 'CPA', 1400, 1, 1),
(47, 8, 16, 'CPA', 'CPA', 2800, 1, 1),
(47, 9, 1, 'CPA', 'CPA', 1400, 1, 1),
(47, 9, 4, 'CPA', 'CPA', 1700, 1, 1),
(47, 9, 5, 'CPA', 'CPA', 1500, 1, 1),
(47, 9, 6, 'CPA', 'CPA', 1500, 1, 1),
(47, 9, 7, 'CPA', 'CPA', 1400, 1, 1),
(47, 9, 8, 'CPA', 'CPA', 1400, 1, 1),
(47, 9, 9, 'CPA', 'CPA', 1400, 1, 1),
(47, 9, 10, 'CPA', 'CPA', 1400, 1, 1),
(47, 9, 13, 'CPA', 'CPA', 1400, 1, 1),
(48, 1, 14, 'Date/Time', 'Date/Time', 100, 1, 1),
(49, 1, 14, 'Source', 'Source', 200, 1, 1),
(49, 1, 15, 'Traffic Source', 'Traffic Source', 2000, 1, 1),
(50, 1, 14, 'Campaign', 'Campaign', 400, 1, 1),
(51, 1, 14, 'IP', 'IP', 1600, 1, 1),
(52, 1, 14, 'Click Date', 'Click Date', 1700, 0, 0),
(53, 1, 14, 'Click', 'Click', 1800, 1, 1),
(54, 1, 14, 'Referrer-Page', 'Referrer-Page', 1900, 0, 0),
(54, 4, 4, 'Referrer-Page', 'Referrer-Page', 500, 0, 0),
(54, 5, 4, 'Referrer-Page', 'Referrer-Page', 500, 0, 0),
(54, 6, 4, 'Referrer-Page', 'Referrer-Page', 500, 0, 0),
(54, 7, 4, 'Referrer-Page', 'Referrer-Page', 500, 0, 0),
(54, 8, 4, 'Referrer-Page', 'Referrer-Page', 500, 0, 0),
(54, 9, 4, 'Referrer-Page', 'Referrer-Page', 500, 0, 0),
(55, 1, 14, 'Referrer-Domain', 'Referrer-Domain', 2000, 1, 1),
(55, 4, 4, 'Referrer-Domain', 'Referrer-Domain', 600, 0, 0),
(55, 5, 4, 'Referrer-Domain', 'Referrer-Domain', 600, 0, 0),
(55, 6, 4, 'Referrer-Domain', 'Referrer-Domain', 600, 0, 0),
(55, 7, 4, 'Referrer-Domain', 'Referrer-Domain', 600, 0, 0),
(55, 8, 4, 'Referrer-Domain', 'Referrer-Domain', 600, 0, 0),
(55, 9, 4, 'Referrer-Domain', 'Referrer-Domain', 600, 0, 0),
(56, 1, 14, 'Conversion Date', 'Conversion Date', 2300, 0, 0),
(57, 1, 14, 'Conversion', 'Conversion', 2400, 1, 1),
(58, 1, 14, 'SubId', 'SubId', 2700, 1, 1),
(59, 1, 14, 'Country', 'Country', 2800, 1, 1),
(60, 1, 14, 'State', 'State', 2900, 1, 1),
(61, 1, 14, 'City', 'City', 3000, 1, 1),
(62, 4, 5, 'ID', 'ID', 100, 1, 1),
(62, 4, 6, 'ID', 'ID', 100, 1, 1),
(62, 5, 5, 'ID', 'ID', 100, 1, 1),
(62, 5, 6, 'ID', 'ID', 100, 1, 1),
(62, 6, 5, 'ID', 'ID', 100, 1, 1),
(62, 6, 6, 'ID', 'ID', 100, 1, 1),
(62, 6, 11, 'ID', 'ID', 100, 1, 1),
(62, 7, 5, 'ID', 'ID', 100, 1, 1),
(62, 7, 6, 'ID', 'ID', 100, 1, 1),
(62, 8, 5, 'ID', 'ID', 100, 1, 1),
(62, 8, 6, 'ID', 'ID', 100, 1, 1),
(62, 9, 5, 'ID', 'ID', 100, 1, 1),
(62, 9, 6, 'ID', 'ID', 100, 1, 1),
(63, 4, 4, 'Category', 'Category', 700, 0, 0),
(63, 5, 4, 'Category', 'Category', 700, 0, 0),
(63, 6, 4, 'Category', 'Category', 700, 0, 0),
(63, 7, 4, 'Category', 'Category', 700, 0, 0),
(63, 8, 4, 'Category', 'Category', 700, 0, 0),
(63, 9, 4, 'Category', 'Category', 700, 0, 0),
(64, 4, 4, 'Ad', 'Ad', 800, 1, 1),
(64, 4, 16, 'Ad', 'Ad', 800, 1, 1),
(64, 5, 4, 'Ad', 'Ad', 800, 1, 1),
(64, 5, 16, 'Ad', 'Ad', 800, 1, 1),
(64, 6, 4, 'Ad', 'Ad', 800, 1, 1),
(64, 6, 16, 'Ad', 'Ad', 800, 1, 1),
(64, 7, 4, 'Ad', 'Ad', 800, 1, 1),
(64, 7, 16, 'Ad', 'Ad', 800, 1, 1),
(64, 8, 4, 'Ad', 'Ad', 800, 1, 1),
(64, 8, 16, 'Ad', 'Ad', 800, 1, 1),
(65, 1, 15, 'Status', 'Status', 1000, 1, 1),
(66, 1, 15, 'Group', 'Group', 3000, 1, 1),
(67, 1, 15, 'Bidding', 'Bidding', 4000, 1, 1),
(68, 1, 15, 'Date Added', 'Date Added', 5000, 1, 1),
(69, 1, 15, 'Last Update', 'Last Update', 6000, 1, 1),
(70, 4, 4, 'Day', 'Day', 1840, 0, 0),
(70, 5, 4, 'Day', 'Day', 1840, 0, 0),
(70, 6, 4, 'Day', 'Day', 1840, 0, 0),
(70, 7, 4, 'Day', 'Day', 1840, 0, 0),
(70, 8, 4, 'Day', 'Day', 1840, 0, 0),
(71, 4, 4, 'Hour', 'Hour', 1860, 0, 0),
(71, 5, 4, 'Hour', 'Hour', 1860, 0, 0),
(71, 6, 4, 'Hour', 'Hour', 1860, 0, 0),
(71, 7, 4, 'Hour', 'Hour', 1860, 0, 0),
(71, 8, 4, 'Hour', 'Hour', 1860, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `Name` varchar(100) NOT NULL,
  `Value` varchar(100) NOT NULL,
  PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`Name`, `Value`) VALUES
('croncampaigns', '0'),
('croncampaignslastid', '0'),
('filterstatus', '-1'),
('loginpage', 'login.php'),
('notemplateaction', '{blank}'),
('version', '2.16');

-- --------------------------------------------------------

--
-- Table structure for table `cpvsources`
--

CREATE TABLE IF NOT EXISTS `cpvsources` (
  `CpvSourceID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Source` varchar(255) NOT NULL,
  `SourceID` varchar(255) NOT NULL,
  `KeywordToken` varchar(255) NOT NULL,
  `UniqueToken` varchar(255) NOT NULL DEFAULT '',
  `AppendToken` varchar(255) NOT NULL DEFAULT '',
  `Timezone` varchar(100) NOT NULL DEFAULT 'America/New_York',
  `CostTypeID` tinyint(4) NOT NULL DEFAULT '1',
  `AdTokenName` varchar(250) NOT NULL DEFAULT '',
  `AdTokenUrl` varchar(250) NOT NULL DEFAULT '',
  `AdTokenParam` varchar(100) NOT NULL DEFAULT '',
  `DateAdded` datetime NOT NULL,
  PRIMARY KEY (`CpvSourceID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `cpvsources`
--

INSERT INTO `cpvsources` (`CpvSourceID`, `Source`, `SourceID`, `KeywordToken`, `UniqueToken`, `AppendToken`, `Timezone`, `CostTypeID`, `AdTokenName`, `AdTokenUrl`, `AdTokenParam`, `DateAdded`) VALUES
(1, 'MediaTraffic', 'tm', 'target_passthrough', '', '', 'America/New_York', 1, '', '', '', '2010-03-21 10:40:09'),
(2, 'Direct CPV - url', 'dtu', 'keyword', '', '&keyword={{sm_vurl}}', 'America/New_York', 1, '', '', '', '2010-04-26 00:00:00'),
(3, 'Direct CPV - keyword', 'dt', 'keyword', '', '&keyword={{keyword}}', 'America/New_York', 1, '', '', '', '2010-04-24 10:58:42'),
(4, 'Jema Media', 'mam', 'keyword', '', '&keyword=[keyword]', 'America/Chicago', 1, '', '', '', '2010-04-24 10:58:53'),
(5, 'Ad On Network', 'nad', 'keyword', '', '&keyword=SEARCHTEXT', 'America/Los_Angeles', 1, '', '', '', '2010-04-24 10:59:05'),
(6, 'Lead Impact', 'dlp', 'keyword', '', '&keyword=%KEYWORD%', 'America/Los_Angeles', 1, '', '', '', '2010-04-24 10:59:46'),
(7, 'CPV Marketplace', 'cpl', 'keyword', '', '&keyword=[keyword]', 'America/New_York', 1, '', '', '', '2010-04-24 11:00:07'),
(8, 'Trafficvance', 'tv', 'keyword', '', '&keyword={keyword}', 'America/Los_Angeles', 1, '', '', '', '2010-04-27 00:00:00'),
(9, 'Clicksor', 'cs', 'keyword', '', '&keyword=[keyword]', 'America/New_York', 1, '', '', '', '2010-04-27 00:00:00'),
(10, 'Google Adwords - Search', 'adwds', 'keyword', '', '&keyword={keyword}', 'America/Los_Angeles', 2, 'Ad / Creative', '&ad={creative}', 'ad', '2012-10-16 00:00:00'),
(11, 'Google Adwords - Display', 'adwdd', 'keyword', '', '&keyword={placement}', 'America/Los_Angeles', 2, 'Ad / Creative', '&ad={creative}', 'ad', '2012-10-16 00:00:00'),
(12, 'Site Scout RTB', 'ssrtb', 'keyword', '', '&keyword={SITEID}', 'America/New_York', 2, 'Ad / Creative', '&ad={ADID}', 'ad', '2012-10-16 00:00:00'),
(13, 'Bing', 'bng', 'keyword', '', '&keyword={keyword}', 'America/Los_Angeles', 2, 'Ad / Creative', '&ad={AdId}', 'ad', '2012-10-16 00:00:00'),
(14, 'Plenty of Fish', 'pof', 'keyword', '', '&keyword=EDIT', 'America/Los_Angeles', 2, 'Ad / Creative', '&ad={creativeid:}', 'ad', '2012-10-16 00:00:00'),
(15, 'Adoori', 'adri', 'keyword', '', '&keyword=keywordSearched', 'America/Los_Angeles', 1, '', '', '', '2012-10-16 00:00:00'),
(16, 'JumpTap', 'jmptp', 'keyword', '', '&keyword=JT_KEYWORD', 'America/Los_Angeles', 2, 'Ad / Creative', '&ad=JT_ADBUNDLE', 'ad', '2012-10-16 00:00:00'),
(17, 'ExoClick', 'exck', 'keyword', '', '&keyword={src_hostname}', 'America/New_York', 2, '', '', '', '2012-10-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cpvsourcestokens`
--

CREATE TABLE IF NOT EXISTS `cpvsourcestokens` (
  `CpvSourceTokenID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `CpvSourceID` smallint(5) unsigned NOT NULL,
  `ExtraTokenName` varchar(250) NOT NULL,
  `ExtraTokenUrl` varchar(250) NOT NULL,
  `ExtraTokenParam` varchar(100) NOT NULL,
  PRIMARY KEY (`CpvSourceTokenID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `cpvsourcestokens`
--

INSERT INTO `cpvsourcestokens` (`CpvSourceTokenID`, `CpvSourceID`, `ExtraTokenName`, `ExtraTokenUrl`, `ExtraTokenParam`) VALUES
(1, 5, 'Partner ID', '&partner=PARTNERID', 'partner'),
(2, 5, 'Referrer', '&refr=${referurl}$', 'refr'),
(3, 10, 'Match Type', '&match={matchtype}', 'match'),
(4, 10, 'Ad Position', '&adpos={adposition}', 'adpos'),
(5, 10, 'Network', '&network={network}', 'network'),
(6, 10, 'Mobile', '&mobl={ifmobile:Y}', 'mobl'),
(7, 10, 'Search', '&srchn={ifsearch:Y}', 'srchn'),
(8, 10, 'Display', '&dis={ifcontent:Y}', 'dis'),
(9, 11, 'Category', '&category={target}', 'category'),
(10, 11, 'Network', '&network={network}', 'network'),
(11, 11, 'Mobile', '&mobl={ifmobile:Y}', 'mobl'),
(12, 11, 'Display', '&dis={ifcontent:Y}', 'dis'),
(13, 12, 'Campaign ID', '&campid={CAMPAIGNID}', 'campid'),
(14, 13, 'Query', '&query={QueryString}', 'query'),
(15, 13, 'Match Type', '&match={MatchType}', 'match'),
(16, 14, 'State', '&state={state:default}', 'state'),
(17, 14, 'Gender', '&gender={gender:default}', 'gender'),
(18, 14, 'Age', '&age={age:default}', 'age'),
(19, 16, 'Publisher', '&pub=JT_PUBLISHER', 'pub'),
(20, 16, 'Site', '&site=JT_SITE', 'site'),
(21, 16, 'Handset', '&hand=JT_HANDSET', 'hand'),
(22, 16, 'Operator', '&op=JT_OPERATOR', 'op'),
(23, 17, 'User Search', '&usrch={keyword}', 'usrch'),
(24, 17, 'Campaign ID', '&cpnid={campaign_id}', 'cpnid');

-- --------------------------------------------------------

--
-- Table structure for table `cpvtemplates`
--

CREATE TABLE IF NOT EXISTS `cpvtemplates` (
  `CpvTemplateID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `CpvTemplate` varchar(200) NOT NULL,
  `RowsToSkip` smallint(5) unsigned NOT NULL,
  `KeywordColumn` varchar(200) NOT NULL,
  `ViewsColumn` varchar(200) NOT NULL,
  `CostColumn` varchar(200) NOT NULL,
  `CampaignIDColumn` varchar(200) NOT NULL DEFAULT 'CampaignID',
  `CampaignNameColumn` varchar(200) NOT NULL DEFAULT 'Campaign',
  `Active` tinyint(1) NOT NULL,
  `CpvSourceID` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`CpvTemplateID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `cpvtemplates`
--

INSERT INTO `cpvtemplates` (`CpvTemplateID`, `CpvTemplate`, `RowsToSkip`, `KeywordColumn`, `ViewsColumn`, `CostColumn`, `CampaignIDColumn`, `CampaignNameColumn`, `Active`, `CpvSourceID`) VALUES
(1, 'MediaTraffic - Target, Categories Report', 2, 'Targets|Categories', 'Views', 'Amounts', 'CampaignID', 'Campaign', 1, 1),
(2, 'Jema Media - Keyword Report', 7, 'Keyword', 'Impressions', 'Cost', 'CampaignID', 'Campaign', 1, 4),
(3, 'AdOnNetwork - Keyword Report - Standard Report', 4, 'Keyword ', ' Views ', ' Amt Spent ', 'CampaignID', 'Campaign', 1, 5),
(4, 'AdOnNetwork - Keyword Report - Daily Breakdown', 4, 'Keyword ', ' Views ', ' Amt Spent ', 'CampaignID', 'Campaign', 1, 5),
(5, 'Trafficvance - Daily Targets Export', 0, 'Target', 'Impressions', 'Cost', 'CampaignID', 'Campaign', 1, 8),
(6, 'Trafficvance - Monthly Targets Export', 0, 'Target', 'Impressions', 'Cost', 'CampaignID', 'Campaign', 1, 8),
(7, 'LeadImpact - Keyword Level', 0, 'Keyword', 'BillingImpressions', 'BillingRevenue', 'CampaignID', 'Campaign', 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `cronresults`
--

CREATE TABLE IF NOT EXISTS `cronresults` (
  `CronResultID` int(11) NOT NULL,
  `CronKey` varchar(100) NOT NULL,
  `CronValue` varchar(200) NOT NULL,
  PRIMARY KEY (`CronResultID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cronresults`
--

INSERT INTO `cronresults` (`CronResultID`, `CronKey`, `CronValue`) VALUES
(1, 'ViewsToday', '0'),
(2, 'ViewsYesterday', '0'),
(3, 'ViewsWeek', '0'),
(4, 'ViewsMonth', '0'),
(5, 'RevenueToday', '0'),
(6, 'RevenueYesterday', '0'),
(7, 'RevenueWeek', '0'),
(8, 'RevenueMonth', '0');

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE IF NOT EXISTS `destinations` (
  `DestinationID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CampaignID` int(10) unsigned NOT NULL,
  `PathID` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Offer` varchar(500) NOT NULL,
  `Url` varchar(500) NOT NULL,
  `Payout` double NOT NULL DEFAULT '0',
  `Share` tinyint(4) NOT NULL DEFAULT '0',
  `CurrentShare` tinyint(4) NOT NULL DEFAULT '0',
  `LandingPageID` int(11) NOT NULL DEFAULT '0',
  `AffiliateSourceID` smallint(5) unsigned NOT NULL DEFAULT '0',
  `Level` tinyint(4) NOT NULL DEFAULT '1',
  `Sent` int(11) NOT NULL DEFAULT '0',
  `SharePath` tinyint(4) NOT NULL DEFAULT '50',
  `CurrentSharePath` tinyint(4) NOT NULL DEFAULT '0',
  `Inactive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`DestinationID`),
  KEY `idxCampaignID` (`CampaignID`),
  KEY `idxCampaignInactiveLevel` (`CampaignID`,`Inactive`,`Level`),
  KEY `idxCampaignPath` (`CampaignID`,`PathID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131 ;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`DestinationID`, `CampaignID`, `PathID`, `Offer`, `Url`, `Payout`, `Share`, `CurrentShare`, `LandingPageID`, `AffiliateSourceID`, `Level`, `Sent`, `SharePath`, `CurrentSharePath`, `Inactive`) VALUES
(1, 32, 0, 'dk4mmtnn', 'http://freerealdating.com/dk/dk4mmtnn', 0, 50, 0, 0, 0, 1, 0, 50, 0, 0),
(2, 32, 0, 'dk5coonh', 'http://freerealdating.com/dk/dk5coonh', 0, 50, 1, 1, 0, 1, 0, 50, 0, 0),
(3, 32, 0, 'ADSIMILIS 2154 Erovie 30+ [DK] $8.20 88766', 'http://go.adsimilis.com/aff_c?offer_id=88766&aff_id=5978&aff_sub=', 8.2, 33, 0, 0, 10, 2, 0, 50, 0, 0),
(4, 32, 0, 'ADSIMILIS 2154 Erovie 30+ [DK] $8.20 11110', 'http://go.adsimilis.com/aff_c?offer_id=11110&aff_id=5978&aff_sub=', 8.2, 33, 0, 1, 10, 2, 0, 50, 0, 0),
(5, 32, 0, 'ADSIMILIS 2154 Erovie 30+ [DK] $8.20 22993', 'http://go.adsimilis.com/aff_c?offer_id= 22993&aff_id=5978&aff_sub=', 8.2, 34, 1, 4, 10, 2, 0, 50, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `errors`
--

CREATE TABLE IF NOT EXISTS `errors` (
  `ErrorID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ErrorDate` datetime NOT NULL,
  `UserID` int(11) DEFAULT NULL,
  `ErrorTypeID` tinyint(3) unsigned NOT NULL DEFAULT '3',
  `Page` varchar(200) NOT NULL DEFAULT '',
  `Context` varchar(200) NOT NULL DEFAULT '',
  `Error` varchar(5000) NOT NULL DEFAULT '',
  `Query` varchar(5000) NOT NULL DEFAULT '',
  `ErrorCode` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`ErrorID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `errortypes`
--

CREATE TABLE IF NOT EXISTS `errortypes` (
  `ErrorTypeID` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ErrorType` varchar(200) NOT NULL,
  PRIMARY KEY (`ErrorTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `errortypes`
--

INSERT INTO `errortypes` (`ErrorTypeID`, `ErrorType`) VALUES
(1, 'Install'),
(2, 'Upgrade'),
(3, 'Website'),
(4, 'Cron job');

-- --------------------------------------------------------

--
-- Table structure for table `hiddencampaignalerts`
--

CREATE TABLE IF NOT EXISTS `hiddencampaignalerts` (
  `CampaignID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Views` bigint(20) NOT NULL,
  PRIMARY KEY (`CampaignID`,`Views`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hiddentargetalerts`
--

CREATE TABLE IF NOT EXISTS `hiddentargetalerts` (
  `CampaignID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SubIdID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CampaignID`,`SubIdID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE IF NOT EXISTS `logins` (
  `LoginID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LoginDate` datetime DEFAULT NULL,
  `IPAddress` int(10) unsigned NOT NULL DEFAULT '2130706433',
  `Username` varchar(255) DEFAULT '',
  `Password` varchar(255) DEFAULT '',
  PRIMARY KEY (`LoginID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`LoginID`, `LoginDate`, `IPAddress`, `Username`, `Password`) VALUES
(1, '2015-08-18 23:15:01', 2130706433, '', ''),
(2, '2015-08-18 23:15:45', 2130706433, 'Admin', 'Admin'),
(3, '2015-09-04 15:39:33', 2130706433, 'admin', 'root'),
(4, '2015-09-04 15:39:40', 2130706433, 'admin', 'admin'),
(5, '2015-09-04 15:39:45', 2130706433, 'root', 'admin'),
(6, '2015-09-04 15:39:58', 2130706433, 'admin', 'root'),
(7, '2015-09-04 15:40:06', 2130706433, 'admin', 'pass'),
(8, '2015-09-04 15:40:14', 2130706433, 'admin', 'root'),
(9, '2015-09-05 16:56:52', 2130706433, 'root', 'root'),
(10, '2015-09-07 13:05:52', 2130706433, 'root', 'root'),
(11, '2015-09-07 13:56:56', 2130706433, 'root', 'root'),
(12, '2015-09-08 04:50:15', 2130706433, 'root', 'root'),
(13, '2015-09-08 05:30:08', 2130706433, 'root', 'root'),
(14, '2015-09-08 18:01:15', 2130706433, 'root', 'root'),
(15, '2015-09-08 21:12:15', 2130706433, 'root', 'root');

-- --------------------------------------------------------

--
-- Table structure for table `optimizationprofiles`
--

CREATE TABLE IF NOT EXISTS `optimizationprofiles` (
  `OptimizationProfileID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `OptimizationProfileName` varchar(200) NOT NULL,
  `DefaultProfile` tinyint(1) NOT NULL DEFAULT '0',
  `topt1ROI` varchar(10) DEFAULT NULL,
  `topt1PPV` varchar(10) DEFAULT NULL,
  `lopt2ROI` varchar(10) DEFAULT NULL,
  `lopt2PPV` varchar(10) DEFAULT NULL,
  `oopt3ROI` varchar(10) DEFAULT NULL,
  `topt4Views` varchar(10) DEFAULT NULL,
  `topt4Clicks` varchar(10) DEFAULT NULL,
  `topt5Views` varchar(10) DEFAULT NULL,
  `topt5Conversion` varchar(10) DEFAULT NULL,
  `topt6Views` varchar(10) DEFAULT NULL,
  `topt6ROI` varchar(10) DEFAULT NULL,
  `topt6PPV` varchar(10) DEFAULT NULL,
  `lopt7Views` varchar(10) DEFAULT NULL,
  `lopt7Clicks` varchar(10) DEFAULT NULL,
  `lopt8Views` varchar(10) DEFAULT NULL,
  `lopt8Conversion` varchar(10) DEFAULT NULL,
  `oopt9Views` varchar(10) DEFAULT NULL,
  `oopt9Conversion` varchar(10) DEFAULT NULL,
  `oopt10Visitors` varchar(10) DEFAULT NULL,
  `oopt10Conversion` varchar(10) DEFAULT NULL,
  `CreateDate` datetime NOT NULL,
  `ModifyDate` datetime DEFAULT NULL,
  PRIMARY KEY (`OptimizationProfileID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `optimizationprofiles`
--

INSERT INTO `optimizationprofiles` (`OptimizationProfileID`, `OptimizationProfileName`, `DefaultProfile`, `topt1ROI`, `topt1PPV`, `lopt2ROI`, `lopt2PPV`, `oopt3ROI`, `topt4Views`, `topt4Clicks`, `topt5Views`, `topt5Conversion`, `topt6Views`, `topt6ROI`, `topt6PPV`, `lopt7Views`, `lopt7Clicks`, `lopt8Views`, `lopt8Conversion`, `oopt9Views`, `oopt9Conversion`, `oopt10Visitors`, `oopt10Conversion`, `CreateDate`, `ModifyDate`) VALUES
(1, 'Default Optimization Profile', 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-12-01 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `optimizations`
--

CREATE TABLE IF NOT EXISTS `optimizations` (
  `OptimizeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OptimizeName` varchar(45) NOT NULL,
  `OptimizeValue` varchar(45) NOT NULL,
  PRIMARY KEY (`OptimizeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `optimizations`
--

INSERT INTO `optimizations` (`OptimizeID`, `OptimizeName`, `OptimizeValue`) VALUES
(1, 'topt1ROI', ''),
(2, 'topt1PPV', ''),
(3, 'lopt2ROI', ''),
(4, 'lopt2PPV', ''),
(5, 'oopt3ROI', ''),
(6, 'topt4Views', ''),
(7, 'topt4Clicks', ''),
(8, 'topt5Views', ''),
(9, 'topt5Conversion', ''),
(10, 'topt6Views', ''),
(11, 'topt6ROI', ''),
(12, 'topt6PPV', ''),
(13, 'lopt7Views', ''),
(14, 'lopt7Clicks', ''),
(15, 'lopt8Views', ''),
(16, 'lopt8Conversion', ''),
(17, 'oopt9Views', ''),
(18, 'oopt9Conversion', ''),
(19, 'oopt10Visitors', ''),
(20, 'oopt10Conversion', '');

-- --------------------------------------------------------

--
-- Table structure for table `parsingtemplates`
--

CREATE TABLE IF NOT EXISTS `parsingtemplates` (
  `ParsingTemplateID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `ReferrerName` varchar(250) NOT NULL,
  `Parameter` varchar(250) NOT NULL,
  `SiteCategoryID` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ParsingTemplateID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=201 ;

--
-- Dumping data for table `parsingtemplates`
--

INSERT INTO `parsingtemplates` (`ParsingTemplateID`, `ReferrerName`, `Parameter`, `SiteCategoryID`) VALUES
(1, 'digg.com', 's', 3),
(2, 'twitter.com', 'q', 23),
(3, 'cnet.com', 'query', 22),
(4, 'autopartswarehouse.com', 'Ntt', 22),
(5, 'autozone.com', 'searchText', 22),
(6, 'slickdeals.net', 'search', 4),
(7, 'cheapuncle.com', 'k/', 4),
(8, 'fatwallet.com', 'query', 4),
(9, 'retailmenot.com', 'query', 4),
(10, 'wikipedia.org', 'wiki/', 20),
(11, 'earnmydegree.com', 'q', 7),
(12, 'billboard.com', 'search/', 8),
(13, 'download.cnet.com', 'query', 6),
(14, 'filehippo.com', 'q', 6),
(15, '4shared.com', '1/', 6),
(16, 'thepiratebay.org', 'search/', 6),
(17, 'filestube.com', 'q', 6),
(18, 'ign.com', 'query', 10),
(19, 'ezinearticles.com', 'q', 2),
(20, 'findarticles.com', 'qt', 2),
(21, 'ftc.gov', 'q', 20),
(22, 'answers.com', 'topic/', 17),
(23, 'associatedcontent.com', 'article/', 2),
(24, 'doityourself.com', 'qt', 5),
(25, 'ehow.com', 's', 5),
(26, 'howstuffworks.com', 'terms', 5),
(27, 'scribd.com', 'q', 20),
(28, 'aolhealth.com', 'q1', 11),
(29, 'drugs.com', 'searchterm', 11),
(30, 'healthboards.com', 'searchid', 11),
(31, 'healthgrades.com', 'q', 11),
(32, 'mayoclinic.com', 'search/', 11),
(33, 'webmd.com', 'query', 11),
(34, 'wellness.com', 'q,q_base', 11),
(35, 'wikihow.com', 'q', 20),
(36, 'about.com', 'terms', 20),
(37, 'servicemagic.com', 'query', 12),
(38, 'businessweek.com', 'searchTerm', 13),
(39, 'people.com', 'search', 13),
(40, 'cosmopolitan.com', 'search_term', 13),
(41, 'maxim.com', 'keywords', 13),
(42, 'motortrend.com', 'SearchText', 13),
(43, 'parenting.com', 'q', 13),
(44, 'pcmag.com', 'qry', 13),
(45, 'popularmechanics.com', 'search_term', 13),
(46, 'rollingstone.com', 'searchText', 13),
(47, 'askmen.com', 'q', 14),
(48, 'boxofficemojo.com', 'q', 8),
(49, 'fandango.com', 'q', 8),
(50, 'imdb.com', 'q', 8),
(51, 'moviefone.com', 'search/', 8),
(52, 'movieline.com', 'search', 8),
(53, 'rhapsody.com', 'query', 8),
(54, 'abcnews.go.com', 'searchtext', 15),
(55, 'mashable.com', 's', 15),
(56, 'foxsports.com', 'sp_q', 15),
(57, 'nytimes.com', 'query', 15),
(58, 'bbc.co.uk', 'q', 15),
(59, 'cnbc.com', 'keywords', 15),
(60, 'philly.com', 'q', 15),
(61, 'businessinsider.com', 'q', 15),
(62, 'cbsnews.com', 'query', 15),
(63, 'cnn.com', 'query', 15),
(64, 'consumerreports.org', 'query', 15),
(65, 'foxnews.com', 'q', 15),
(66, 'hollywoodreporter.com', 'keyword', 15),
(67, 'huffingtonpost.com', 'q', 15),
(68, 'msnbc.msn.com', 'q', 15),
(69, 'pcworld.com', 'qt', 15),
(70, 'reuters.com', 'blob', 15),
(71, 'usatoday.com', 'q', 15),
(72, 'usmagazine.com', 'q', 15),
(73, 'washingtonpost.com', 'st', 15),
(74, 'washingtontimes.com', 'q', 15),
(75, 'imageshack.us', 'q', 27),
(76, 'megavideo.com', 's', 27),
(77, 'fotolog.com', 'q', 27),
(78, 'tinypic.com', 'tag', 27),
(79, 'vimeo.com', 'videos/', 27),
(80, 'break.com', 'findvideo/', 27),
(81, 'dailymotion.com', 'search/', 27),
(82, 'flickr.com', 'q', 27),
(83, 'metacafe.com', 'topics/', 27),
(84, 'photobucket.com', 'images/', 27),
(85, 'veoh.com', 'videos/', 27),
(86, 'webshots.com', 'query', 27),
(87, 'bizrate.com', 'bizrate.com/', 16),
(88, 'epinions.com', 'search_string', 16),
(89, 'mysimon.com', 'find/', 16),
(90, 'nextag.com', 'nextag.com/', 16),
(91, 'pricegrabber.com', 'pricegrabber.com/', 16),
(92, 'shopzilla.com', '.shopzilla.com/', 16),
(93, 'techbargains.com', 'techbargains.com/', 16),
(94, 'dealtime.com', 'KW', 16),
(95, 'hud.gov', 'q', 18),
(96, 'allrecipes.com', 'WithTerm', 19),
(97, 'myrecipes.com', 'Ntt', 19),
(98, 'simplyrecipes.com', 'q', 19),
(99, 'bettycrocker.com', 'term', 19),
(100, 'cooking.com', 'Keywords', 19),
(101, 'cooks.com', 'q', 19),
(102, '.google.', 'q,as_q', 21),
(103, '.yahoo.', 'p,va', 21),
(104, 'bing.com', 'q', 21),
(105, 'ask.com', 'q', 21),
(106, 'aol.com', 'q', 21),
(107, 'comcast.net', 'q', 21),
(108, 'lycos.com', 'query', 21),
(109, 'comcast.com', 'q', 21),
(110, 'amazon.com', 'field-keywords', 22),
(111, 'ebay.com', '_nkw,_nkwusc', 22),
(112, 'barnesandnoble.com', 'WRD', 22),
(113, 'sephora.com', 'attr1', 22),
(114, 'ae.com', 'question', 22),
(115, 'babycenter.com', 'q', 22),
(116, 'basspro.com', 'hvarSearchString', 22),
(117, 'bedbathandbeyond.com', 'sstr', 22),
(118, 'bestbuy.com', 'st', 22),
(119, 'bhphotovideo.com', 'Ntt', 22),
(120, 'bidz.com', 'txt', 22),
(121, 'buy.com', 'qu', 22),
(122, 'childrensplace.com', 'Ntt', 22),
(123, 'cvs.com', 'addFacet', 22),
(124, 'drugstore.com', 'Ntt', 22),
(125, 'eastbay.com', 'keyword', 22),
(126, 'guitarcenter.com', 'src', 22),
(127, 'homedepot.com', 'keyword', 22),
(128, 'kaboodle.com', 'q', 22),
(129, 'kohls.com', 'searchTerm', 22),
(130, 'llbean.com', 'freeText', 22),
(131, 'lowes.com', 'Ntt', 22),
(132, 'musiciansfriend.com', 'q', 22),
(133, 'mysears.com', 'q', 22),
(134, 'overstock.com', 'keywords', 22),
(135, 'petsmart.com', 'kw', 22),
(136, 'sears.com', 'keyword', 22),
(137, 'shop.com', 'shop.com/', 22),
(138, 'shopping.com', 'KW', 22),
(139, 'sportsauthority.com', 'kw,origkw', 22),
(140, 'staples.com', 'searchKey', 22),
(141, 'target.com', 'keywords', 22),
(142, 'tigerdirect.com', 'keywords', 22),
(143, 'toysrus.com', 'kw,origkw', 22),
(144, 'vitacost.com', 'Ntt', 22),
(145, 'walgreens.com', 'Ntt', 22),
(146, 'walmart.com', 'search_query', 22),
(147, 'zappos.com', 'zappos.com/', 22),
(148, 'macys.com', 'Keyword', 22),
(149, 'jcpenney.com', 'SearchString', 22),
(150, 'mlb.com', 'query', 24),
(151, 'espn.go.com', 'espn.go.com/', 24),
(152, 'fifa.com', 'q', 24),
(153, 'nascar.com', 'text', 24),
(154, 'nba.com', 'text', 24),
(155, 'nfl.com', 'query', 24),
(156, 'aol.com', 'q', 9),
(157, 'bloomberg.com', 'q', 9),
(158, 'forbes.com', 'MT', 9),
(159, 'fool.com', 'q', 9),
(160, 'investors.com', 'Ntt', 9),
(161, 'marketwatch.com', 'q', 9),
(162, 'sharebuilder.com', 'QueryText', 9),
(163, 'tdameritrade.com', 'q,is', 9),
(164, 'stubhub.com', 'searchStr', 25),
(165, 'ticketmaster.com', 'q', 25),
(166, 'tickets.com', 'q', 25),
(167, 'ticketsnow.com', 'WHAT', 25),
(168, 'abc.go.com', 'search', 26),
(169, 'abcfamily.go.com', 'search', 26),
(170, 'animal.discovery.com', 'query', 26),
(171, 'disney.go.com', 'q', 26),
(172, 'planetgreen.discovery.com', 'query', 26),
(173, 'travelchannel.com', 'q', 26),
(174, 'usanetwork.com', 'q', 26),
(175, 'tlc.discovery.com', 'query', 26),
(176, 'aetv.com', 'keywords', 26),
(177, 'bravotv.com', 'query', 26),
(178, 'cartoonnetwork.com', 'keywords', 26),
(179, 'cbs.com', 'query', 26),
(180, 'comedycentral.com', 'term', 26),
(181, 'eonline.com', 'searchString', 26),
(182, 'fancast.com', 's', 26),
(183, 'foodnetwork.com', 'fnSearchString', 26),
(184, 'history.com', 'search-field', 26),
(185, 'hulu.com', 'query', 26),
(186, 'mtv.com', 'q', 26),
(187, 'mylifetime.com', 'q,as_q', 26),
(188, 'mystyle.com', 'searchKeyword', 26),
(189, 'nbc.com', 'searchString', 26),
(190, 'nick.com', 'term', 26),
(191, 'spike.com', 'query', 26),
(192, 'starz.com', 'starz/', 26),
(193, 'sundancechannel.com', 'words', 26),
(194, 'tbs.com', 'query', 26),
(195, 'thewb.com', 'q', 26),
(196, 'tnt.tv', 'query', 26),
(197, 'tvguide.com', 'keyword', 26),
(198, 'vh1.com', 'q', 26),
(199, 'youtube.com', 'search_query', 27),
(200, 'theknot.com', 'q', 28);

-- --------------------------------------------------------

--
-- Table structure for table `referrerdomains`
--

CREATE TABLE IF NOT EXISTS `referrerdomains` (
  `ReferrerDomainID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ReferrerDomain` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`ReferrerDomainID`),
  UNIQUE KEY `idxReferrerDomain` (`ReferrerDomain`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `referrerdomains`
--

INSERT INTO `referrerdomains` (`ReferrerDomainID`, `ReferrerDomain`) VALUES
(1, ''),
(2, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `referrers`
--

CREATE TABLE IF NOT EXISTS `referrers` (
  `ReferrerID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Referrer` varchar(250) NOT NULL DEFAULT '',
  `ReferrerDomainID` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ReferrerID`),
  UNIQUE KEY `idxReferrer` (`Referrer`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `referrers`
--

INSERT INTO `referrers` (`ReferrerID`, `Referrer`, `ReferrerDomainID`) VALUES
(1, '', 1),
(2, 'http://127.0.0.1/cpv/direct-and-landing-campaign.php?id=17', 2);

-- --------------------------------------------------------

--
-- Table structure for table `reportdetailscampaign`
--

CREATE TABLE IF NOT EXISTS `reportdetailscampaign` (
  `ReportDetailID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ReportID` int(10) unsigned NOT NULL,
  `CampaignID` int(10) unsigned NOT NULL,
  `Views` int(11) NOT NULL DEFAULT '0',
  `Clicks` int(10) unsigned NOT NULL DEFAULT '0',
  `CTR` double NOT NULL DEFAULT '0',
  `Cost` double NOT NULL DEFAULT '0',
  `Revenue` double NOT NULL DEFAULT '0',
  `EPV` double NOT NULL DEFAULT '0',
  `PPV` double NOT NULL DEFAULT '0',
  `Profit` double NOT NULL DEFAULT '0',
  `ROI` double NOT NULL DEFAULT '0',
  `Conversion` int(11) NOT NULL DEFAULT '0',
  `CR` double NOT NULL DEFAULT '0',
  `CPA` double NOT NULL DEFAULT '0',
  `Sent` int(11) NOT NULL DEFAULT '0',
  `EPS` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`ReportDetailID`),
  KEY `idxReportID` (`ReportID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reportdetailsgroup`
--

CREATE TABLE IF NOT EXISTS `reportdetailsgroup` (
  `ReportDetailID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ReportID` int(10) unsigned NOT NULL,
  `SubIdID` int(10) unsigned NOT NULL,
  `Views` int(10) unsigned NOT NULL,
  `Cost` double NOT NULL,
  `CPV` double DEFAULT NULL,
  `Clicks` int(11) NOT NULL DEFAULT '0',
  `CTR` double NOT NULL DEFAULT '0',
  `CPC` double NOT NULL DEFAULT '0',
  `Conversion` int(11) NOT NULL DEFAULT '0',
  `CR` double NOT NULL DEFAULT '0',
  `CPA` double NOT NULL DEFAULT '0',
  `Revenue` double NOT NULL DEFAULT '0',
  `EPV` double DEFAULT '0',
  `PPV` double DEFAULT '0',
  `eCPM` double NOT NULL,
  `Profit` double NOT NULL,
  `ROI` double NOT NULL DEFAULT '0',
  `Engages` int(11) NOT NULL DEFAULT '0',
  `EngageRate` double NOT NULL DEFAULT '0',
  `Sent` int(11) NOT NULL DEFAULT '0',
  `EPS` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`ReportDetailID`),
  KEY `idxReportID` (`ReportID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `ReportID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ReportName` varchar(100) NOT NULL,
  `CampaignID` int(10) unsigned NOT NULL,
  `DateAdded` datetime NOT NULL,
  PRIMARY KEY (`ReportID`),
  KEY `idxCampaignID` (`CampaignID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reporttypes`
--

CREATE TABLE IF NOT EXISTS `reporttypes` (
  `ReportTypeID` tinyint(3) unsigned NOT NULL,
  `ReportTypeName` varchar(200) NOT NULL,
  PRIMARY KEY (`ReportTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reporttypes`
--

INSERT INTO `reporttypes` (`ReportTypeID`, `ReportTypeName`) VALUES
(1, 'Reports Target'),
(2, 'Reports Landing'),
(3, 'Reports Offer'),
(4, 'Stats Target'),
(5, 'Stats Landing'),
(6, 'Stats Offer'),
(7, 'Trends Time'),
(8, 'Trends Day of Week'),
(9, 'Reports Campaign'),
(10, 'Stats Campaign'),
(11, 'Stats Opt-In'),
(12, 'Stats Thank You'),
(13, 'Trends Days'),
(14, 'Visitor Stats'),
(15, 'Campaigns'),
(16, 'Ad Performance');

-- --------------------------------------------------------

--
-- Table structure for table `sitecategories`
--

CREATE TABLE IF NOT EXISTS `sitecategories` (
  `SiteCategoryID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `SiteCategory` varchar(100) NOT NULL,
  `CreateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`SiteCategoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `sitecategories`
--

INSERT INTO `sitecategories` (`SiteCategoryID`, `SiteCategory`, `CreateDate`) VALUES
(1, 'Other', '2012-06-01 00:00:00'),
(2, 'Article Directory', '2012-06-01 00:00:00'),
(3, 'Bookmarking', '2012-06-01 00:00:00'),
(4, 'Coupons', '2012-06-01 00:00:00'),
(5, 'DIY', '2012-06-01 00:00:00'),
(6, 'Downloads & Sharing', '2012-06-01 00:00:00'),
(7, 'Education', '2012-06-01 00:00:00'),
(8, 'Entertainment', '2012-06-01 00:00:00'),
(9, 'Finance', '2012-06-01 00:00:00'),
(10, 'Gaming', '2012-06-01 00:00:00'),
(11, 'Health', '2012-06-01 00:00:00'),
(12, 'Local', '2012-06-01 00:00:00'),
(13, 'Magazine', '2012-06-01 00:00:00'),
(14, 'Mens', '2012-06-01 00:00:00'),
(15, 'News', '2012-06-01 00:00:00'),
(16, 'Price Search', '2012-06-01 00:00:00'),
(17, 'Q & A ', '2012-06-01 00:00:00'),
(18, 'Real Estate', '2012-06-01 00:00:00'),
(19, 'Recipes', '2012-06-01 00:00:00'),
(20, 'Reference', '2012-06-01 00:00:00'),
(21, 'Search Engine', '2012-06-01 00:00:00'),
(22, 'Shopping', '2012-06-01 00:00:00'),
(23, 'Social', '2012-06-01 00:00:00'),
(24, 'Sports', '2012-06-01 00:00:00'),
(25, 'Tickets', '2012-06-01 00:00:00'),
(26, 'TV', '2012-06-01 00:00:00'),
(27, 'Video / Media', '2012-06-01 00:00:00'),
(28, 'Wedding', '2012-06-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subids`
--

CREATE TABLE IF NOT EXISTS `subids` (
  `SubIdID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SubId` varchar(45) NOT NULL,
  `Keyword` varchar(250) NOT NULL,
  `DateAdded` datetime NOT NULL,
  PRIMARY KEY (`SubIdID`),
  UNIQUE KEY `idxKeyword` (`Keyword`),
  UNIQUE KEY `idxSubId` (`SubId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `subids`
--

INSERT INTO `subids` (`SubIdID`, `SubId`, `Keyword`, `DateAdded`) VALUES
(1, 'ekmwpdty', '', '2012-06-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `trackings`
--

CREATE TABLE IF NOT EXISTS `trackings` (
  `TrackingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CampaignID` int(10) unsigned DEFAULT NULL,
  `TrackingCode` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`TrackingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trackingsoffers`
--

CREATE TABLE IF NOT EXISTS `trackingsoffers` (
  `TrackingID` int(10) unsigned NOT NULL,
  `OfferID` int(11) NOT NULL,
  PRIMARY KEY (`TrackingID`,`OfferID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Username` varchar(45) NOT NULL DEFAULT '',
  `Password` varchar(45) NOT NULL DEFAULT '',
  `DateAdded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastLogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Timezone` varchar(100) NOT NULL DEFAULT 'America/New_York',
  `StatsInterval` int(10) unsigned DEFAULT '8',
  `StatsCustomFrom` varchar(45) DEFAULT '',
  `StatsCustomTo` varchar(45) DEFAULT '',
  `SessionTimeout` int(11) NOT NULL DEFAULT '30',
  `CookieTimeout` int(11) NOT NULL DEFAULT '1440',
  `DefaultPage` varchar(100) NOT NULL DEFAULT 'campaigns.php',
  `LiveRefresh` int(11) NOT NULL DEFAULT '600',
  `LiveRecords` int(11) NOT NULL DEFAULT '100',
  `LiveView` tinyint(4) NOT NULL DEFAULT '0',
  `CampaignsSort` varchar(100) NOT NULL DEFAULT 'CampaignName',
  `CampaignsDir` varchar(20) NOT NULL DEFAULT 'asc',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `Username`, `Password`, `DateAdded`, `LastLogin`, `Timezone`, `StatsInterval`, `StatsCustomFrom`, `StatsCustomTo`, `SessionTimeout`, `CookieTimeout`, `DefaultPage`, `LiveRefresh`, `LiveRecords`, `LiveView`, `CampaignsSort`, `CampaignsDir`) VALUES
(1, 'user', '21232f297a57a5a743894a0e4a801fc3', '2008-01-13 00:00:00', '2015-09-08 21:12:26', 'Europe/Moscow', 8, '', '', 30, 1440, 'campaigns.php', 600, 100, 0, 'CampaignName', 'asc');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
